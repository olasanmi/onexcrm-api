<?php

namespace App\Http\Resources\Pod;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Http\Resources\PodProduct\PodProductCollection;
use App\Http\Resources\Client\ClientResource;
use App\Http\Resources\User\UserResource;
use App\Http\Resources\Seller\SellerResource;

class PodResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'total' => $this->total,
            'total_payment' => $this->total_payment,
            'utilities'  => $this->utilities,
            'type_payment'  => $this->type_payment,
            'observation'  => $this->observation,
            'seller_id'  => $this->seller_id,
            'client_id'  => $this->client_id,
            'user_id'  => $this->user_id,
            'effecty'  => $this->effecty,
            'debit'  => $this->debit,
            'credit'  => $this->credit,
            'transfer'  => $this->transfer,
            'reference'  => $this->reference,
            'products'  => new PodProductCollection($this->Products),
            'client'  => new ClientResource($this->Client),
            'seller'  => new SellerResource($this->Seller),
            'brand'  => new UserResource($this->User)
        ];
    }
}
