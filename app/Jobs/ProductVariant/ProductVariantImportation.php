<?php

namespace App\Jobs\ProductVariant;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use Automattic\WooCommerce\Client;

use App\ProductVariants;
use App\Product;

class ProductVariantImportation implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

     protected $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        //
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        //set woocomerce client
        $wooCommerce = new Client(
            $this->data['woocomerce']['url'],
            $this->data['woocomerce']['ck'],
            $this->data['woocomerce']['cs'],
            [
                'wp_api' => true,
                'version' => 'wc/v3'
            ]
        );

        //get variant data
        $variant = $wooCommerce->get('products/' . $this->data['product_id'] . '/variations/' . $this->data['woocomerce_id']);

        // bb product and variant
        $product = Product::where('woocomerce_id', '=', $this->data['product_id'])->first();

        // check if isset variant
        $issetVariant = ProductVariants::where('woocomerce_id', '=', $this->data['woocomerce_id'])->first();

        // set variant data
        //set price
        if (isset($variant->price) and $variant->price != '') {
            $price = $variant->price;
        } else {
            if (isset($product->price)) {
                $price = $product->price;
            } else {
                $price = 0;
            }
            
        }
        // set regular price
        if (isset($variant->regular_price) and $variant->regular_price != '') {
            $regularPrice = $variant->regular_price;
        } else {
            if (isset($product->regular_price)) {
                $regularPrice = $product->regular_price;
            } else {
                $regularPrice = 0;
            }
        }
        // set stock quantity
        if (isset($variant->stock_quantity)) {
            $stockQuantity = $variant->stock_quantity;
        } else {
            $stockQuantity = 100;
        }

        // validate is isset variant
        if (isset($issetVariant->id)) {
            $issetVariant->update([
                'status' => $variant->status,
                'description' => $variant->description,
                'sku' => $variant->sku,
                'price' => $price,
                'regular_price' => $regularPrice,
                'stock_quantity' => $stockQuantity,
                'stock_status' => $variant->stock_status
            ]);

            $attrs = '';
            foreach ($variant->attributes as $attr => $attribute) {
                $attrs = $attrs.''. $attribute->name . ' - ' . $attribute->option . ' .';
            }
            $issetVariant->attribute = $attrs;
            $issetVariant->save();
        }

    }
}
