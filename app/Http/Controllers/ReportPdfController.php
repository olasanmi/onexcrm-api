<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

use PDF;

class ReportPdfController extends Controller
{
    //ventas diarias
    public function pdfDiary(Request $request) {
        try {
            $data = $request->all();
            //main id user
            $mainId = $request->user()->getIdMain($request->user());

            //get user data
            $userData = User::findOrFail($mainId);
            $data['user'] = $userData;

            $pdf = PDF::loadView('report.sellers.diary', array('data' => $data));
            
            //set path
            $path = public_path('reports/sellers/');
            //set name
            if ($request->has('user') and $request->user['type'] === 2) {
                $fileName =  'seller_diary_'. $request->user['brand']['brand_name'] . '.' . 'pdf' ;
            } else {
                $fileName =  'seller_diary_'. $request->user['ownUser']['brand']['brand_name'] . '.' . 'pdf' ;
            }
            //save
            $pdf->save($path . '/' . $fileName);
            
            $file = public_path() . "/reports/sellers/" . $fileName;
            
            $headers = [
                'Content-Type' => 'application/pdf',
            ];
            
            return response()->download($file, 'factura.pdf', $headers);

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    //ventas por item
    public function pdfItemsSeller(Request $request) {
        try {
            $data = $request->all();
            //main id user
            $mainId = $request->user()->getIdMain($request->user());

            //get user data
            $userData = User::findOrFail($mainId);
            $data['user'] = $userData;

            $pdf = PDF::loadView('report.items.seller', array('data' => $data));
            
            //set path
            $path = public_path('reports/items/');
            //set name
            $fileName =  'seller_item_'. $userData->Ecommerce->brand_name . '.' . 'pdf' ;

            //save
            $pdf->save($path . '/' . $fileName);
            
            $file = public_path() . "/reports/items/" . $fileName;
            
            $headers = [
                'Content-Type' => 'application/pdf',
            ];
            
            return response()->download($file, 'factura.pdf', $headers);

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    // client seller pdf
    public function pdfClientsSeller(Request $request) {
        try {
            $data = $request->all();
            //main id user
            $mainId = $request->user()->getIdMain($request->user());

            //get user data
            $userData = User::findOrFail($mainId);
            $data['user'] = $userData;

            $pdf = PDF::loadView('report.clients.seller', array('data' => $data));
            
            //set path
            $path = public_path('reports/clients/');
            //set name
            $fileName =  'seller_client_'. $userData->Ecommerce->brand_name . '.' . 'pdf' ;

            //save
            $pdf->save($path . '/' . $fileName);
            
            $file = public_path() . "/reports/clients/" . $fileName;
            
            $headers = [
                'Content-Type' => 'application/pdf',
            ];
            
            return response()->download($file, 'factura.pdf', $headers);

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    //rentabildiad por item
    public function pdfItemRent(Request $request) {
        try {
            $data = $request->all();
            //main id user
            $mainId = $request->user()->getIdMain($request->user());

            //get user data
            $userData = User::findOrFail($mainId);
            $data['user'] = $userData;

            $pdf = PDF::loadView('report.items.rent', array('data' => $data));
            
            //set path
            $path = public_path('reports/items/');
            //set name
            $fileName =  'rent_item_'. $userData->Ecommerce->brand_name . '.' . 'pdf' ;

            //save
            $pdf->save($path . '/' . $fileName);
            
            $file = public_path() . "/reports/items/" . $fileName;
            
            $headers = [
                'Content-Type' => 'application/pdf',
            ];
            
            return response()->download($file, 'factura.pdf', $headers);

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    // SELLERS seller pdf
    public function pdfSellerSellers(Request $request) {
        try {
            $data = $request->all();
            //main id user
            $mainId = $request->user()->getIdMain($request->user());

            //get user data
            $userData = User::findOrFail($mainId);
            $data['user'] = $userData;

            $pdf = PDF::loadView('report.sellers.sellers', array('data' => $data));
            
            //set path
            $path = public_path('reports/sellers/');
            //set name
            $fileName =  'seller_sellers_'. $userData->Ecommerce->brand_name . '.' . 'pdf' ;

            //save
            $pdf->save($path . '/' . $fileName);
            
            $file = public_path() . "/reports/sellers/" . $fileName;
            
            $headers = [
                'Content-Type' => 'application/pdf',
            ];
            
            return response()->download($file, 'factura.pdf', $headers);

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

}
