<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DomicilioProduct extends Model
{
    //fillable
    protected $fillable = ['reference', 'name', 'price', 'quantity', 'total', 'domicilio_id', 'product_id', 'selected_store', 'utilities'];

    //relations
    public function domicilio() {
        return $this->belongsTo('App\Domicilio');
    }

    public function product() {
        return $this->belongsTo('App\Product');
    }

}
