<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDomicilioProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('domicilio_products', function (Blueprint $table) {
            $table->id();
            $table->string('reference')->nullable();
            $table->bigInteger('product_id')->nullable();
            $table->string('name')->nullable();
            $table->float('price', 50, 2)->nullable();
            $table->integer('quantity')->nullable();
            $table->float('total', 50, 2)->nullable();
            $table->unsignedBigInteger('domicilio_id')->nullable()->unsigned();
            $table->foreign('domicilio_id')->references('id')->on('domicilios')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('domicilio_products');
    }
}
