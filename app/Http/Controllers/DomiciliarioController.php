<?php

namespace App\Http\Controllers;

use App\Domiciliario;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Hash;

use App\User;

use App\Helpers\Helper;
use Response;

use App\Http\Controllers\AuditoriaController;

use App\Http\Resources\Domiciliario\DomiciliarioResource;
use App\Http\Resources\Domiciliario\DomiciliarioCollection;

use App\Http\Controllers\AuthController;

class DomiciliarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //listen
        try {
            if ($request->user()->type == 1)
                $domiciliarios = Domiciliario::all();

            if ($request->user()->type == 2)
                $domiciliarios = Domiciliario::where('user_id', '=', $request->user()->id)->get();

            if ($request->user()->type == 4)
                $domiciliarios = Domiciliario::where('user_id', '=', $request->user()->parent_id)->get();
            
            $response = Response::make(json_encode(['success' => new DomiciliarioCollection($domiciliarios)]), 200)->header('Content-Type','application/json');
            
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            //create new domiciliario
            $path = Helper::uploadImage($request->file, 'domiciliario');

            $domiciliario = Domiciliario::create([
                'name' => $request->name,
                'dni' => $request->dni,
                'phone' => $request->phone,
                'placa' => $request->placa,
                'photo' => $path,
                'username' => $request->username,
                'password' => $request->password
            ]);

            if ($request->user()->type == 2) {
                $domiciliario->user_id = $request->user()->id;
            } else {
                $domiciliario->user_id = $request->user()->parent_id;
            }
            $domiciliario->save();
            
            //get main id
            $mainId = $request->user()->getIdMain($request->user());
            
            //store user
            $user = AuthController::register($domiciliario->id, $request->email, $request->username, $request->password, $mainId);

            //set data to auditoria
            AuditoriaController::store($request->user()->name, 'Ha creado al domiciliario: ' .$domiciliario->name, 'Domiciliario', $mainId);

            if ($request->user()->type == 1)
                $domiciliarios = Domiciliario::all();

            if ($request->user()->type == 2)
                $domiciliarios = Domiciliario::where('user_id', '=', $request->user()->id)->get();

            if ($request->user()->type == 4)
                $domiciliarios = Domiciliario::where('user_id', '=', $request->user()->parent_id)->get();

            $response = Response::make(json_encode(['success' => new DomiciliarioCollection($domiciliarios)]), 201)->header('Content-Type','application/json');
            
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Domiciliario  $domiciliario
     * @return \Illuminate\Http\Response
     */
    public function show(Domiciliario $domiciliario)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Domiciliario  $domiciliario
     * @return \Illuminate\Http\Response
     */
    public function edit(Domiciliario $domiciliario)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Domiciliario  $domiciliario
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {
            //update domiciliario
            $domiciliario = Domiciliario::findOrFail($request->id);
            $domiciliario->update([
                'name' => $request->name,
                'dni' => $request->dni,
                'phone' => $request->phone,
                'placa' => $request->placa,
                'username' => $request->username
            ]);

            $mainId = $request->user()->getIdMain($request->user());
            AuditoriaController::store($request->user()->name, 'Ha modificado al domiciliario: ' .$domiciliario->name, 'Domiciliario', $mainId);

            //if request have image change it
            if ($request->has('file')) {
                Helper::deleteFile($domiciliario->photo, 'domiciliario');
                $path = Helper::uploadImage($request->file, 'domiciliario');
                $domiciliario->photo = $path;
                $domiciliario->save();
            }
            //edit user email
            $user = User::where('domiciliario_id', '=', $domiciliario->id)->first();
            if ($user) {
                $user->email = $request->email;
                $user->name = $request->username;
                $user->password = Hash::make($request->password);
                $user->save();
            }

            //return data
            if ($request->user()->type == 1)
                $domiciliarios = Domiciliario::all();

            if ($request->user()->type == 2)
                $domiciliarios = Domiciliario::where('user_id', '=', $request->user()->id)->get();
            
            if ($request->user()->type == 4)
                $domiciliarios = Domiciliario::where('user_id', '=', $request->user()->parent_id)->get();

            $response = Response::make(json_encode(['success' => new DomiciliarioCollection($domiciliarios)]), 201)->header('Content-Type','application/json');
            
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Domiciliario  $domiciliario
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try {
            //get domiciliario data and delete
            $domiciliario = Domiciliario::findOrFail($request->id);
            Helper::deleteFile($domiciliario->photo, 'domiciliario');
            
            $mainId = $request->user()->getIdMain($request->user());
            AuditoriaController::store($request->user()->name, 'Ha eliminado al domiciliario: ' .$domiciliario->name, 'Domiciliario', $mainId);

            $domiciliario->delete();

            //delete user from bbdd
            $user = User::where('domiciliario_id', '=', $domiciliario->id)->first();
            if ($user) {
                $user->delete();
            }
            //get all user domiciliarios
            if ($request->user()->type == 1)
                $domiciliarios = Domiciliario::all();

            if ($request->user()->type == 2)
                $domiciliarios = Domiciliario::where('user_id', '=', $request->user()->id)->get();

            if ($request->user()->type == 4)
                $domiciliarios = Domiciliario::where('user_id', '=', $request->user()->parent_id)->get();

            //return response
            $response = Response::make(json_encode(['success' => new DomiciliarioCollection($domiciliarios)]), 201)->header('Content-Type','application/json');
            
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }
}
