<?php

namespace App\Http\Controllers;

ini_set('max_execution_time', '5000000000000');

use App\Product;
use App\ProductVariants;
use App\ProductStorage;
use App\User;

use Illuminate\Http\Request;

use App\Http\Resources\Product\ProductsCollection;
use App\Http\Resources\Product\ProductsResource;

use Response;

use App\Helpers\Helper;

use Automattic\WooCommerce\Client;

use App\Http\Controllers\AuditoriaController;

use Illuminate\Support\Facades\Hash;

class ProductController extends Controller
{
    private $wooCommerce;
    
    function __construct() {
        $this->wooCommerce = new Client(
            env('WOOCOMMERCE_URL'),
            env('WOOCOMMERCE_CK'),
            env('WOOCOMMERCE_CS'),
            [
                'wp_api' => true,
                'version' => 'wc/v3'
            ]
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $products = Product::getProductsByUserType($request->user());

        $response = Response::make(json_encode(['success' => new ProductsCollection($products)]), 200)->header('Content-Type','application/json');

        return $response;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            //set woocommerce data to send info
            $wooData = $request->user()->getWooKeys($request->user()->type, $request->user());
            $wooCommerce = new Client(
                $wooData['url'],
                $wooData['ck'],
                $wooData['cs'],
                [
                    'wp_api' => true,
                    'version' => 'wc/v3'
                ]
            );
            //data send to crm
            $data = [
                'name' => $request->name,
                'type' => $request->type['id'],
                'regular_price' => $request->price,
                'description' => $request->description,
                'short_description' => $request->short_description,
                'status' => $request->status['id'],
                'price' => $request->price,
                'sku' => $request->sku,
                'manage_stock' => true,
                'regular_price' => $request->price,
                'stock_quantity' => $request->stock_quantity,
                'stock_status' => $request->stock_status['id'],
                'categories' => [
                    [
                        'id' => $request->category_id['slug']
                    ]
                ]
            ];
            //send data to crm
            $productInWoocommerce = $wooCommerce->post('products', $data);
            $productInWoocommerce = json_encode($productInWoocommerce, true);
            $decodeWoocomerce = json_decode($productInWoocommerce, true);
            //save in our bbdd
            if (isset($decodeWoocomerce['id'])) {
                // get id of user main
                $mainId = $request->user()->getIdMain($request->user());
                //save product on bbdd
                $product = Product::create([
                    'name' => $request->name,
                    'type' => $request->type['id'],
                    'status' => $request->status['id'],
                    'description' => $request->description,
                    'short_description' => $request->short_description,
                    'price' => $request->price,
                    'sku' => $request->sku,
                    'utilities' => $request->utilities,
                    'regular_price' => $request->price,
                    'stock_quantity' => $request->stock_quantity,
                    'stock_status' => $request->stock_status['id'],
                    'category_id' => $request->category_id['id'],
                    'user_id' => $mainId
                ]);
                $product->woocomerce_id = $decodeWoocomerce['id'];
                $product->save();
            
            } else {
                return response()->json(['errors'=>array(['code' => 422, 'message'=>'Ha ocurrido un error creando el producto en el woocommerce, inténtalo de nuevo.'])], 422);
            }

            $mainId = $request->user()->getIdMain($request->user());
            AuditoriaController::store($request->user()->name, 'Ha creado el producto: ' .$product->name, 'Productos', $mainId);

            $products = Product::getProductsByUserType($request->user());

            $response = Response::make(json_encode(['success' => new ProductsCollection($products)]), 200)->header('Content-Type','application/json');

            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        if ($product)
            $response = Response::make(json_encode(['success' => new ProductsResource($product)]), 200)->header('Content-Type','application/json');
        else
            $response = Response::make(json_encode((['errors'=>array(['code' => 404, 'message'=>'No existe ninguna categoria con esa id.'])])), 404)->header('Content-Type','application/json');;

        return $response;

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        try {
            // validate f user is admin
            if ($request->user()->type == 1) {
                return response()->json(['errors' => array(['code' => 422, 'message' => 'Por el momento el administrador no puede modificar productos.'])], 422);
            }
            //set woocommerce data to send info
            $wooData = $request->user()->getWooKeys($request->user()->type, $request->user());
            $wooCommerce = new Client(
                $wooData['url'],
                $wooData['ck'],
                $wooData['cs'],
                [
                    'wp_api' => true,
                    'version' => 'wc/v3'
                ]
            );
            //update product in our bbdd
            $product->update([
                'name' => $request->name,
                'type' => $request->type['id'],
                'status' => $request->status['id'],
                'description' => $request->description,
                'short_description' => $request->short_description,
                'price' => $request->price,
                'sku' => $request->sku,
                'utilities' => $request->value,
                'regular_price' => $request->price,
                'stock_quantity' => $request->stock_quantity,
                'stock_status' => $request->stock_status['id']
            ]);
            //data to send
            $data = [
                'name' => $request->name,
                'type' => $request->type['id'],
                'regular_price' => $request->price,
                'description' => $request->description,
                'short_description' => $request->short_description,
                'status' => $request->status['id'],
                'sku' => $request->sku,
                'manage_stock' => true,
                'price' => (string)$request->price,
                'regular_price' => (string)$request->price,
                'stock_quantity' => $request->stock_quantity,
                'stock_status' => $request->stock_status['id']
            ];

            $productInWoocommerce = $wooCommerce->put('products/' . $product->woocomerce_id, $data);

            $mainId = $request->user()->getIdMain($request->user());
            AuditoriaController::store($request->user()->name, 'Ha modificado el producto: ' .$product->name, 'Productos', $mainId);

            $products = Product::getProductsByUserType($request->user());

            $response = Response::make(json_encode(['success' => new ProductsCollection($products)]), 200)->header('Content-Type','application/json');

            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Product $product)
    {
        try {
            //set woocommerce data to send info
            $wooData = $request->user()->getWooKeys($request->user()->type, $request->user());
            $wooCommerce = new Client(
                $wooData['url'],
                $wooData['ck'],
                $wooData['cs'],
                [
                    'wp_api' => true,
                    'version' => 'wc/v3'
                ]
            );
            //delete product
            if (isset($product->woocomerce_id)) {
                $wooCommerce->delete('products/' . $product->woocomerce_id, ['force' => true]);
                $product->delete();
            }

            $mainId = $request->user()->getIdMain($request->user());
            AuditoriaController::store($request->user()->name, 'Ha eliminado el producto: ' .$product->name, 'Productos', $mainId);

            //return response
            $products = Product::getProductsByUserType($request->user());

            $response = Response::make(json_encode(['success' => new ProductsCollection($products)]), 200)->header('Content-Type','application/json');

            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    //mange stock
    public function manageStock($products, $user) {
        //set woocommerce data to send info
        $wooData = $user->getWooKeys($user->type, $user);
        $wooCommerce = new Client(
            $wooData['url'],
            $wooData['ck'],
            $wooData['cs'],
            [
                'wp_api' => true,
                'version' => 'wc/v3'
            ]
        );
        //simple data array
        $dataSimple = [
            'update'=> []
        ];
        //loop in prods
        foreach ($products as $key => $value) {
            //get product data
            $product = Product::findOrFail($value['product_id']);
            //switch product type
            if ($product->stock_quantity > 0) {
                //update storage stock
                if (isset($value['selectedStorage'])) {
                    $storage = ProductStorage::where('storage_id', '=', $value['selectedStorage'])
                    ->where('product_id', '=', $product->id)
                    ->first();
                    if (isset($storage->id)) {
                        if ($storage->quantity > 0 and $storage->quantity >= $value['quantity']) {
                            $storage->quantity = ($storage->quantity - $value['quantity']);
                        }
                        $storage->save();
                    }
                }
                //update product stock

                $product->stock_quantity = ($product->stock_quantity - $value['quantity']);
                        //set out stock if producto quantiti is 0
                if ($product->stock_quantity == 0) {
                    $product->stock_status = 'outofstock';
                }
                        //save product in our bbdd
                $product->save();
                        //set data for batch update
                $pData = [
                    'stock_quantity' => $product->stock_quantity,
                    'stock_status' => $product->stock_status,
                    'id' => $product->woocomerce_id
                ];
                array_push($dataSimple['update'], $pData);
            }
            //if product is variable
            if ($product->type == 'variable') {
                //get variant data bi sku
                $variantData = ProductVariants::findOrFail($value['idVariant']);
                //if quiantiti hight 0
                if ($variantData->stock_quantity > 0) {
                    //discount variant data
                    $variantData->stock_quantity = ($variantData->stock_quantity - $value['quantity']);
                    //set out stock variant if quantiti lower than 0
                    if ($variantData->stock_quantity == 0) {
                        $variantData->stock_status = 'outofstock';
                    }
                    //save variant data
                    $variantData->save();
                    //send data to woocomerce
                    $dataVariant = [
                        'stock_quantity' => $variantData->stock_quantity,
                        'stock_status' => $variantData->stock_status
                    ];

                    $wooCommerce->put('products/' . $variantData->Product->woocomerce_id .'/variations/' . $variantData->woocomerce_id, $dataVariant);
                }
            }
        }
        // update simple product with bacth update
        $wooCommerce->post('products/batch', $dataSimple);
    }

    public function manageStockFromUpdateItem($products, $user) {
        //set woocommerce data to send info
        $wooData = $user->getWooKeys($user->type, $user);
        $wooCommerce = new Client(
            $wooData['url'],
            $wooData['ck'],
            $wooData['cs'],
            [
                'wp_api' => true,
                'version' => 'wc/v3'
            ]
        );
        //findProduct
        //simple data array
        $dataSimple = [
            'update'=> []
        ];
        foreach ($products as $key => $value) {
            //get product data
            $product = Product::findOrFail($value['product_id']);
            //upate storge data
            if (isset($value['selected_store'])) {
                $storage = ProductStorage::where('storage_id', '=', $value['selected_store'])
                ->where('product_id', '=', $product->id)
                ->first();
                if (isset($storage->id)) {
                    if ($storage->quantity >= 0) {
                        if ($value['operation'] == 1)
                            $storage->quantity = ($storage->quantity - $value['newQuantity']);
                            $storage->save();
                        if ($value['operation'] == 2)
                            $storage->quantity = ($storage->quantity + $value['newQuantity']);
                            $storage->save();
                    }
                }
            }
            //finish update stock of storage
            //start update product global storage
            if ($value['operation'] == 1) {
                $product->stock_quantity = ($product->stock_quantity - $value['newQuantity']);
            } else {
                $product->stock_quantity = ($product->stock_quantity + $value['newQuantity']);
            }
            if ($product->stock_quantity == 0) {
                $product->stock_status = 'outofstock';
            }
            $product->save();
            //end global storage update
            //set woocommerce data to update
            $pData = [
                'stock_quantity' => $product->stock_quantity,
                'stock_status' => $product->stock_status,
                'id' => $product->woocomerce_id
            ];
            array_push($dataSimple['update'], $pData);
            //end set woocomerce data update

            //if product is variable
            if ($product->type == 'variable') {
                //get variant data bi sku
                $variantData = ProductVariants::findOrFail($value['idVariant']);
                //if quiantity > 0
                if ($variantData->stock_quantity > 0) {
                    //discount variant data
                    if ($value['operation'] == 1) {
                        $variantData->stock_quantity = ($variantData->stock_quantity - $value['newQuantity']);
                    } else {
                        $variantData->stock_quantity = ($variantData->stock_quantity + $value['newQuantity']);
                    }
                    //set out stock variant if quantiti lower than 0
                    if ($variantData->stock_quantity == 0) {
                        $variantData->stock_status = 'outofstock';
                    }
                    //save variant data
                    $variantData->save();
                    //send data to woocomerce
                    $dataVariant = [
                        'stock_quantity' => $variantData->stock_quantity,
                        'stock_status' => $variantData->stock_status
                    ];

                    $wooCommerce->put('products/' . $variantData->Product->woocomerce_id .'/variations/' . $variantData->woocomerce_id, $dataVariant);
                }
            }
        }
        // update simple product with bacth update
        $wooCommerce->post('products/batch', $dataSimple);
        //end batch update
    }

    //search prods by name
    public function search(Request $request) {
        //get main id
        $mainId = $request->user()->getIdMain($request->user());
        $products = Product::where('user_id', '=', $mainId)
        ->where('name', 'like', '%' . $request->name .'%')
        ->get();
        $response = Response::make(json_encode(['success' => new ProductsCollection($products)]), 200)->header('Content-Type','application/json');

        return $response;
    }

    //search prods by ref
    public function searchRef(Request $request, $ref) {
        //get main id
        if (isset($ref)) {
            $mainId = $request->user()->getIdMain($request->user());
            $products = Product::where('user_id', '=', $mainId)
            ->where('sku', 'like', '%' . $ref .'%')
            ->get();
            $response = Response::make(json_encode(['success' => new ProductsCollection($products)]), 200)->header('Content-Type','application/json');

            return $response;
        }
    }

    // import products for user
    public function importProducts(Request $request) {
        try {
            //set woocommerce data to send info
            $wooData = $request->user()->getWooKeys($request->user()->type, $request->user());
            $wooCommerce = new Client(
                $wooData['url'],
                $wooData['ck'],
                $wooData['cs'],
                [
                    'wp_api' => true,
                    'version' => 'wc/v3'
                ]
            );
            //get main user id
            $mainId = $request->user()->getIdMain($request->user());
            $allNewProducts = [];
            //lops all products
            foreach ($request->products as $key => $value) {
                $product = Product::where('woocomerce_id', '=', $value['id'])
                ->where('user_id', '=', $mainId)
                ->first();
                //if don`t issets product id create product
                $newProduct = [];
                if ($value['stock_quantity'] > 0) {
                    $quantity = $value['stock_quantity'];
                } else {
                    $quantity = 100;
                }
                if (!isset($product->id)) {
                    $newProduct = [
                        'name' => $value['name'],
                        'type' => $value['type'],
                        'status' => $value['status'],
                        'description' => $value['description'],
                        'short_description' => $value['short_description'],
                        'price' => $value['price'],
                        'slug' => $value['img'],
                        'sku' => $value['sku'],
                        'regular_price' => $value['regular_price'],
                        'stock_quantity' => $quantity,
                        'stock_status' => $value['stock_status'],
                        'woocomerce_id' => $value['id'],
                        'user_id' => $mainId
                    ];
                    array_push($allNewProducts, $newProduct);
                } else {
                    $product->update([
                        'name' => $value['name'],
                        'type' => $value['type'],
                        'status' => $value['status'],
                        'description' => $value['description'],
                        'short_description' => $value['short_description'],
                        'price' => $value['price'],
                        'slug' => $value['img'],
                        'sku' => $value['sku'],
                        'regular_price' => $value['regular_price'],
                        'stock_quantity' => $quantity,
                        'stock_status' => $value['stock_status'],
                        'woocomerce_id' => $value['id']
                    ]);
                }
            }
            $products = Product::insert($allNewProducts);
            
            // proccess variant 
            $allVariantProducts = Product::where('type', '=', 'variable')
            ->where('user_id', '=', $mainId)
            ->orderBy('id', 'DESC')
            ->get();
            
            //loops of all product with variant
            foreach ($allVariantProducts as $key => $value) {
                $id = $value['woocomerce_id'];
                //filter in request main product with woocommerce proccess id
                $filterInAllNewProducts = collect($request->products)->where('id', $id)->first();
                //loops in variant of product
                $variants = array();
                if (isset($filterInAllNewProducts['type']) and $filterInAllNewProducts['type'] == 'variable') {
                    foreach ($filterInAllNewProducts['variants'] as $var => $variant) {
                         //load variant data from woocommercer
                        $variant = $wooCommerce->get('products/' . $id . '/variations/' . $variant);
                        //validate variant
                        $issetVariant = ProductVariants::where('woocomerce_id', '=', $variant->id)
                        ->first();
                        //if don`t issets variation id create variation
                        if (isset($variant->price) and $variant->price != '') {
                            $price = $variant->price;
                        } else {
                            $price = $value['price'];
                        }
                        if (isset($variant->regular_price) and $variant->regular_price != '') {
                            $regularPrice = $variant->regular_price;
                        } else {
                            $regularPrice = $value['regular_price'];
                        }
                        if (isset($variant->stock_quantity)) {
                            $stockQuantity = $variant->stock_quantity;
                        } else {
                            $stockQuantity = 100;
                        }
                        // validate is isset variant
                        if (!isset($issetVariant->id)) {
                            $newVariant = [
                                'status' => $variant->status,
                                'description' => $variant->description,
                                'sku' => $variant->sku,
                                'price' => $price,
                                'regular_price' => $regularPrice,
                                'stock_quantity' => $stockQuantity,
                                'stock_status' => $variant->stock_status,
                                'product_id' => $value['id'],
                                'woocomerce_id' => $variant->id
                            ];
                            $attrs = '';
                            foreach ($variant->attributes as $attr => $attribute) {
                                $attrs = $attrs.''. $attribute->name . ' - ' . $attribute->option . ' .';
                            }
                            $newVariant['attribute'] = $attrs;
                            array_push($variants, $newVariant);

                        } else {
                            $issetVariant->update([
                                'status' => $variant->status,
                                'description' => $variant->description,
                                'sku' => $variant->sku,
                                'price' => $price,
                                'regular_price' => $regularPrice,
                                'stock_quantity' => $stockQuantity,
                                'stock_status' => $variant->stock_status
                            ]);
                            
                            $attrs = '';
                            foreach ($variant->attributes as $attr => $attribute) {
                                $attrs = $attrs.''. $attribute->name . ' - ' . $attribute->option . ' .';
                            }
                            $issetVariant['attribute'] = $attrs;
                            $issetVariant->save();
                        }
                    }
                    $proccessVariants = ProductVariants::insert($variants);
                }
            }

            $response = Response::make(json_encode(['success' => 'Se han importado los productos']), 200)->header('Content-Type','application/json');
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    //import product variants data
    public function importProductsVariantData($variations, $product) {
        //loops variations of a product.
        foreach ($variations as $key => $value) {
            $variant = ProductVariants::where('woocomerce_id', '=', $value->id)
            ->first();
            //if don`t issets variation id create variation
            if (isset($value->price) and $value->price != '') {
                $price = $value->price;
            } else {
                $price = $product->price;
            }
            if (isset($value->regular_price) and $value->regular_price != '') {
                $regularPrice = $value->regular_price;
            } else {
                $regularPrice = $product->price;
            }

            if (isset($value->stock_quantity)) {
                $stockQuantity = $value->stock_quantity;
            } else {
                $stockQuantity = 100;
            }
            if (!isset($variant->id)) {
                $variant = ProductVariants::create([
                    'status' => $value->status,
                    'description' => $value->description,
                    'sku' => $value->sku,
                    'price' => $price,
                    'regular_price' => $regularPrice,
                    'stock_quantity' => $stockQuantity,
                    'stock_status' => $value->stock_status,
                    'product_id' => $product->id,
                    'woocomerce_id' => $value->id
                ]);
                $attr = '';
                foreach ($value->attributes as $attr => $attribute) {
                    $attr .= $attribute->name . ' - ' . $attribute->option . '.';
                }
                $variant->attribute = $attr;
                $variant->save();
                //if don`t issets variaton id update product
            } else {
                $variant->update([
                    'status' => $value->status,
                    'description' => $value->description,
                    'sku' => $value->sku,
                    'price' => $price,
                    'regular_price' => $regularPrice,
                    'stock_quantity' => $stockQuantity,
                    'stock_status' => $value->stock_status
                ]);
                $attr = '';
                foreach ($value->attributes as $attr => $attribute) {
                    $attr .= $attribute->name . ' - ' . $attribute->option . '.';
                }
                $variant->attribute = $attr;
                $variant->save();
            }
        }
    }

    // from erp create or update product
    public function fromErpPlugin(Request $request) {
        try {
            if (!$request->has('username') or !$request->has('password')) {
                return response()->json(['errors' => 'Configura tus credenciales de ingreso.'], 422);
            } else {
                $user = User::where('name', $request->username)->first();
                if ($user) {
                    $mainId = $user->getIdMain($user);
                    if (Hash::check($request->password, $user->password)) {
                        $product = Product::where('woocomerce_id', '=', $request->id)
                        ->where('user_id', '=', $mainId)
                        ->first();
                        
                        if ($request->stock_quantity > 0) {
                            $quantity = $request->stock_quantity;
                        } else {
                            $quantity = 100;
                        }
                        
                        if (is_null($request->price)) {
                            $price = 0;
                        } else {
                            $price = $request->price;
                        }

                        if (is_null($request->regularPrice)) {
                            $regular_price = 0;
                        } else {
                            $regular_price = $request->regularPrice;
                        }
                        if (!isset($product->id)) {
                            $product = Product::create([
                                'name' => $request->name,
                                'woocomerce_id' => $request->id,
                                'status' => $request->status,
                                'description' => $request->description,
                                'sku' => $request->sku,
                                'stock_quantity' => $quantity,
                                'stock_status' => $request->stock_status,
                                'price' => $price,
                                'regular_price' => $regular_price,
                                'type' => $request->type,
                                'user_id' => $mainId
                            ]);

                            AuditoriaController::store($user->name, 'Ha creado el producto: ' .$product->name . ' desde el plugin del erps.', 'Productos', $mainId);

                            $response = Response::make(json_encode(['success' => 'Se ha creado el producto correctamente.']), 200)->header('Content-Type','application/json');
                        } else {
                            $product->update([
                                'name' => $request->name,
                                'woocomerce_id' => $request->id,
                                'status' => $request->status,
                                'description' => $request->description,
                                'sku' => $request->sku,
                                'stock_quantity' => $quantity,
                                'stock_status' => $request->stock_status,
                                'price' => $price,
                                'regular_price' => $regular_price,
                                'type' => $request->type,
                                'user_id' => $mainId
                            ]);

                            AuditoriaController::store($user->name, 'Ha modificado el producto: ' .$product->name . ' desde el plugin del erps.', 'Productos', $mainId);

                            $response = Response::make(json_encode(['success' => 'Se ha modificado el producto correctamente.']), 200)->header('Content-Type','application/json');
                        }

                        
                        return $response;

                    } else {
                        $response = ['errors' => array(['code' => 422, 'message'=>'La contraseña es incorrecta.'])];
                        return response($response, 422);
                    }
                } else {
                    $response = ['errors' => array(['code' => 422, 'message'=>'El usuario no existe.'])];
                    return response($response, 422);
                }
            }

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    //paginate
    public function paginateProduct(Request $request) {
        try {
            $mainId = $request->user()->getIdMain($request->user());
            
            $products = Product::where('user_id', '=', $mainId)
            ->paginate(52);

            $data['products'] = new ProductsCollection($products);
            $data['last_page'] = $products->lastPage();
            
            $response = Response::make(json_encode(['success' => $data]), 200)->header('Content-Type','application/json');

            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    // list product for report
    public function getAllForReport(Request $request) {
        try {
            $mainId = $request->user()->getIdMain($request->user());
            
            $products = Product::where('user_id', '=', $mainId)
            ->get();
            
            $response = Response::make(json_encode(['success' => $products]), 200)->header('Content-Type','application/json');
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

}
