<?php
namespace App\Helpers;
use File;

use App\Domicilio;
use App\Mail\StatusChangeEmail;
use Illuminate\Support\Facades\Mail;

use Automattic\WooCommerce\Client;

/**
 * 
 */
class Helper
{
	 //upload file
    public static function uploadImage($file, $pathModel) {
        $name = date('Ymd_His');
        $file->move('images/'.$pathModel, $name);
        return $name;
    }

    //delete file
    public static function deleteFile($file, $pathModel) {
        $oldfile = public_path('images/'.$pathModel.'/'.$file);
        if (File::exists($oldfile)) {
            unlink($oldfile);
        }
    }

    //add reference
    public static function generateReference($length) {
        $key = '';
        $keys = array_merge(range(0, 6), range('A', 'Z'));
        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }
        return $key;
    }

    //send ws
    public static function sendWhatsAppMsg($id, $status, $observation) {
        //get domicilio data
        $domicilio = Domicilio::findOrFail($id);

        // validate status
        if ($status == 1) {
            $mensajeWs  = "👏 Hola, *".$domicilio->name."* te contamos que ya hemos ASIGNADO para tu pedido al mensajero 🛵:\n \n 🛵Información del mensajero: \n *".$domicilio->domiciliario->name."* \n *placa:* ".$domicilio->domiciliario->placa." "."\n \n  🚫Esto es una notificación automática, NO UNA LÍNEA DE ATENCIÓN🚫 \n\n ✅Para comunicarte con un asesor de @xhobbies CLIC AQUI 👉 \nhttps://bit.ly/34iRxyO \n\n 🤑Compra ONLINE en 📲xhobbies.co🖥";
        }

        if ($status == 2) {
            $mensajeWs = "😱 Oye ".$domicilio->name.", te contamos que el mensajero 🛵".$domicilio->domiciliario->name."  ya RECOGIÓ tu pedido y va a salir pitado a llevarlo🚀.\n \n"."⏰ RECUERDA QUE EL DOMICILIO DESDE ESTE MOMENTO PUEDE DEMORARSE ENTRE (30min - 2horas)👌.\n \n"."Información del mensajero: \n 🧔🟨✈*".$domicilio->domiciliario->name."* \n 🔰*placa:* ".$domicilio->domiciliario->placa."\n \n  🚫Esto es una notificación automática, NO UNA LÍNEA DE ATENCIÓN🚫 \n\n ✅Para comunicarte con un asesor de @xhobbies CLIC AQUI 👉 \nhttps://bit.ly/34iRxyO \n\n 🤑Compra ONLINE en 📲xhobbies.co🖥";

        }

        if ($status == 3) {
            $mensajeWs = "🥳🕺💃🟨🤟🥳 \n Gracias ".$domicilio->name.", tu pedido ya ha sido ENTREGADO!!\n\n" . "📑Notas dejadas por el mensajero: \n".$domicilio->observation."\n\n 🙌Agradecemos tu compra y recuerda que lo que necesites es con el mayor de los gustos!\n\n Puedes descargar tu factura en el siguiente link:".url('/factura/'.$domicilio->id)." \n\n 🚫Esto es una notificación automática, NO UNA LÍNEA DE ATENCIÓN🚫 \n\n ✅Para comunicarte con un asesor de @xhobbies CLIC AQUI 👉 \nhttps://bit.ly/34iRxyO \n\n 🤑Compra ONLINE en 📲xhobbies.co🖥";

        }

        if ($status == 4) {
            $mensajeWs = "⚠⚠⚠⚠⚠⚠⚠⚠ \n".$domicilio->name.", te contamos que tu pedido tiene la siguiente novedad:\n\n"."📞 Comunícate con tu mensajero al siguiente celular directamente con tu mensajero 🧔🟨✈ ".$domicilio->domiciliario->name." para que cuadres con el pronto 📞 ".$domicilio->domiciliario->phone."\n\nObservaciones dejadas por el mensajero: ".$observation."\n\n 🚫Esto es una notificación automática, NO UNA LÍNEA DE ATENCIÓN🚫 \n\n ✅Para comunicarte con un asesor de @xhobbies CLIC AQUI 👉 \nhttps://bit.ly/34iRxyO \n\n 🤑Compra ONLINE en 📲xhobbies.co🖥";
        }

        if ($status == 5) {
            $mensajeWs = "😭💔😢💔😰💔😭💔😢 \n Has cancelado tu pedido! De todas formas sabes que estaremos puestos para brindarte un excelente servicio! \n\n  🚫Esto es una notificación automática, NO UNA LÍNEA DE ATENCIÓN🚫 \n\n ✅Para comunicarte con un asesor de @xhobbies CLIC AQUI 👉 \nhttps://bit.ly/34iRxyO \n\n 🤑Compra ONLINE en 📲xhobbies.co🖥";
        }

        // send data
        $dataSend = [
            'phone' => +'57' . $domicilio->phone, // Receivers phone
            'body' => $mensajeWs, // Message
        ];
        $json = json_encode($dataSend); // Encode data to JSON
        // URL for request POST /message
        $url = 'https://eu15.chat-api.com/instance114416/message?token=mehrpul8jvu5k20z';
        // Make a POST request
        $options = stream_context_create(['http' => [
            'method'  => 'POST',
            'header'  => 'Content-type: application/json',
            'content' => $json
        ]]);
        $result = file_get_contents($url, false, $options);
    }

    //send ws
    public static function sendEmailMsg($id, $status, $observation) {
        //get domicilio data
        $domicilio = Domicilio::findOrFail($id);
        // send data
        $data['status'] = $status;
        $data['observation'] = $observation;
        $data['name'] = $domicilio->name;
        $data['reference'] = $domicilio->reference;
        $data['domiciliario'] = $domicilio->domiciliario->name;
        $data['placa'] = $domicilio->domiciliario->placa;
        $data['phone'] = $domicilio->domiciliario->phone;
        $body = $data;
        Mail::to($domicilio->email)->send(new StatusChangeEmail($data));
    }

    // get from woocomerce
    public static function getWoocomerce($url, $user) {
        // get woocomerce data
        $wooData = $user->getWooKeys($user->type, $user);

        //set woocomerce data
        $wooCommerce = new Client(
            $wooData['url'],
            $wooData['ck'],
            $wooData['cs'],
            [
                'wp_api' => true,
                'version' => 'wc/v3'
            ]
        );
        
        // send post to woocomerce
        $get = $wooCommerce->get($url . '?per_page=100&page=1');

        // return response
        return $get;
    }

    // post to woocomerce
    public static function createInWoocomerce($data, $url, $user, $type) {
        // get woocomerce data
        $wooData = $user->getWooKeys($user->type, $user);

        //set woocomerce data
        $wooCommerce = new Client(
            $wooData['url'],
            $wooData['ck'],
            $wooData['cs'],
            [
                'wp_api' => true,
                'version' => 'wc/v3'
            ]
        );
        // send post to woocomerce
        if ($type == 1) {
            $productInWoocommerce = $wooCommerce->post($url, $data);
        } else {
            $productInWoocommerce = $wooCommerce->put($url, $data);
        }
        
        // decode response
        $productInWoocommerce = json_encode($productInWoocommerce, true);
        $decodeWoocomerce = json_decode($productInWoocommerce, true);

        // return response
        return $decodeWoocomerce;
    }

    // delete from woocomerce
    public static function deleteInWoocomerce($url, $user) {
        // get woocomerce data
        $wooData = $user->getWooKeys($user->type, $user);

        //set woocomerce data
        $wooCommerce = new Client(
            $wooData['url'],
            $wooData['ck'],
            $wooData['cs'],
            [
                'wp_api' => true,
                'version' => 'wc/v3'
            ]
        );
        
        // send post to woocomerce
        $delete = $wooCommerce->delete($url, ['force' => true]);

        // return response
        return $delete;
    }
}