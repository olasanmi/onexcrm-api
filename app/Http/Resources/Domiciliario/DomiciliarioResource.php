<?php

namespace App\Http\Resources\Domiciliario;

use Illuminate\Http\Resources\Json\JsonResource;

class DomiciliarioResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'dni' => $this->dni,
            'phone' => $this->phone,
            'placa' => $this->placa,
            'photo' => $this->photo,
        ];
    }
}
