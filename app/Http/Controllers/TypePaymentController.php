<?php

namespace App\Http\Controllers;

use App\TypePayment;
use Illuminate\Http\Request;

use Response;
use App\Http\Resources\PaymentType\PaymentTypeCollection;

use App\Helpers\Helper;

class TypePaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        try {
            // get id of user main
            $mainId = $request->user()->getIdMain($request->user());

            if ($request->user()->type == 1)
                $payments = TypePayment::orderBy('id', 'DESC')
                ->get();
            else
                $payments = TypePayment::orderBy('id', 'DESC')
                ->where('user_id', '=', $mainId)
                ->get();

            $response = Response::make(json_encode(['success' => new PaymentTypeCollection($payments)]), 200)->header('Content-Type','application/json');
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors' => array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //store payment type
        try {
            // get id of user main
            $mainId = $request->user()->getIdMain($request->user());

            $payment = TypePayment::create([
                'name' => $request->name,
                'user_id' => $mainId
            ]);

            if (isset($request->file)) {
                $path = Helper::uploadImage($request->file, 'payments');
                $payment->photo = $path;
                $payment->save();
            }


            $response = Response::make(json_encode(['success' => 'Se ha guardado el tipo de pago correctamente']), 201)->header('Content-Type','application/json');
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors' => array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TypePayment  $typePayment
     * @return \Illuminate\Http\Response
     */
    public function show(TypePayment $typePayment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TypePayment  $typePayment
     * @return \Illuminate\Http\Response
     */
    public function edit(TypePayment $typePayment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TypePayment  $typePayment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TypePayment $typePayment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TypePayment  $typePayment
     * @return \Illuminate\Http\Response
     */
    public function destroy(TypePayment $typePayment)
    {
        //
        try {
            if (isset($typePayment->photo)) {
                Helper::deleteFile($typePayment->photo, 'payments');
            }

            $typePayment->delete();

            $response = Response::make(json_encode(['success' => 'Se ha guardado el tipo de pago correctamente']), 201)->header('Content-Type','application/json');
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors' => array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }
}
