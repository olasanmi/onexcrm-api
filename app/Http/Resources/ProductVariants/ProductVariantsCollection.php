<?php

namespace App\Http\Resources\ProductVariants;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ProductVariantsCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->map(function ($models) {
                return [
                    'id' => $models->id,
                    'attribute' => $models->attribute,
                    'status' => $models->status,
                    'description' => $models->description,
                    'short_description' => $models->short_description,
                    'sku' => $models->sku,
                    'price' => $models->price,
                    'regular_price' => $models->regular_price,
                    'sale_price' => $models->sale_price,
                    'stock_quantity' => $models->stock_quantity,
                    'stock_status' => $models->stock_status
                ];
            });
    }
}
