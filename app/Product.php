<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Http\Resources\Storage\StoragesProductCollection;
use App\ProductStorage;
use App\Storage;

class Product extends Model
{
    //fillable
    protected $fillable = ['name' , 'slug', 'type', 'status', 'description', 'short_description', 'featured', 'sku', 'price', 'regular_price', 'sale_price', 'stock_quantity', 'stock_status', 'category_id', 'woocomerce_id', 'user_id', 'utilities'];

    // relations
    public function Categories() {
    	return $this->belongsToMany('App\Category', 'category_product');
    }

    public function Attributes() {
        return $this->belongsToMany('App\Attribute', 'attribute_product');
    }

    public function Terms() {
        return $this->belongsToMany('App\Term', 'term_product');
    }

    public function ProductVariants() {
    	return $this->hasMany('App\ProductVariants');
    }

    public function Storages()
    {
        return $this->belongsToMany('App\Storage');
    }

    public function DomicilioProduct()
    {
        return $this->hasMany('App\DomicilioProduct');
    }

    public function PodOrderProduct()
    {
        return $this->hasMany('App\PodOrderProduct');
    }

    //hook
    public static function getProductsByUserType($user) {
        if ($user->type == 1)
            $products = Product::orderBy('id', 'ASC')
            ->get();

        if ($user->type == 2)
            $products = Product::orderBy('id', 'ASC')
            ->where('user_id', '=', $user->id)
            ->get();

         if ($user->type == 4)
            $products = Product::orderBy('id', 'ASC')
            ->where('user_id', '=', $user->parent_id)
            ->get();

        return $products;
    }

    public static function getStorages($model) {
        if (count($model->Storages) > 0) {
            $storages = [];
            $productStorages = ProductStorage::where('product_id', '=', $model->id)
            ->get();
            foreach ($productStorages as $key => $value) {
                $storage = Storage::findOrFail($value->storage_id);
                $storage->quantity  = $value->quantity;
                $storage->idT  = $value->id;
                array_push($storages, $storage);
            }
            return new StoragesProductCollection($storages);
        } else {
            return [];
        }
    }

}
