<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ecommerce extends Model
{
    // fillable
    protected $fillable = ['address', 'state', 'city', 'type_doc', 'nit', 'brand_name', 'phone', 'commercial_brand', 'economic_activity', 'user_id', 'legal_form', 'iva', 'email_invoice', 'email_notification', 'web_url', 'rut_path'];

    // relations
    public function User()
    {
        return $this->belongsToMany('App\User');
    }
}
