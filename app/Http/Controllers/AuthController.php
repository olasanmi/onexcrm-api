<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use App\Http\Resources\User\UserResource;
use App\Http\Resources\User\UserCollection;

use App\Http\Controllers\AuditoriaController;

use App\User;
use App\Ecommerce;

use App\Helpers\Helper;

use Response;

use App\Mail\CreateUserByAdmin;
use App\Mail\Recovery\RecoveryPassword;
use Illuminate\Support\Facades\Mail;

use Carbon\Carbon;
use DB;

class AuthController extends Controller
{
    //register new user
    public static function register($domiciliario, $email, $username, $password, $mainId) {
        // return response()->json(123);
    	if (User::where('email', '=', $email)->count() > 0) {
           return response()->json(['errors'=>array(['code' => 422, 'message'=>'El correo se encuentra en uso.'])], 422);
        }
        $user = User::create([
            'name' => $username,
            'email' => $email,
            'password' => Hash::make($password),
            'type' => 3,
            'domiciliario_id' => $domiciliario,
            'parent_id' => $mainId
        ]);
        $response = Response::make(json_encode(['data' => $user]), 201)->header('Content-Type','application/json');
        return $response;
    }

     //register new user
    public static function register2(Request $request) {
        try {
            // return response()->json(123);
            if (User::where('email', '=', $request->email)->count() > 0) {
               return response()->json(['errors'=>array(['code' => 422, 'message'=>'El correo se encuentra en uso.'])], 422);
            }
            $user = User::create([
                'name' => $request->username,
                'email' => $request->email,
                'password' => Hash::make($request->password),
                'type' => $request->type
            ]);

            if ($request->has('ownUser'))
                $user->parent_id = $request->ownUser;

            //save user
            $user->save();
            
            $userEcoomerce = Ecommerce::create([
                'brand_name' => $request->brand_name,
                'type_doc' => $request->type_doc,
                'user_id' => $user->id
            ]);

            // get id of user main
            $mainId = $request->user()->getIdMain($request->user());
            AuditoriaController::store($request->user()->name, ' Ha creado el usuario con el nombre: ' .$request->username, 'Usuarios', $mainId);

            //mail to user new
            Mail::to($user->email)->send(new CreateUserByAdmin($request->all()));
            
            $response = Response::make(json_encode(['data' => $user]), 201)->header('Content-Type','application/json');
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    // login user
    public function login(Request $request) {
        $user = User::where('name', $request->username)->first();
        if ($user) {
            if (Hash::check($request->password, $user->password)) {
                $token = $user->createToken('USER AUTH TOKEN')->accessToken;
                $response = ['token' => $token, 'user' => new UserResource($user)];
                return response($response, 200);
            } else {
                $response = ['errors' => array(['code' => 422, 'message'=>'La contraseña es incorrecta.'])];
                return response($response, 422);
            }
        } else {
            $response = ['errors' => array(['code' => 422, 'message'=>'El usuario no existe.'])];
            return response($response, 422);
        }
    }
    
    // logout
    public function logout(Request $request) {
        try {
            $token = $request->user()->token();
            $token->revoke();
            $response = ['message' => 'Has cerrado sesión correctamente.'];
            return response($response, 200);

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    public function index(Request $request) {
        if ($request->user()->type == 1)
            $users = User::orderBy('id', 'DESC')
            ->where('type', '!=', 3)
            ->where('type', '!=', 4)
            ->get();

        if ($request->user()->type == 2)
            $users = User::orderBy('id', 'DESC')
            ->where('type', '!=', 3)
            ->where('parent_id', '=', $request->user()->id)
            ->get();

        $response = ['users' => new UserCollection($users)];
        return response($response, 200);
    }

    public function updatePhoto(Request $request) {
        try {
            $user = User::findOrFail($request->user()->id);
            if ($user->photo != 'default-profile') {
                Helper::deleteFile($user->photo, 'profile');
            }
            $path = Helper::uploadImage($request->file, 'profile');
            $user->photo = $path;
            $user->save();
            $response = ['user' => new UserResource($user)];
            return response($response, 200);

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    //update profile by auth user
    public function updateProfile(Request $request) {
        try {
            $user = User::findOrFail($request->id);
            // update name
            if ($request->has('name')) {
                $user->update([
                    'name' => $request->name,
                ]);
            }
            // update email
            if ($request->has('email')) {
                $user->update([
                    'email' => $request->email,
                ]);
            }
            // update password
            if ($request->has('passwordN')) {
                $user->update([
                    'password' => Hash::make($request->passwordN),
                ]);
            }
            // get id of user main
            $mainId = $request->user()->getIdMain($request->user());
            AuditoriaController::store($request->user()->name, ' Ha modificado sus datos de acceso.', 'Usuarios', $mainId);
        
            $response = ['user' => new UserResource($user)];
            return response($response, 200);

        } catch (\Exception $e) {
            return response()->json(['errors' => array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    //edit user data
    public function edit(Request $request) {
        try {
            $user = User::findOrFail($request->id);
            $user->update([
                'name' => $request->username,
                'email' => $request->email,
                'type' => $request->type
            ]);
            if ($request->has('password')) {
                $user->update([
                    'password' => Hash::make($request->password),
                ]);
            }

            // get id of user main
            $mainId = $request->user()->getIdMain($request->user());
            AuditoriaController::store($request->user()->name, ' Ha modificado los datos de acceso del usuario: ' . $user->name, 'Usuarios', $mainId);

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    //delete user
    public function delete(Request $request) {
        try {
            $user = User::findOrFail($request->id);
            $user->delete();

            $parentUsers = User::where('parent_id', '=', $user->id)
            ->get();
            foreach ($parentUsers as $key => $value) {
                $value->delete();
            }

            $mainId = $request->user()->getIdMain($request->user());
            AuditoriaController::store($request->user()->name, 'Ha borrado el usuario con el nombre: ' .$user->username, 'Usuarios', $mainId);

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    //set Woocommerce data
    public function setWoocommerce(Request $request) {
        try {
            $request->user()->woocomerce_ck = $request->ck;
            $request->user()->woocomerce_cs = $request->cs;
            $request->user()->woocomerce_url = $request->url;
            $request->user()->save();

            $response = ['user' => new UserResource($request->user())];
            return response($response, 200);

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    // recovery password
    public function recovery(Request $request) {
        try {
            // validate if isset user in bbdd
            $user = User::where('name', '=', $request->username)->first();
            if ($user) {
                // valdate if user have a recovery request active
                $isset = DB::select('select * from reset_password where user_id = :id and expired > :now', ['id' => $user->id, 'now' => Carbon::now()]);

                if ($isset) {
                    $response = ['errors' => array(['code' => 422, 'message' => 'El usuario posee un proceso de recuperacion activo. Verifica tu correo.'])];
                    return response($response, 422);
                }

                // generate code of recuperation
                $token = Helper::generateReference(12);
                
                //generate date expired
                $now = Carbon::now();
                $expired = Carbon::now()->addMinutes(30);
                
                // proccess token in bbdd
                DB::insert('insert into reset_password (token, expired, user_id) values (?, ?, ?)', [$token, $expired, $user->id]);

                // send token to email
                $data['token'] = $token;
                Mail::to($user->email)->send(new RecoveryPassword($data));
                $response = ['success' => 'Se ha enviado un codigo de verificación a tu correo electronico.'];
                return response($response, 200);

            } else {
                $response = ['errors' => array(['code' => 422, 'message' => 'El usuario ingresado no existe en nuestros registro.'])];
                return response($response, 422);
            }

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    // change password with token
    public function changePasswordForToken(Request $request) {
        try {

            // validate if isset token in bbdd
            $token = DB::select('select * from reset_password where token = :token', ['token' => $request->token]);

            if ($token) {
                // validate if token expired or not
                if ($token[0]->expired < Carbon::now()) {
                    $response = ['errors' => array(['code' => 422, 'message' => 'El token ingresado expiro, por favor solicita uno nuevo.'])];
                    return response($response, 422);
                }

                // find urser of token
                $user = User::where('id', '=', $token[0]->user_id)->first();
                //if isset user change password
                if ($user) {
                    $user->password = Hash::make($request->password);
                    $user->save();

                    // delete used token
                    DB::table('reset_password')->where('id', '=', $token[0]->id)->delete();

                    $response = ['success' => 'Se ha cambiado la contraseña correctamente.'];
                    return response($response, 200);
                }

            } else {
                $response = ['errors' => array(['code' => 422, 'message' => 'El token ingresado no existe, por favor verificalo en tu correo electronico.'])];
                return response($response, 422);
            }

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

}
