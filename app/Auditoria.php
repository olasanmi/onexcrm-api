<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Auditoria extends Model
{
    //fillable
    protected $fillable = ['user', 'observation', 'entity', 'date', 'hour', 'user_id'];

    //hook
    public static function getAuditoriasByUserType($user) {
        if ($user->type == 1)
            $auditoria = Auditoria::orderBy('id', 'DESC')
            ->get();

        if ($user->type == 2)
            $auditoria = Auditoria::orderBy('id', 'DESC')
            ->where('user_id', '=', $user->id)
            ->get();

         if ($user->type == 4)
            $auditoria = Auditoria::orderBy('id', 'DESC')
            ->where('user_id', '=', $user->parent_id)
            ->get();

        return $auditoria;
    }

}
