<?php

namespace App\Http\Controllers;

use App\Seller;
use Illuminate\Http\Request;

use Response;

use App\Http\Resources\Seller\SellerCollection;
use App\Http\Resources\Seller\SellerResource2;

use App\Http\Controllers\AuditoriaController;

use App\Domicilio;

class SellerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $mainId = $request->user()->getIdMain($request->user());
        //get all seller
        $sellers = Seller::orderBy('id', 'DESC')
        ->where('user_id', '=', $mainId)
        ->get();
        $response = Response::make(json_encode(['success' => new SellerCollection($sellers)]), 200)->header('Content-Type','application/json');
        return $response;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //store
        try {
            $mainId = $request->user()->getIdMain($request->user());

            $seller = Seller::create([
                'name' => $request->name,
                'phone' => $request->phone,
                'nit' => $request->nit,
                'email' => $request->email,
                'address' => $request->address,
                'user_id' => $mainId
            ]);

            AuditoriaController::store($request->user()->name, 'Ha creado al vendedor: '. $seller->name, 'Vendedores', $mainId);

            $response = Response::make(json_encode(['success' => 'Se ha creado el vendedor correctamente.']), 200)->header('Content-Type','application/json');
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Seller  $seller
     * @return \Illuminate\Http\Response
     */
    public function show(Seller $seller)
    {
        $response = Response::make(json_encode(['success' => new SellerResource2($seller)]), 200)->header('Content-Type','application/json');
        return $response;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Seller  $seller
     * @return \Illuminate\Http\Response
     */
    public function edit(Seller $seller)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Seller  $seller
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        try {
            $mainId = $request->user()->getIdMain($request->user());

            $seller = Seller::findOrFail($request->id);
            $seller->update([
                'name' => $request->name,
                'phone' => $request->phone,
                'nit' => $request->nit,
                'email' => $request->email,
                'address' => $request->address
            ]);

            AuditoriaController::store($request->user()->name, 'Ha modificado al vendedor: '. $seller->name, 'Vendedores', $mainId);

            $response = Response::make(json_encode(['success' => 'Se ha modificado el vendedor correctamente.']), 200)->header('Content-Type','application/json');
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Seller  $seller
     * @return \Illuminate\Http\Response
     */
    public function destroy(Seller $seller, Request $request)
    {
        try {
            $mainId = $request->user()->getIdMain($request->user());

            // $seller = Seller::findOrFail($id);
            $seller->delete();

            AuditoriaController::store($request->user()->name, 'Ha eliminado al vendedor: '. $seller->name, 'Vendedores', $mainId);

            $response = Response::make(json_encode(['success' => 'Se ha eliminado el vendedor correctamente.']), 200)->header('Content-Type','application/json');
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    // toggleComision
    public function toggleComision(Request $request)
    {
        try {
            $domicilios = Domicilio::where('seller_id', '=', $request->id)
            ->get();
            foreach ($domicilios as $key => $value) {
                $value->is_pay_comision = true;
                $value->save();
            }
            
            $seller = Seller::findOrFail($request->id);
            $response = Response::make(json_encode(['success' => new SellerResource2($seller)]), 200)->header('Content-Type','application/json');
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors' => array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }
}
