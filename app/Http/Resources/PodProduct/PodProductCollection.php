<?php

namespace App\Http\Resources\PodProduct;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Http\Resources\Product\ProductsResource;

class PodProductCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->map(function ($models) {
            return [
                'id' => $models->id,
                'reference' => $models->reference,
                'name' => $models->name,
                'price'  => $models->price,
                'utilities'  => $models->utilities,
                'product_id'  => $models->product_id,
                'total'  => $models->total,
                'old'  => true,
                'newProduct'  => 0,
                'quantity'  => $models->quantity,
                'product' => new ProductsResource($models->product)
            ];        
        });
    }
}
