<?php

namespace App\Http\Controllers;

use App\Ecommerce;
use Illuminate\Http\Request;

use App\Http\Resources\User\UserResource;

use App\Helpers\Helper;

class EcommerceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try {
            $ecommerce = Ecommerce::where('user_id', '=', $request->user_id)->first();
            if (!isset($ecommerce->id)) {
                Ecommerce::create([
                    'nit' => $request->nit,
                    'brand_name' => $request->brand_name,
                    'phone' => $request->phone,
                    'type_doc' => $request->type_doc,
                    'commercial_brand' => $request->commercial_brand,
                    'economic_activity' => $request->economic_activity,
                    'user_id' => $request->user_id,
                    'legal_form' => $request->legal_form,
                    'iva' => $request->iva,
                    'email_invoice' => $request->email_invoice,
                    'email_notification' => $request->email_notification,
                    'web_url' => $request->web_url,
                    'address' => $request->address,
                    'state' => $request->state,
                    'city' => $request->city
                ]);

                $mainId = $request->user()->getIdMain($request->user());
                AuditoriaController::store($request->user()->name, ' Ha guardado los datos de su empresa.', 'Usuarios', $mainId);
                
            } else {
                if ($request->has('nit'))
                    $ecommerce->nit = $request->nit;

                // update brand name
                if ($request->has('brand_name'))
                    $ecommerce->brand_name = $request->brand_name;

                // update phone
                if ($request->has('phone'))
                    $ecommerce->phone = $request->phone;

                //if request have tipe docs
                if ($request->has('type_doc'))
                    $ecommerce->type_doc = $request->type_doc;

                //if request have trademark
                if ($request->has('commercial_brand'))
                    $ecommerce->commercial_brand = $request->commercial_brand;

                //if request have tipe docs
                if ($request->has('economic_activity'))
                    $ecommerce->economic_activity = $request->economic_activity;

                //complements
                if ($request->has('legal_form'))
                    $ecommerce->legal_form = $request->legal_form;

                if ($request->has('iva'))
                    $ecommerce->iva = $request->iva;

                if ($request->has('email_invoice'))
                    $ecommerce->email_invoice = $request->email_invoice;

                if ($request->has('email_notification'))
                    $ecommerce->email_notification = $request->email_notification;

                if ($request->has('web_url'))
                    $ecommerce->web_url = $request->web_url;

                if ($request->has('address'))
                    $ecommerce->address = $request->address;

                if ($request->has('state'))
                    $ecommerce->state = $request->state;

                if ($request->has('city'))
                    $ecommerce->city = $request->city;

                //save
                $ecommerce->save();

                // get id of user main
                $mainId = $request->user()->getIdMain($request->user());
                AuditoriaController::store($request->user()->name, ' Ha modificado los datos de su empresa.', 'Usuarios', $mainId);
            }
            
            $response = ['user' => new UserResource($request->user())];
            return response($response, 200);

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ecommerce  $ecommerce
     * @return \Illuminate\Http\Response
     */
    public function show(Ecommerce $ecommerce)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ecommerce  $ecommerce
     * @return \Illuminate\Http\Response
     */
    public function edit(Ecommerce $ecommerce)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ecommerce  $ecommerce
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ecommerce $ecommerce)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ecommerce  $ecommerce
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ecommerce $ecommerce)
    {
        //
    }

    // update rut of brand client
    public function updateRut(Request $request) {
        try {
            $ecommerce = Ecommerce::where('user_id', '=', $request->user_id)->first();

            if ($ecommerce->rut_path) {
                Helper::deleteFile($ecommerce->rut_path, 'rut');
            }
            $path = Helper::uploadImage($request->file, 'rut');
            $ecommerce->rut_path = $path;
            $ecommerce->save();

            $response = ['user' => new UserResource($request->user())];
            return response($response, 200);

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }
}
