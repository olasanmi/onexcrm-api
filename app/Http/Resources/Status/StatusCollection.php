<?php

namespace App\Http\Resources\Status;

use Illuminate\Http\Resources\Json\ResourceCollection;

class StatusCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
         return [
            'data' => $this->collection->transform(function ($models){
                return [
                    'id' => $models->id,
                    'status' => $models->status,
                    'observation' => $models->observation,
                    'domiciliario'  => $models->domiciliario,
                    'date'  => $models->created_at,
                ];
            })
        ];
    }
}
