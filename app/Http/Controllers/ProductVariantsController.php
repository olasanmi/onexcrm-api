<?php

namespace App\Http\Controllers;

use App\ProductVariants;
use App\Product;
use App\User;

use Illuminate\Http\Request;

use Response;

use App\Helpers\Helper;

use Automattic\WooCommerce\Client;

use App\Http\Resources\Product\ProductsResource;

use App\Http\Controllers\AuditoriaController;

use Illuminate\Support\Facades\Hash;

class ProductVariantsController extends Controller
{
    private $wooCommerce;
    
    function __construct() {
        $this->wooCommerce = new Client(
            env('WOOCOMMERCE_URL'),
            env('WOOCOMMERCE_CK'),
            env('WOOCOMMERCE_CS'),
            [
                'wp_api' => true,
                'version' => 'wc/v3'
            ]
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product = Product::where('woocomerce_id', '=', $_GET['id'])
        ->get();
        $listVariant = array();
        foreach ($product[0]->ProductVariants as $key => $value) {
            if ($value->stock_quantity > 0) {
                array_push($listVariant, $value);
            }
        }
        return response()->json(['success' => $listVariant], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            //set woocommerce data to send info
            $wooData = $request->user()->getWooKeys($request->user()->type, $request->user());
            $wooCommerce = new Client(
                $wooData['url'],
                $wooData['ck'],
                $wooData['cs'],
                [
                    'wp_api' => true,
                    'version' => 'wc/v3'
                ]
            );

            //get product
            $product = Product::find($request->product_id);

            $arrayAtribute = [];
            foreach ($request->attribute as $key => $value) {
                $attrData['id'] = $value['slug'];
                $attrData['option'] = $request->attributeTerms[$key]['name'];
                array_push($arrayAtribute, $attrData);
            }
            $data = [
                'status' => $request->status['id'],
                'description' => $request->description, 
                'sku' => $request->sku,
                'price' => $request->price,
                'regular_price' => $request->price,
                'manage_stock' => true,
                'stock_quantity' => $request->stock_quantity,
                'stock_status' => $request->stock_status['id'],
                'attributes' => $arrayAtribute
            ];

            $variationWoocomerce = $wooCommerce->post('products/' . $product->woocomerce_id .'/variations', $data);
            $variationWoocomerce = json_encode($variationWoocomerce, true);
            $decodeWoocomerce = json_decode($variationWoocomerce, true);

            if (isset($decodeWoocomerce['id'])) {
                // set term in product
                $label = "";
                foreach ($request->attributeTerms as $key => $value) {
                    if ($key > 0) {
                        $label = $label . ' - ' . $value['name'];
                    } else {
                        $label = $label . $value['name'];
                    }
                }
                // create variant 
                $variant = ProductVariants::create([
                    'status' => $request->status['id'],
                    'description' => $request->description,
                    'attribute' => $label,
                    'sku' => $request->sku,
                    'price' => $request->price,
                    'regular_price' => $request->price,
                    'stock_quantity' => $request->stock_quantity,
                    'stock_status' => $request->stock_status['id'],
                    'product_id' => $request->product_id
                ]);
                $variant->woocomerce_id = $decodeWoocomerce['id'];
                $variant->save();
            }

            $mainId = $request->user()->getIdMain($request->user());
            AuditoriaController::store($request->user()->name, 'Ha agregado la variacion' . $request->sku . ' para el producto: ' .$product->name, 'Productos-Variaciones', $mainId);

            $response = Response::make(json_encode(['success' => new ProductsResource($variant->Product)]), 201)->header('Content-Type','application/json');

            return $response;
        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductVariants  $productVariants
     * @return \Illuminate\Http\Response
     */
    public function show(ProductVariants $productVariants)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductVariants  $productVariants
     * @return \Illuminate\Http\Response
     */
    public function edit(ProductVariants $productVariants)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductVariants  $productVariants
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductVariants $ProductVariants)
    {
        //
        try {
            //set woocommerce data to send info
            $wooData = $request->user()->getWooKeys($request->user()->type, $request->user());
            $wooCommerce = new Client(
                $wooData['url'],
                $wooData['ck'],
                $wooData['cs'],
                [
                    'wp_api' => true,
                    'version' => 'wc/v3'
                ]
            );

            $variant = ProductVariants::find($request->id);

            $data = [
                'status' => $request->status,
                'description' => $request->description,
                'price' => ''.$request->price,
                'sku' => $request->sku,
                'regular_price' => ''.$request->price,
                'stock_quantity' => $request->stock_quantity,
                'stock_status' => $request->stock_status
            ];

            $variationWoocomerce = $wooCommerce->put('products/' . $variant->Product->woocomerce_id .'/variations/' . $variant->woocomerce_id, $data);
            
            $variant->update([
                'status' => $request->status,
                'description' => $request->description,
                'price' => $request->price,
                'regular_price' => $request->price,
                'sku' => $request->sku,
                'stock_quantity' => $request->stock_quantity,
                'stock_status' => $request->stock_status
            ]);

            $mainId = $request->user()->getIdMain($request->user());
            AuditoriaController::store($request->user()->name, 'Ha modificado la variacion' . $request->sku . ' para el producto: ' .$variant->Product->name, 'Productos-Variaciones', $mainId);

            $response = Response::make(json_encode(['success' => new ProductsResource($variant->Product)]), 201)->header('Content-Type','application/json');

            return $response;
        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductVariants  $productVariants
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        try {
            //set woocommerce data to send info
            $wooData = $request->user()->getWooKeys($request->user()->type, $request->user());
            $wooCommerce = new Client(
                $wooData['url'],
                $wooData['ck'],
                $wooData['cs'],
                [
                    'wp_api' => true,
                    'version' => 'wc/v3'
                ]
            );

            $variant = ProductVariants::find($id);
            if(isset($variant->woocomerce_id)) {
                $variationWoocomerce = $wooCommerce->delete('products/' . $variant->Product->woocomerce_id .'/variations/' . $variant->woocomerce_id, ['force' => true]);
            }
            $variant->delete();
            $response = Response::make(json_encode(['success' => new ProductsResource($variant->Product)]), 201)->header('Content-Type','application/json');
            return $response;
            
        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    public function fromErpPluginVariant(Request $request) {
        // return response()->json($request);
        try {
            if (!$request->has('username') or !$request->has('password')) {
                return response()->json(['errors' => 'Configura tus credenciales de ingreso.'], 422);
            } else {
                //get user
                $user = User::where('name', $request->username)->first();
                if ($user) {
                    if ($user->woocomerce_url != $request->urlRequest) {
                        return response()->json('El wordpress del que haces la petición no coincide con el configurado en la cuenta de tu erp', 422);
                    }
                    //get main id
                    $mainId = $user->getIdMain($user);
                    //validate user password with crm password
                    if (Hash::check($request->password, $user->password)) {
                        $product = Product::where('woocomerce_id', '=', $request->parentId)
                        ->where('user_id', '=', $mainId)
                        ->first();

                        $variant = ProductVariants::where('woocomerce_id', '=', $request->variantId)
                        ->where('product_id', '=', $product->id)
                        ->first();
                        
                        if ($request->stock_quantity > 0) {
                            $quantity = $request->stock_quantity;
                        } else {
                            $quantity = 100;
                        }
                        
                        if (is_null($request->price)) {
                            $price = 0;
                        } else {
                            $price = $request->price;
                        }

                        if (is_null($request->regularPrice)) {
                            $regular_price = 0;
                        } else {
                            $regular_price = $request->regularPrice;
                        }

                        $label = "";
                        foreach ($request->attr as $key => $value) {
                            if ($key > 0) {
                                $label = $label . ' - ' . $value;
                            } else {
                                $label = $label . $value;
                            }
                        }


                        if (!isset($variant->id)) {
                            $variant = ProductVariants::create([
                                'status' => $request->status,
                                'description' => $request->description,
                                'attribute' => $label,
                                'sku' => $request->sku,
                                'price' => $request->price,
                                'regular_price' => $request->regularPrice,
                                'stock_quantity' => $request->stock_quantity,
                                'stock_status' => $request->stock_status,
                                'woocomerce_id' => $request->variantId,
                                'product_id' => $product->id
                            ]);
                            
                            AuditoriaController::store($user->name, 'Ha agregado la variacion' . $variant->id . ' desde el plugin erp para el producto: ' .$product->name, 'Productos-Variaciones', $mainId);

                            $response = Response::make(json_encode('Se ha creado el variacion de producto correctamente en Onex Erp.'), 200)->header('Content-Type','application/json');

                        } else {
                            $variant->update([
                                'status' => $request->status,
                                'description' => $request->description,
                                'attribute' => $label,
                                'sku' => $request->sku,
                                'price' => $request->price,
                                'regular_price' => $request->regularPrice,
                                'stock_quantity' => $request->stock_quantity,
                                'stock_status' => $request->stock_status,
                                'woocomerce_id' => $request->variantId,
                                'product_id' => $product->id
                            ]);
                            
                            AuditoriaController::store($user->name, 'Ha modificado la variacion' . $variant->id . ' desde el plugin erp para el producto: ' .$product->name, 'Productos-Variaciones', $mainId);

                            $response = Response::make(json_encode('Se ha modificado el variacion de producto correctamente en Onex Erp.'), 200)->header('Content-Type','application/json');
                        }
                        return $response;

                    } else {
                        $response = ['errors' => array(['code' => 422, 'message'=>'La contraseña es incorrecta.'])];
                        return response($response, 422);
                    }
                } else {
                    $response = ['errors' => array(['code' => 422, 'message'=>'El usuario no existe.'])];
                    return response($response, 422);
                }
            }

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }
}
