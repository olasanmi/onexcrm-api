<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductStorage extends Model
{
    protected $table = 'product_storage';
    //fillable
    protected $fillable = ['product_id', 'storage_id', 'quantity'];
}
