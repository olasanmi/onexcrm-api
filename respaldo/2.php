$products = RequestController::getProducts($this->tokens, $this->page);
    	$data = json_decode($products, true);
    	foreach ($data as $key => $value) {
            $data = [
			    'name' => $value['Description'],
			    'type' => 'simple',
			    'price' => $value['PriceList1'],
			    'regular_price' => ''.$value['PriceList1'].'',
			    'description' => $value['Comments'],
			    'short_description' => $value['Comments'],
			    'categories' => [
			        [
			            'id' => 36
			        ]
			    ],
			    'images' => [
			        [
			            'src' => 'http://katikastienda.com/wp-content/uploads/2017/01/product-placeholder.jpg'
			        ]
			    ]
			];
			print_r($this->woocommerce->post('products', $data));
    	}
    	if (count($data) >= 1) {
            $this->page = $this->page + 1;
    		$this->getProducs();
    	} else {
    		return false;
    	}