<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    //fillable
    protected $fillable = ['name' , 'slug', 'woocomerce_id', 'user_id'];

    //relations
    public function terms() {
        return $this->hasMany('App\Term');
    }

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function Products() {
        return $this->belongsToMany('App\Product', 'category_product');
    }
}
