<?php

namespace App\Http\Resources\Domiciliario;

use Illuminate\Http\Resources\Json\ResourceCollection;

class DomiciliarioCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->transform(function ($models){
                return [
                    'id' => $models->id,
                    'name' => $models->name,
                    'dni' => $models->dni,
                    'phone'  => $models->phone,
                    'placa'  => $models->placa,
                    'photo'  => $models->photo,
                    'username' => $models->username,
                    'password' => $models->password,
                ];
            })
        ];
    }
}
