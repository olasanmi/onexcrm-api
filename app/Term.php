<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Term extends Model
{
    //fillable
    protected $fillable = ['name' , 'slugs', 'attribute_id', 'woocomerce_id'];

    //relations
    public function attribute() {
        return $this->belongsTo('App\Attribute');
    }

    public function Products() {
        return $this->belongsToMany('App\Product', 'attribute_product');
    }

}
