<?php

namespace App\Http\Controllers;

use App\PodOrderProduct;
use Illuminate\Http\Request;

class PodOrderProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PodOrderProduct  $podOrderProduct
     * @return \Illuminate\Http\Response
     */
    public function show(PodOrderProduct $podOrderProduct)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PodOrderProduct  $podOrderProduct
     * @return \Illuminate\Http\Response
     */
    public function edit(PodOrderProduct $podOrderProduct)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PodOrderProduct  $podOrderProduct
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PodOrderProduct $podOrderProduct)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PodOrderProduct  $podOrderProduct
     * @return \Illuminate\Http\Response
     */
    public function destroy(PodOrderProduct $podOrderProduct)
    {
        //
    }
}
