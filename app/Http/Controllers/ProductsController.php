<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Cache;

use Automattic\WooCommerce\Client;

class ProductsController extends Controller
{
    private $url, $cs, $ck, $woocomerce, $curl;
    public $domainCrm = '';

    //constructor
    public function __construct() {
        $this->url = 'https://xhobbies.dev';
    $this->ck = 'ck_13fdf68ef5c509a4805805376bf71ccc8d2c220a';
        $this->cs = 'cs_95005afa928f23d5017d886178d79fa89ac9e63f';
        $this->woocommerce = new Client(
            $this->url,
            $this->ck,
            $this->cs,
            ['version' => 'wc/v3']
        );
    }
    //store data from request
    public function store(Request $request) {
        //clear cache
    	Cache::flush();
        //set cache
        Cache::put('products', $request->products , 1123);
        //return data
        return response()->json(['success' => $request->products]);
    }

    public function get(Request $request) {
    	//get Cache
        $products = Cache::get('products');
        // get reference from products
        $reference = "" ;
        foreach ($products as $key => $value) {
            $reference .= $value['reference'] . ',';
        }
        // Obtenemos todos los productos de la lista de referencias
        $productsWoocomerce = $this->woocommerce->get('products/?sku=' . $reference);
        //from array
        $itemData = [];
        foreach($productsWoocomerce as $product){
            // Filtramos el array de origen por referencia
            $sku = $product->sku;
            $filter = array_filter($products, function($item) use($sku) {
                return $item['reference'] == $sku;
            });
            $filter = reset($filter);
            // Formamos el array a actualizar
            $itemData[] = [
                'id' => $product->id,
                'manage_stock' => true,
                'stock_quantity' => $filter['quantity'],
            ];
        }
        // actualizacion en lote
        $data = [
            'update' => $itemData,
        ];
        //return cache
        $update = $this->woocommerce->post('products/batch', $data);

        return response()->json(['success' => '']);
    }
}
