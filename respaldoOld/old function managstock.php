<?php
public function manageStock($product, $sku, $quantity, $user) {
        //set woocommerce data to send info
        $wooData = $user->getWooKeys($user->type, $user);
        $wooCommerce = new Client(
            $wooData['url'],
            $wooData['ck'],
            $wooData['cs'],
            [
                'wp_api' => true,
                'version' => 'wc/v3'
            ]
        );
        //filter product
        $product = Product::where('woocomerce_id', '=', $product)
        ->get();
        if (count($product) > 0) {
            switch ($product[0]->type) {
                case 'simple':
                    $product[0]->stock_quantity = ($product[0]->stock_quantity - $quantity);
                    
                    if ($product[0]->stock_quantity == 0) {
                        $product[0]->stock_status = 'outofstock';
                    }
                    
                    $product[0]->save();

                    //update product stock in woocommerce
                    $data = [
                        'stock_quantity' => $product[0]->stock_quantity,
                        'stock_status' => $product[0]->stock_status
                    ];
                    
                    $wooCommerce->put('products/'.$product[0]->woocomerce_id, $data);
                    break;

                case 'variable':
                    $product[0]->stock_quantity = ($product[0]->stock_quantity - $quantity);
                    
                    if ($product[0]->stock_quantity == 0) {
                        $product[0]->stock_status = 'outofstock';
                    }
                    
                    $product[0]->save();

                    //update product stock in woocommerce
                    $data = [
                        'stock_quantity' => $product[0]->stock_quantity,
                        'stock_status' => $product[0]->stock_status
                    ];
                    
                    $wooCommerce->put('products/'.$product[0]->woocomerce_id, $data);
                    // update variant
                    $variant = ProductVariants::where('sku', '=', $sku)
                    ->get();
                    $variant[0]->stock_quantity = ($variant[0]->stock_quantity - $quantity);
                    if ($variant[0]->stock_quantity == 0) {
                        $variant[0]->stock_status = 'outofstock';
                    }
                    $variant[0]->save();
                    //update product stock in woocommerce
                    $data = [
                        'stock_quantity' => $variant[0]->stock_quantity,
                        'stock_status' => $variant[0]->stock_status
                    ];

                    $wooCommerce->put('products/' . $variant[0]->Product->woocomerce_id .'/variations/' . $variant[0]->woocomerce_id, $data);
                    break;
                
                default:
                    # code...
                    break;
            }
        }
    }