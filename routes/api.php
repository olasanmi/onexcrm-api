<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'v1'], function () {
    Route::post('/domicilio/fromWoocomerce','DomicilioController@fromWoocomerce');
    Route::post('/track','DomicilioController@tracker');
    Route::get('/soap-client','DomicilioController@createSoapClient');
    
    Route::get('/soap-client/getReference','DomicilioController@reference');
    Route::resource('estado','EstadosController');
    Route::resource('estados','EstadosController');
});

//api version 2
Route::group(['prefix' => 'v2'], function () {
    // auth routes with out api middleware
    Route::post('login', 'AuthController@login');
    Route::post('recovery', 'AuthController@recovery');
    Route::post('change/password', 'AuthController@changePasswordForToken');

    Route::group(['middleware' => 'auth:api'], function() {
        //auth
        Route::post('register', 'AuthController@register2');
        Route::post('logout', 'AuthController@logout');
        
        //users request
        Route::get('userAll', 'AuthController@index');
        Route::post('editUser', 'AuthController@edit');
        Route::post('deleteUser', 'AuthController@delete');
        Route::post('set/woocommerce', 'AuthController@setWoocommerce');

        //domiciliario
        Route::resource('domiciliario','DomiciliarioController');
        Route::post('/domiciliario/update','DomiciliarioController@update');
        Route::post('/domiciliario/delete','DomiciliarioController@destroy');

        //categories
        Route::resource('category','CategoryController');
        Route::post('categories/import','CategoryController@importCategories');

        // product
        Route::resource('products','ProductController');
        Route::post('product/search', 'ProductController@search');
        Route::get('product/search/{ref}', 'ProductController@searchRef');
        Route::post('product/import','ProductController@importProducts');
        Route::get('product/list', 'ProductController@paginateProduct');
        Route::get('get/all/products', 'ProductController@getAllForReport');

        //poduct variant
        Route::resource('product-variants','ProductVariantsController');
        Route::get('product-variants/delete/{id}','ProductVariantsController@destroy');

        // product attributes global routes

        //ser atributes to produts
        Route::resource('product-attribute','AttributeProductController');
        Route::get('product-attribute/delete/{id}/{product}','AttributeProductController@destroy');
        Route::post('push/product-attribute','AttributeProductController@update');

        //auditorias routes
        Route::resource('auditorias','AuditoriaController');

        //domicilios
        Route::resource('domicilio','DomicilioController');
        Route::post('/domicilio/update','DomicilioController@update');
        Route::get('/domicilio-month','DomicilioController@orderMonth');
        Route::get('/domicilio-year','DomicilioController@orderYear');
        Route::post('/domicilio/factura','DomicilioController@generatePdf');
        Route::post('/domicilio/delete','DomicilioController@destroy');
        //############################
        Route::post('/domicilio/filter-date','DomicilioController@filterDateRange');
        Route::get('/domicilio-history/getAll','DomicilioController@getHistory');
        Route::get('/domicilio-history/cancel','DomicilioController@getHistoryCancel');
        Route::get('/domicilio-history/warranty','DomicilioController@getHistoryWarranty');

        //channels
        Route::resource('channel','ChannelController');
        Route::post('channel/update','ChannelController@update');
        
        //channels
        Route::resource('typePayment','TypePaymentController');

        //storage
        Route::resource('storage','StorageController');

        //product storage
        Route::post('product/storage','StorageController@setProductStorage');
        Route::post('product/storage/update','StorageController@updateProductStorage');
        Route::delete('product/storage/{id}','StorageController@delProductStorage');

        //clientes
        Route::get('clients/{dni}','ClientController@show');
        Route::get('clients','ClientController@index');
        Route::resource('client','ClientController');
        Route::get('clients/filter/{id}','ClientController@filterById');
        Route::post('clients/add','ClientController@createNewClient');
        Route::post('clients/edit','ClientController@editClient');

        //estados
        Route::resource('estado','EstadosController');
        Route::resource('estados','EstadosController');

        //liquidar
        Route::post('/domicilio/liquidar','DomicilioController@liquidarPdf');

        //auth request
        Route::post('user/edit','AuthController@updateProfile');
        Route::post('user/brand/edit','EcommerceController@store');
        Route::post('brand/complement','EcommerceController@updateBrandComplement');

        //user photo edit request
        Route::post('/profile/photo','AuthController@updatePhoto');
        Route::post('/profile/rut','EcommerceController@updateRut');

        //seller routes
        Route::resource('seller','SellerController');
        Route::post('seller/update','SellerController@update');

        //pod routes
        Route::resource('podorder','PodOrderController');
        Route::get('podorder/filter/{id}','PodOrderController@show');

        // reports
        Route::get('report/ventas-diarias','ReportController@diary');
        Route::get('report/ventas-cliente','ReportController@client');
        Route::get('report/ventas-vendedors','ReportController@sellers');
        Route::get('report/ventas-products','ReportController@products');

        // report pdf
        Route::post('report/pdf/ventas-diarias','ReportPdfController@pdfDiary');
        Route::post('report/pdf/ventas-item','ReportPdfController@pdfItemsSeller');
        Route::post('report/pdf/ventas-cliente','ReportPdfController@pdfClientsSeller');
        Route::post('report/pdf/item-rent','ReportPdfController@pdfItemRent');
        Route::post('report/pdf/vendedor-ventas','ReportPdfController@pdfSellerSellers');

        // attribute
        Route::resource('attribute','AttributeController');
        Route::post('attributes/import','AttributeController@importAttributes');

        // terms attribute
        Route::resource('terms','TermsController');
        Route::post('/delete/terms/in-product','TermProductController@deleteTermInProduct');

    });

    //plugin erp request    
    // product from plugins erp
    Route::post('product/from/plugin/test','ProductController@fromErpPlugin');
    Route::post('product/from/plugin','ProductController@fromErpPlugin');
    Route::post('productVariant/from/plugin/test','ProductVariantsController@fromErpPluginVariant');
    Route::post('productVariant/from/plugin','ProductVariantsController@fromErpPluginVariant');
});