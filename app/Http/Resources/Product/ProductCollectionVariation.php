<?php

namespace App\Http\Resources\Product;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ProductCollectionVariation extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->map(function ($models) {
            return [
                'id' => $models->id,
                'name' => $models->name,
                'slug' => $models->slug,
                'type' => $models->type,
                'status' => $models->status,
                'description' => $models->description,
                'short_description' => $models->short_description,
                'featured' => $models->featured,
                'sku' => $models->sku,
                'woocomerce_id' => $models->woocomerce_id,
                'price' => $models->price,
                'value' => $models->utilities,
                'regular_price' => $models->regular_price,
                'sale_price' => $models->sale_price,
                'stock_quantity' => $models->stock_quantity,
                'stock_status' => $models->stock_status,
                    // 'category' => new CategoryResource($models->Category),
                'variants' => count($models->ProductVariants),
                    // 'storages' => $models::getStorages($models)
            ];
        });
    }
}
