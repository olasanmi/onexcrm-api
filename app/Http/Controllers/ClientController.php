<?php

namespace App\Http\Controllers;

use App\Client;
use Illuminate\Http\Request;

use App\Http\Resources\Domicilio\DomicilioCollection;

use Response;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        if ($request->user()->type == 1)
            $clients = Client::orderby('id', 'DESC')->get();
        
        $mainId = $request->user()->getIdMain($request->user());
        if ($request->user()->type != 1)
            $clients = Client::orderby('id', 'DESC')->where('user_id', '=', $mainId)->get();

        return response()->json(['success' => $clients]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    static function stores($name, $dni, $phone, $email, $address, $user)
    {
        //
        $validateIfIsset = Client::where('dni', '=', $dni)
        ->first();
        if (!isset( $validateIfIsset)) {
            $user = Client::create([
                'name' => $name,
                'dni' => $dni,
                'phone' => $phone,
                'email' => $email,
                'address' => $address,
                'user_id' => $user
            ]);
            return $user;
        } else {
            return $validateIfIsset;
        }
    }

    public function show($dni)
    {
        //
        $client = Client::getClientData($dni);
        if (isset($client->id)) {
            $response = Response::make(json_encode(['success' => $client]), 200)->header('Content-Type','application/json');
        } else {
            $response = Response::make(json_encode(['success' => 'No existe ningun cliente']), 200)->header('Content-Type','application/json');
        }

        return $response;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        //delete client
        try {
            $client->delete();
            
            $response = Response::make(json_encode(['success' => 'Se ha eliminado el cliente correctamente']), 200)->header('Content-Type','application/json');
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors' => array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    // filter client and her orders
    public function filterById(Request $request, $id) {
        try {
            $client = Client::findOrFail($id);
            $orders = $client::getAllPedidos($client->dni);
            $client->orders = new DomicilioCollection($orders);
            

            $response = Response::make(json_encode(['success' => $client]), 200)->header('Content-Type','application/json');
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors' => array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    // create new client in our bbdd
    public function createNewClient(Request $request) {
        try {
            $mainId = $request->user()->getIdMain($request->user());
            
            $user = Client::create([
                'name' => $request->name,
                'dni' => $request->dni,
                'phone' => $request->phone,
                'email' => $request->email,
                'address' => $request->address,
                'user_id' => $mainId
            ]);
            $response = Response::make(json_encode(['success' => 'Se ha creado el cliente correctamente.']), 200)->header('Content-Type','application/json');
            return $response;


        } catch (\Exception $e) {
            return response()->json(['errors' => array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    // edit a client in our bbdd
    public function editClient(Request $request) {
        try {
            $client = Client::findOrFail($request->id);
            
            $client->update([
                'name' => $request->name,
                'dni' => $request->dni,
                'phone' => $request->phone,
                'email' => $request->email,
                'address' => $request->address
            ]);

            $response = Response::make(json_encode(['success' => 'Se ha modificado el cliente correctamente.']), 200)->header('Content-Type','application/json');
            return $response;


        } catch (\Exception $e) {
            return response()->json(['errors' => array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }
     
}
