<?php

namespace App\Http\Resources\Terms;

use Illuminate\Http\Resources\Json\ResourceCollection;

class TermsCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->map(function ($models) {
            return [
                'id' => $models->id,
                'name' => $models->name,
                'slug' => $models->slugs,
                'woocomerce_id' => $models->woocomerce_id,
                'attribute_id' => $models->attribute_id,
                'chip' => true
            ];
        });
    }
}
