<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductVariantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_variants', function (Blueprint $table) {
            $table->id();
            $table->string('status', 250)->nullable();
            $table->longText('description')->nullable();
            $table->string('sku', 250)->nullable();
            $table->float('price', 250, 2)->default(0);
            $table->float('regular_price', 250, 2)->default(0);
            $table->float('sale_price', 250, 2)->default(0);
            $table->integer('stock_quantity')->default(0);
            $table->string('stock_status', 250)->nullable();
            $table->string('attribute')->default(0);
            $table->bigInteger('woocomerce_id')->default(0);
            $table->unsignedBigInteger('product_id')->nullable()->unsigned();
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_variants');
    }
}
