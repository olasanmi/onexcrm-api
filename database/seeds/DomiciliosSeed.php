<?php

use Illuminate\Database\Seeder;

class DomiciliosSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(App\Domicilio::class, 300)->create()->each(function($u) {
			$u->products()->save(factory(App\DomicilioProduct::class)->make());
		});
    }
}
