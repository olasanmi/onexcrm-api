<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TermProduct extends Model
{
    //
    protected $table = "term_product";
    //fillable
    protected $fillable = ['term_id' , 'product_id'];
}
