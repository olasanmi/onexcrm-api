<?php

namespace App\Http\Resources\Seller;

use Illuminate\Http\Resources\Json\ResourceCollection;

class SellerCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->map(function ($models) {
                return [
                    'id' => $models->id,
                    'name' => $models->name,
                    'phone' => $models->phone,
                    'email' => $models->email,
                    'nit' => $models->nit,
                    'address' => $models->address
                ];
            });
    }
}
