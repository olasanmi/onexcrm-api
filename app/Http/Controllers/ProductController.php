<?php

namespace App\Http\Controllers;

ini_set('max_execution_time', '5000000000000');

use App\Product;
use App\ProductVariants;
use App\ProductStorage;
use App\Storage;

use App\User;

use App\Category;
use App\CategoryProduct;

use App\AttributeProduct;
use App\Attribute;

use App\Term;
use App\TermProduct;

use Illuminate\Http\Request;

use App\Http\Resources\Product\ProductsCollection;
use App\Http\Resources\Product\ProductCollectionVariation;
use App\Http\Resources\Product\ProductsResource;

use Response;

use App\Helpers\Helper;

use Automattic\WooCommerce\Client;

use App\Http\Controllers\AuditoriaController;

use Illuminate\Support\Facades\Hash;

use App\Jobs\ProductVariant\ProductVariantImportation;
use App\Jobs\Product\ProductVariantCreate;

use App\DomicilioProduct;
use App\PodOrderProduct;

class ProductController extends Controller
{
    private $wooCommerce;
    
    function __construct() {
        $this->wooCommerce = new Client(
            env('WOOCOMMERCE_URL'),
            env('WOOCOMMERCE_CK'),
            env('WOOCOMMERCE_CS'),
            [
                'wp_api' => true,
                'version' => 'wc/v3'
            ]
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $products = Product::getProductsByUserType($request->user());

        $response = Response::make(json_encode(['success' => new ProductCollectionVariation($products)]), 200)->header('Content-Type','application/json');

        return $response;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            // get main id of user
            $mainId = $request->user()->getIdMain($request->user());
            // set data for create news products
            $data = [
                'create' => []
            ];
            // initializate variant product array
            $variantProducts = [];
            // loops in product for set data
            foreach ($request->products as $key => $value) {
                $product['name'] = $value['name'];
                $product['sku'] = $value['reference'];
                $product['type'] = $value['type']['id'];
                $product['regular_price'] = $value['price'];
                $product['description'] = $value['description'];
                $product['status'] = $value['status']['id'];
                $product['price'] = $value['price'];
                $product['manage_stock'] = true;
                $product['stock_quantity'] = $value['quantity'];
                $product['categories'] = [];
                // set product categories
                foreach ($value['categories'] as $cat => $categories) {
                    $category['id'] = $categories['woocomerce_id'];
                    array_push($product['categories'], $category);
                }
                // set attributes si product is variable
                if ($value['type']['id'] == 'variable') {
                    //push product to variant product array
                    array_push($variantProducts, $value);
                    // set the product attribute
                    $product['attributes'] = [];
                    // loops in product attributes
                    foreach ($value['attributes'] as $attr => $attributes) {
                        $attribute['id'] = $attributes['selectedAttr']['woocomerce_id'];
                        $attribute['variation'] = true;
                        $attribute['visible'] = true;
                        // set attriute terms options
                        $attribute['options'] = [];
                        foreach ($attributes['selectedAttr']['options'] as $term => $terms) {
                            array_push($attribute['options'], $terms['name']); 
                        }
                        array_push($product['attributes'],  $attribute);
                    }
                }
                // push product to data create
                array_push($data['create'], $product);
            }
            // save products on woocomercer
            $woocommerceProduct = Helper::createInWoocomerce($data, 'products/batch', $request->user(), 1);
            // validate products creation in woocomerce.
            foreach ($woocommerceProduct['create'] as $key => $value) {
                if (isset($value['error'])) {
                    return response()->json(['errors' => array(['code' => 400, 'message' => $value['error']['message'] . ' en ' . $data['create'][$key]['sku'] ])], 400);
                }
            }
            // save product variant in woocomerce
            foreach ($request->products as $key => $value) {
                // get the woocomerce data with reference and sku
                $productWoocommerce = collect($woocommerceProduct['create'])->where('sku', $value['reference'])->first();
                // save product in bbdd
                $prod = Product::create([
                    'name' => $value['name'],
                    'type' => $value['type']['id'],
                    'status' => $value['status']['id'],
                    'description' => $value['description'],
                    'price' => $value['price'],
                    'sku' => $value['reference'],
                    'regular_price' => $value['price'],
                    'stock_quantity' => $value['quantity'],
                    'stock_status' => $value['stock_status']['id'],
                    'utilities' => $value['value'],
                    'woocomerce_id' => $productWoocommerce['id'],
                    'user_id' => $mainId
                ]);
                // set product categories
                foreach ($value['categories'] as $cat => $categories) {
                    CategoryProduct::create([
                        'category_id' => $categories['id'],
                        'product_id' => $prod->id
                    ]);
                }
                // set attributes
                foreach ($value['attributes'] as $attr => $attributes) {
                    $attribute = Attribute::where('woocomerce_id', '=', $attributes['selectedAttr']['woocomerce_id'])->first();
                        AttributeProduct::create([
                            'product_id' => $prod->id,
                            'attribute_id' => $attributes['selectedAttr']['id']
                        ]);
                    foreach ($attributes['selectedAttr']['options'] as $term => $terms) {
                        TermProduct::create([
                            'product_id' => $prod->id,
                            'term_id' => $terms['id'],
                        ]);
                    }
                }
                // set new prod variant
                if ($value['type']['id'] == 'variable') {
                    $dataJ['variants'] = $value['variants'];
                    $dataJ['user'] = $request->user();
                    $dataJ['prod'] = $prod;
                    ProductVariantCreate::dispatch($dataJ);
                }
                //save auditoria
                AuditoriaController::store($request->user()->name, 'Ha creado el producto: ' . $prod->name, 'Productos', $mainId);
            }

            // return response
            $response = Response::make(json_encode(['success' => 'Se han creado los productos correctamente.']), 200)->header('Content-Type','application/json');
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        try {
            $response = Response::make(json_encode(['success' => new ProductsResource($product)]), 200)->header('Content-Type','application/json');

            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        try {
            // validate f user is admin
            if ($request->user()->type == 1) {
                return response()->json(['errors' => array(['code' => 422, 'message' => 'Por el momento el administrador no puede modificar productos.'])], 422);
            }
            //validate if product categories is in request categories if no is in delee it
            foreach ($product->Categories as $key => $value) {
                $category = collect($request->categories)->where('id', $value['id'])->first();
                if (!isset($category->id)) {
                    $delCat = CategoryProduct::where('product_id', '=', $product->id)
                    ->where('category_id', '=', $value['id'])
                    ->first();
                    $delCat->delete();
                }
            }

            //update product in our bbdd
            $product->update([
                'name' => $request->name,
                'type' => $request->type['id'],
                'status' => $request->status['id'],
                'description' => $request->description,
                'short_description' => $request->short_description,
                'price' => $request->price,
                'sku' => $request->sku,
                'utilities' => $request->value,
                'regular_price' => $request->price,
                'stock_quantity' => $request->stock_quantity,
                'stock_status' => $request->stock_status['id']
            ]);
            //data to send
            $data = [
                'name' => $request->name,
                'type' => $request->type['id'],
                'regular_price' => $request->price,
                'description' => $request->description,
                'short_description' => $request->short_description,
                'status' => $request->status['id'],
                'sku' => $request->sku,
                'manage_stock' => true,
                'price' => (string)$request->price,
                'regular_price' => (string)$request->price,
                'stock_quantity' => $request->stock_quantity,
                'stock_status' => $request->stock_status['id'],
                'categories' => []
            ];

            // set categories
            foreach ($request->categories as $key => $value) {
                $issetCat = CategoryProduct::where('product_id', '=', $product->woocomerce_id)
                ->where('category_id', '=', $value['id'])
                ->first();
                if (!isset($issetCat->id)) {
                    CategoryProduct::create([
                        'category_id' => $value['id'],
                        'product_id' => $product->id,
                    ]);
                }
                $cat['id'] = $value['woocomerce_id'];
                array_push($data['categories'], $cat);
            }

            // save products on woocomercer
            $woocommerceProduct = Helper::createInWoocomerce($data, 'products/'.$product->woocomerce_id, $request->user(), 2);

            $mainId = $request->user()->getIdMain($request->user());
            AuditoriaController::store($request->user()->name, 'Ha modificado el producto: ' .$product->name, 'Productos', $mainId);

            $response = Response::make(json_encode(['success' => 'Se ha modificado el producto correctamente.']), 200)->header('Content-Type','application/json');

            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Product $product)
    {
        try {
            //delete product
            if (isset($product->woocomerce_id)) {
                $woocomerceTerm = Helper::deleteInWoocomerce('products/' . $product->woocomerce_id, $request->user());
            }

            //delete al product domicilio i pod order product 
            DomicilioProduct::where('product_id', '=', $product->id)->delete();
            PodOrderProduct::where('product_id', '=', $product->id)->delete();

            // borrar producto en nuestra base de datos
            $product->delete();

            // guardar auditoria
            $mainId = $request->user()->getIdMain($request->user());
            AuditoriaController::store($request->user()->name, 'Ha eliminado el producto: ' .$product->name, 'Productos', $mainId);

            //return response
            $products = Product::getProductsByUserType($request->user());

            $response = Response::make(json_encode(['success' => new ProductsCollection($products)]), 200)->header('Content-Type','application/json');

            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    //mange stock
    public function manageStock($products, $user) {
        //set woocommerce data to send info
        $wooData = $user->getWooKeys($user->type, $user);
        $wooCommerce = new Client(
            $wooData['url'],
            $wooData['ck'],
            $wooData['cs'],
            [
                'wp_api' => true,
                'version' => 'wc/v3'
            ]
        );
        //simple data array
        $dataSimple = [
            'update'=> []
        ];
        //loop in prods
        foreach ($products as $key => $value) {
            //get product data
            $product = Product::findOrFail($value['product_id']);
            //switch product type
            if ($product->stock_quantity > 0) {
                //update storage stock
                if (isset($value['selectedStorage'])) {
                    $storage = ProductStorage::where('storage_id', '=', $value['selectedStorage'])
                    ->where('product_id', '=', $product->id)
                    ->first();
                    if (isset($storage->id)) {
                        if ($storage->quantity > 0 and $storage->quantity >= $value['quantity']) {
                            $storage->quantity = ($storage->quantity - $value['quantity']);
                        }
                        $storage->save();
                    }
                }
                //update product stock

                $product->stock_quantity = ($product->stock_quantity - $value['quantity']);
                        //set out stock if producto quantiti is 0
                if ($product->stock_quantity == 0) {
                    $product->stock_status = 'outofstock';
                }
                        //save product in our bbdd
                $product->save();
                        //set data for batch update
                $pData = [
                    'stock_quantity' => $product->stock_quantity,
                    'stock_status' => $product->stock_status,
                    'id' => $product->woocomerce_id
                ];
                array_push($dataSimple['update'], $pData);
            }
            //if product is variable
            if ($product->type == 'variable') {
                //get variant data bi sku
                $variantData = ProductVariants::findOrFail($value['idVariant']);
                //if quiantiti hight 0
                if ($variantData->stock_quantity > 0) {
                    //discount variant data
                    $variantData->stock_quantity = ($variantData->stock_quantity - $value['quantity']);
                    //set out stock variant if quantiti lower than 0
                    if ($variantData->stock_quantity == 0) {
                        $variantData->stock_status = 'outofstock';
                    }
                    //save variant data
                    $variantData->save();
                    //send data to woocomerce
                    $dataVariant = [
                        'stock_quantity' => $variantData->stock_quantity,
                        'stock_status' => $variantData->stock_status
                    ];

                    $wooCommerce->put('products/' . $variantData->Product->woocomerce_id .'/variations/' . $variantData->woocomerce_id, $dataVariant);
                }
            }
        }
        // update simple product with bacth update
        $wooCommerce->post('products/batch', $dataSimple);
    }

    public function manageStockFromUpdateItem($products, $user) {
        //set woocommerce data to send info
        $wooData = $user->getWooKeys($user->type, $user);
        $wooCommerce = new Client(
            $wooData['url'],
            $wooData['ck'],
            $wooData['cs'],
            [
                'wp_api' => true,
                'version' => 'wc/v3'
            ]
        );
        //findProduct
        //simple data array
        $dataSimple = [
            'update'=> []
        ];
        foreach ($products as $key => $value) {
            //get product data
            $product = Product::findOrFail($value['product_id']);
            //upate storge data
            if (isset($value['selected_store'])) {
                $storage = ProductStorage::where('storage_id', '=', $value['selected_store'])
                ->where('product_id', '=', $product->id)
                ->first();
                if (isset($storage->id)) {
                    if ($storage->quantity >= 0) {
                        if ($value['operation'] == 1)
                            $storage->quantity = ($storage->quantity - $value['newQuantity']);
                            $storage->save();
                        if ($value['operation'] == 2)
                            $storage->quantity = ($storage->quantity + $value['newQuantity']);
                            $storage->save();
                    }
                }
            }
            //finish update stock of storage
            //start update product global storage
            if ($value['operation'] == 1) {
                $product->stock_quantity = ($product->stock_quantity - $value['newQuantity']);
            } else {
                $product->stock_quantity = ($product->stock_quantity + $value['newQuantity']);
            }
            if ($product->stock_quantity == 0) {
                $product->stock_status = 'outofstock';
            }
            $product->save();
            //end global storage update
            //set woocommerce data to update
            $pData = [
                'stock_quantity' => $product->stock_quantity,
                'stock_status' => $product->stock_status,
                'id' => $product->woocomerce_id
            ];
            array_push($dataSimple['update'], $pData);
            //end set woocomerce data update

            //if product is variable
            if ($product->type == 'variable') {
                //get variant data bi sku
                $variantData = ProductVariants::findOrFail($value['idVariant']);
                //if quiantity > 0
                if ($variantData->stock_quantity > 0) {
                    //discount variant data
                    if ($value['operation'] == 1) {
                        $variantData->stock_quantity = ($variantData->stock_quantity - $value['newQuantity']);
                    } else {
                        $variantData->stock_quantity = ($variantData->stock_quantity + $value['newQuantity']);
                    }
                    //set out stock variant if quantiti lower than 0
                    if ($variantData->stock_quantity == 0) {
                        $variantData->stock_status = 'outofstock';
                    }
                    //save variant data
                    $variantData->save();
                    //send data to woocomerce
                    $dataVariant = [
                        'stock_quantity' => $variantData->stock_quantity,
                        'stock_status' => $variantData->stock_status
                    ];

                    $wooCommerce->put('products/' . $variantData->Product->woocomerce_id .'/variations/' . $variantData->woocomerce_id, $dataVariant);
                }
            }
        }
        // update simple product with bacth update
        $wooCommerce->post('products/batch', $dataSimple);
        //end batch update
    }

    //search prods by name
    public function search(Request $request) {
        //get main id
        $mainId = $request->user()->getIdMain($request->user());
        $products = Product::where('user_id', '=', $mainId)
        ->where('name', 'like', '%' . $request->name .'%')
        ->get();
        $response = Response::make(json_encode(['success' => new ProductsCollection($products)]), 200)->header('Content-Type','application/json');

        return $response;
    }

    //search prods by ref
    public function searchRef(Request $request, $ref) {
        //get main id
        if (isset($ref)) {
            $mainId = $request->user()->getIdMain($request->user());
            $products = Product::where('user_id', '=', $mainId)
            ->where('sku', 'like', '%' . $ref .'%')
            ->get();
            $response = Response::make(json_encode(['success' => new ProductsCollection($products)]), 200)->header('Content-Type','application/json');

            return $response;
        }
    }

    // import products for user
    public function importProducts(Request $request) {
        try {
            //set woocommerce data to send info
            $wooData = $request->user()->getWooKeys($request->user()->type, $request->user());
            $wooCommerce = new Client(
                $wooData['url'],
                $wooData['ck'],
                $wooData['cs'],
                [
                    'wp_api' => true,
                    'version' => 'wc/v3'
                ]
            );
            //get main user id
            $mainId = $request->user()->getIdMain($request->user());
            $allNewProducts = [];
            //lops all products
            foreach ($request->products as $key => $value) {
                $product = Product::where('woocomerce_id', '=', $value['id'])
                ->where('user_id', '=', $mainId)
                ->first();
                //if don`t issets product id create product
                $newProduct = [];
                if ($value['stock_quantity'] > 0) {
                    $quantity = $value['stock_quantity'];
                } else {
                    $quantity = 100;
                }
                if (!isset($product->id)) {
                    $newProduct = [
                        'name' => $value['name'],
                        'type' => $value['type'],
                        'status' => $value['status'],
                        'description' => $value['description'],
                        'short_description' => $value['short_description'],
                        'price' => $value['price'],
                        'slug' => $value['img'],
                        'sku' => $value['sku'],
                        'regular_price' => $value['regular_price'],
                        'stock_quantity' => $quantity,
                        'stock_status' => $value['stock_status'],
                        'woocomerce_id' => $value['id'],
                        'user_id' => $mainId
                    ];
                    array_push($allNewProducts, $newProduct);
                } else {
                    $product->update([
                        'name' => $value['name'],
                        'type' => $value['type'],
                        'status' => $value['status'],
                        'description' => $value['description'],
                        'short_description' => $value['short_description'],
                        'price' => $value['price'],
                        'slug' => $value['img'],
                        'sku' => $value['sku'],
                        'regular_price' => $value['regular_price'],
                        'stock_quantity' => $quantity,
                        'stock_status' => $value['stock_status'],
                        'woocomerce_id' => $value['id']
                    ]);
                }
            }
            // insertamos la data del producto
            $products = Product::insert($allNewProducts);
            // procesamos las categorias
            foreach ($request->products as $key => $value) {
                // buscamos el producto por el wooomerce id
                $productInBbDd = Product::where('woocomerce_id', '=', $value['id'])
                ->where('user_id', '=', $mainId)
                ->first();
                //procesamos la bodega po defecto en el product
                // configuramos la bodega por defecto
                $default = Storage::where('user_id', '=', $mainId)
                ->where('is_default', '=', 1)
                ->first();
                //valido la bodega por defecto para el producto y evitar duplicaciones
                $validateStorage = ProductStorage::where('product_id', '=', $productInBbDd->id)
                ->where('storage_id', '=', $default->id)
                ->first();
                if (!isset($validateStorage->id)) {
                    ProductStorage::create([
                        'product_id' => $productInBbDd->id,
                        'storage_id' => $default->id,
                        'quantity' => $productInBbDd->stock_quantity
                    ]);
                }
                // proccess all categories
                foreach ($value['categories'] as $cat => $category) {
                    // saco la categoria
                    $categoryBbDd = Category::where('woocomerce_id', '=', $category['id'])
                    ->where('user_id', '=', $mainId)
                    ->first();
                    // valido si existe la categoria para este producto
                    $validateCategoryInProduct = CategoryProduct::where('product_id', '=', $productInBbDd->id)
                    ->where('category_id', '=', $categoryBbDd->id)
                    ->first();
                    // si no existe la categoria para este producto la creo
                    if (!isset($validateCategoryInProduct->id)) {
                        CategoryProduct::create([
                            'product_id' => $productInBbDd->id,
                            'category_id' => $categoryBbDd->id,
                        ]);
                    }
                }
            }

            // procesamos los attributos
            foreach ($request->products as $key => $value) {
                if ($value['type'] == 'variable') {
                    // buscamos el producto por el wooomerce id
                    $productInBbDd = Product::where('woocomerce_id', '=', $value['id'])
                    ->where('user_id', '=', $mainId)
                    ->first();
                    // proccess all categories
                    foreach ($value['attributes'] as $attr => $attribute) {
                        if (isset($attribute['id'])) {
                            // saco la categoria
                            $attrInBbDd = Attribute::where('woocomerce_id', '=', $attribute['id'])
                            ->where('user_id', '=', $mainId)
                            ->first();
                            // valido si existe la categoria para este producto
                            $validateAttrInBbDd = AttributeProduct::where('product_id', '=', $productInBbDd->id)
                            ->where('attribute_id', '=', $attrInBbDd->id)
                            ->first();
                            // si no existe la categoria para este producto la creo
                            if (!isset($validateAttrInBbDd->id)) {
                                AttributeProduct::create([
                                    'product_id' => $productInBbDd->id,
                                    'attribute_id' => $attrInBbDd->id,
                                ]);
                            }
                            // set terms of products
                            $allTerms = array();
                            foreach ($attribute['options'] as $ter => $term) {
                                // consulto el termino del atributo en la bbdd
                                $termsInBbDd = Term::where('name', '=', $term)
                                ->where('attribute_id', '=', $attrInBbDd->id)
                                ->first();
                                // save term in bbdd
                                if (isset($termsInBbDd->id)) {
                                    // valido si existe el termino en la tabla
                                    $validateTermBBdd = TermProduct::where('product_id', '=', $productInBbDd->id)
                                    ->where('term_id', '=', $termsInBbDd->id)
                                    ->first();
                                    // si no existe le asigno el termino al producto
                                    if (!isset($validateTermBBdd->id)) {
                                        TermProduct::create([
                                            'product_id' => $productInBbDd->id,
                                            'term_id' => $termsInBbDd->id,
                                        ]);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
            // proccess variant 
            $allVariantProducts = Product::where('type', '=', 'variable')
            ->where('user_id', '=', $mainId)
            ->orderBy('id', 'DESC')
            ->get();
            
            //loops of all product with variant
            foreach ($allVariantProducts as $key => $value) {
                $id = $value['woocomerce_id'];
                //filter in request main product with woocommerce proccess id
                $filterInAllNewProducts = collect($request->products)->where('id', $id)->first();
                //loops in variant of product
                $variants = array();
                if (isset($filterInAllNewProducts['type']) and $filterInAllNewProducts['type'] == 'variable') {
                    foreach ($filterInAllNewProducts['variants'] as $var => $variant) {
                        $issetVariant = ProductVariants::where('woocomerce_id', '=', $variant)->first();
                        //if no isset variant create data and job
                        if (!isset($issetVariant->id)) {
                            $newVariant = [
                                'product_id' => $value['id'],
                                'woocomerce_id' => $variant
                            ];

                            // job data
                            $dataJob = $newVariant;
                            $dataJob['woocomerce'] = $wooData;
                            ProductVariantImportation::dispatch($dataJob)->delay(now()->addMinutes(2));
                            //push variant data
                            array_push($variants, $newVariant);
                        } else {
                            $variant = [
                                'product_id' => $value['id'],
                                'woocomerce_id' => $variant
                            ];

                            // job data
                            $dataJob = $variant;
                            $dataJob['woocomerce'] = $wooData;
                            ProductVariantImportation::dispatch($dataJob)->delay(now()->addMinutes(2));
                        }
                    }
                    $proccessVariants = ProductVariants::insert($variants);
                }
            }

            $response = Response::make(json_encode(['success' => 'Se han importado los productos']), 200)->header('Content-Type','application/json');
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    // from erp create or update product
    public function fromErpPlugin(Request $request) {
        try {
            if (!$request->has('username') or !$request->has('password')) {
                return response()->json(['errors' => 'Configura tus credenciales de ingreso.'], 422);
            } else {
                $user = User::where('name', $request->username)->first();
                if ($user) {
                    $mainId = $user->getIdMain($user);
                    if (Hash::check($request->password, $user->password)) {
                        $product = Product::where('woocomerce_id', '=', $request->id)
                        ->where('user_id', '=', $mainId)
                        ->first();
                        
                        if ($request->stock_quantity > 0) {
                            $quantity = $request->stock_quantity;
                        } else {
                            $quantity = 100;
                        }
                        
                        if (is_null($request->price)) {
                            $price = 0;
                        } else {
                            $price = $request->price;
                        }

                        if (is_null($request->regularPrice)) {
                            $regular_price = 0;
                        } else {
                            $regular_price = $request->regularPrice;
                        }
                        if (!isset($product->id)) {
                            $product = Product::create([
                                'name' => $request->name,
                                'woocomerce_id' => $request->id,
                                'status' => $request->status,
                                'description' => $request->description,
                                'sku' => $request->sku,
                                'stock_quantity' => $quantity,
                                'stock_status' => $request->stock_status,
                                'price' => $price,
                                'regular_price' => $regular_price,
                                'type' => $request->type,
                                'user_id' => $mainId
                            ]);

                            AuditoriaController::store($user->name, 'Ha creado el producto: ' .$product->name . ' desde el plugin del erps.', 'Productos', $mainId);

                            $response = Response::make(json_encode(['success' => 'Se ha creado el producto correctamente.']), 200)->header('Content-Type','application/json');
                        } else {
                            $product->update([
                                'name' => $request->name,
                                'woocomerce_id' => $request->id,
                                'status' => $request->status,
                                'description' => $request->description,
                                'sku' => $request->sku,
                                'stock_quantity' => $quantity,
                                'stock_status' => $request->stock_status,
                                'price' => $price,
                                'regular_price' => $regular_price,
                                'type' => $request->type,
                                'user_id' => $mainId
                            ]);

                            AuditoriaController::store($user->name, 'Ha modificado el producto: ' .$product->name . ' desde el plugin del erps.', 'Productos', $mainId);

                            $response = Response::make(json_encode(['success' => 'Se ha modificado el producto correctamente.']), 200)->header('Content-Type','application/json');
                        }

                        
                        return $response;

                    } else {
                        $response = ['errors' => array(['code' => 422, 'message'=>'La contraseña es incorrecta.'])];
                        return response($response, 422);
                    }
                } else {
                    $response = ['errors' => array(['code' => 422, 'message'=>'El usuario no existe.'])];
                    return response($response, 422);
                }
            }

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    //paginate
    public function paginateProduct(Request $request) {
        try {
            $mainId = $request->user()->getIdMain($request->user());
            
            $products = Product::where('user_id', '=', $mainId)
            ->paginate(52);

            $data['products'] = new ProductsCollection($products);
            $data['last_page'] = $products->lastPage();
            
            $response = Response::make(json_encode(['success' => $data]), 200)->header('Content-Type','application/json');

            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    // list product for report
    public function getAllForReport(Request $request) {
        try {
            $mainId = $request->user()->getIdMain($request->user());
            
            $products = Product::where('user_id', '=', $mainId)
            ->get();
            
            $response = Response::make(json_encode(['success' => $products]), 200)->header('Content-Type','application/json');
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

}
