<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttributeProduct extends Model
{
    protected $table = "attribute_product";
    //fillable
    protected $fillable = ['attribute_id' , 'product_id'];
}
