<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'type', 'photo', 'domiciliario_id', 'woocomerce_url', 'woocomerce_ck', 'woocomerce_cs', 'parent_id', 'type_person'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    //relations of model
    public function Domiciliarios() {
        return $this->hasMany('App\Domiciliario');
    }

    public function Channels() {
        return $this->hasMany('App\Channel');
    }

    public function Domicilios() {
        return $this->hasMany('App\Domicilio');
    }

    public function PodOrder() {
        return $this->hasMany('App\PodOrder');
    }

    public function Ecommerce() {
        return $this->hasOne('App\Ecommerce');
    }

    public function attributes() {
        return $this->hasMany('App\Attribute');
    }

    public function categories() {
        return $this->hasMany('App\Category');
    }

    //hook
    public function getWooKeys($type, $user) {
        if ($type == 2) {
            $data['url'] = $user->woocomerce_url;
            $data['ck'] = $user->woocomerce_ck;
            $data['cs'] = $user->woocomerce_cs;
            return $data;

        } else if ($type == 4) {
            $newUser = User::findOrFail($user->parent_id);
            $data['url'] = $newUser->woocomerce_url;
            $data['ck'] = $newUser->woocomerce_ck;
            $data['cs'] = $newUser->woocomerce_cs;
            return $data;
        }
    }

    public function getIdMain($user) {
        if ($user->type == 2) {
            return $user->id;
        }
        if ($user->type == 4) {
            return $user->parent_id;
        }
    }

    public function getOwnUser($user) {
        if ($user > 0) {
            $own = User::findOrFail($user);
            $own->brand =  $own->Ecommerce;
            return $own;
        } else {
            return 'no-parent';
        }
    }

}
