<?php

namespace App\Jobs\TermsAttribute;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use Automattic\WooCommerce\Client;

use App\Helpers\Helper;

use App\Term;
use App\Attribute;

class TermsAttributeImport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        //
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // saco el atributo principal
        $attribute = Attribute::where('woocomerce_id', '=', $this->data['woocomerce_id'])->first();

            // saco los terminos del atributo desde el woocomercer
        $terms = Helper::getWoocomerce('products/attributes/' . $this->data['woocomerce_id'] . '/terms/', $this->data['user']);

        foreach ($terms as $key => $value) {
                // valido si existe el termino por su woocomerce id
            $issetTerms = Term::where('woocomerce_id', '=', $value->id)->first();

                // si no existe el termino lo creamos
            if (!isset($issetTerms)) {
                Term::create([
                    'name' => $value->name,
                    'woocomerce_id' => $value->id,
                    'slugs' => $value->slug,
                    'attribute_id' => $attribute->id
                ]);
            } else {
                    // si existe lo modifico
                $issetTerms->update([
                    'name' => $value->name,
                    'woocomerce_id' => $value->id,
                    'slugs' => $value->slug
                ]);
            }
        }
    }
}
