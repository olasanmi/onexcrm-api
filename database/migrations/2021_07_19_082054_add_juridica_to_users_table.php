<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddJuridicaToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            //
            $table->string('type_person', 250)->nullable();
            $table->string('legal_form', 250)->nullable();
            $table->string('iva', 250)->nullable();
            $table->string('email_invoice', 250)->nullable();
            $table->string('email_notification', 250)->nullable();
            $table->string('web_url', 250)->nullable();
            $table->string('rut_path', 250)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
