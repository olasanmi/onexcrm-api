<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Domicilio;

class Client extends Model
{
    //
    //fillable
    protected $fillable = ['name', 'user_id', 'dni', 'phone', 'email', 'address'];

    //relation
    //relations
    public function PodOrder() {
        return $this->hasMany('App\PodOrder');
    }

    //hook
    static function getClientData($dni) {
        $client = Client::where('dni', '=', $dni)
        ->first();
        if (isset($client->id)) {
            return $client;
        }
    }

    static function getAllPedidos($dni) {
        $orders = Domicilio::where('dni', '=', $dni)
        ->get();
        return $orders;
    }
}
