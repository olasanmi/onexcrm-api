<?php

namespace App\Http\Resources\DomicilioProduct;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ReportCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->map(function ($models) {
            return [
                'id' => $models->id,
                'reference' => $models->reference,
                'name' => $models->name,
                'price'  => $models->price,
                'utilities'  => $models->utilities,
                'product_id'  => $models->product_id,
                'total'  => $models->total,
                'old'  => true,
                'newProduct'  => 0,
                'type'  => 1,
                'quantity'  => $models->quantity,
                'selected_store' => $models->selected_store
            ];
        });
    }
}
