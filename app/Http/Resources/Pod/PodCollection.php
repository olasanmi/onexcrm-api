<?php

namespace App\Http\Resources\Pod;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Http\Resources\DomicilioProduct\DomicilioProductsCollection;
use App\Http\Resources\Seller\SellerResource;
use App\Http\Resources\Client\ClientResource;

class PodCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->map(function ($models) {
                return [
                    'id' => $models->id,
                    'reference' => $models->reference,
                    'price'  => $models->total,
                    'total'  => $models->total,
                    'utilities'  => $models->utilities,
                    'is_pay'  => true,
                    'hora'  => $models->created_at->format('d-m-Y'),
                    'type'  => 3,
                    'status'  => 10,
                    'products'  => new DomicilioProductsCollection($models->Products),
                    'client'  => new ClientResource($models->Client)
                ];
            });
    }
}
