<?php

namespace App\Http\Controllers;

use App\Category;
use App\User;
use Illuminate\Http\Request;

use App\Http\Resources\Category\CategoryCollection;
use App\Http\Resources\Category\CategoryResource;

use Response;

use Automattic\WooCommerce\Client;

use App\Http\Controllers\AuditoriaController;

use App\Helpers\Helper;

class CategoryController extends Controller
{
    private $wooCommerce;
    
    function __construct() {
        $this->wooCommerce = new Client(
            env('WOOCOMMERCE_URL'),
            env('WOOCOMMERCE_CK'),
            env('WOOCOMMERCE_CS'),
            [
                'wp_api' => true,
                'version' => 'wc/v3'
            ]
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        //list all
        try {
            // saco el id del cliente ecoomerce
            $mainId = $request->user()->getIdMain($request->user());
            // si el usuario es admin saco todas las categorias
            if ($request->user()->type == 1)
                $categories = Category::orderBy('id', 'DESC')
                ->get();
            // si el usuairio no es admin filtro por id de woocomerce
            else
                $categories = Category::orderBy('id', 'DESC')
                ->where('user_id', '=', $request->user()->id)
                ->get();

            $response = Response::make(json_encode(['success' => new CategoryCollection($categories)]), 200)->header('Content-Type','application/json');
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors' => array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            // saco el id principal
            $mainId = $request->user()->getIdMain($request->user());
            // seteo la data para guardar la categoria
            $data = [
                'name' => $request->name,
                'description' => $request->description
            ];
            // post data to woocomerce
            $woocomerceCat = Helper::createInWoocomerce($data, 'products/categories', $request->user(), 1);
            // si crea correctamente la categoria en el woocomerce saco la procedo a creara localmente.
            if (isset($woocomerceCat['id'])) {
                $category = Category::create([
                    'name' => $request->name,
                    'description' => $request->description,
                    'user_id' => $mainId,
                    'woocomerce_id' => $woocomerceCat['id']
                ]);
                // respuesta satisfactoria
                $response = Response::make(json_encode(['success' => 'Se ha creado la categoria correctamente.']), 200)->header('Content-Type','application/json');
            } else {
                // informamos al cliente que ocurrio un error guardando la categoria en el woocomerce
                $response = Response::make(json_encode(['errors' => array(['code' => 400, 'message' => 'Error procesando la categoria.'])]), 400)->header('Content-Type','application/json');
            }
            // guardo la auditoria
            AuditoriaController::store($request->user()->name, 'Ha creado la categoría: ' .$category->name, 'Categoria', $mainId);
            // regreso la respuesta
            $response = Response::make(json_encode(['success' => 'Se ha guardado la categoria correctamente']), 201)->header('Content-Type','application/json');
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors' => array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //filtro la categoria
        try {
            $response = ['success' => new CategoryResource($category)];    
            return response($response, 200);

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        //
        try {
            // seteo la data para modificar la categoria
            $data = [
                'name' => $request->name,
                'description' => $request->description
            ];
            // modificar la data en el woocomerce
            $woocomerceTerm = Helper::createInWoocomerce($data, 'products/categories/' . $category->woocomerce_id, $request->user(), 2);

            if (isset($woocomerceTerm['id'])) {
                // modificar la categoria localmente
                $category->update([
                    'name' => $request->name,
                    'description' => $request->description
                ]);

                // get id of user main
                $mainId = $request->user()->getIdMain($request->user());
                AuditoriaController::store($request->user()->name, 'Ha modificado la categoria: ' .$category->name, 'Categorias', $mainId);

                // response if update success
                $response = Response::make(json_encode(['success' => 'Se ha modificado la categoria correctamente']), 200)->header('Content-Type','application/json');
                return $response;
            } else {
                // response if update error
                $response = Response::make(json_encode(['errors' => array(['code' => 422, 'message' => 'Error procesando el categories.'])]), 400)->header('Content-Type','application/json');
            }
            
        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Category $category)
    {
        //
        try {
            // filtro el ecommerce id
            $mainId = $request->user()->getIdMain($request->user());
            // si existe woocomerce id procedo a borrar la categoria en el ecommerce
            if ($category->woocomerce_id) {
                // delete data from woocomerce
                $woocomerceTerm = Helper::deleteInWoocomerce('products/categories/' . $category->woocomerce_id, $request->user());
            }
            // borro la categoria en la bbdd
            $category->delete();
            // guardo en auditoria esta accion.
            AuditoriaController::store($request->user()->name, 'Ha eliminado la categoria: ' .$category->name, 'Categorias', $mainId);

            // response if update success
            $response = Response::make(json_encode(['success' => 'Se ha eliminado la categoria correctamente']), 200)->header('Content-Type','application/json');
            return $response;
            
        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    // funcion de importacion de categorias
    public function importCategories(Request $request) {
        try {
            // get main id of user
            $mainId = $request->user()->getIdMain($request->user());

            //array for insert
            $categories = array();
            // set new attribute data
            foreach ($request['categories'] as $key => $value) {
                // validar si existe el categories en nuestra bbdd
                $category = Category::where('woocomerce_id', '=', $value['id'])
                ->where('user_id', '=', $mainId)
                ->first();
                // si no existe hago push de la data del nuevo categories a crear.
                if (!$category) {
                    $new = [
                        'name' => $value['name'],
                        'description' => $value['description'],
                        'woocomerce_id' => $value['id'],
                        'user_id' => $mainId
                    ];
                    //push categories al arreglo de nuevas categorias
                    array_push($categories,  $new);
                } else {
                    // modifico si existe el categories en la base de datos
                    $category->name = $value['name'];
                    $category->description = $value['description'];
                    $category->save();
                }
            }

            // si poseemos nuevas categorias los procedemos a crear en la base de datos.
            if (count($categories) > 0) {
                Category::insert($categories);
            }

            // response if update success
            $response = Response::make(json_encode(['success' => 'Se han importado las categoria correctamente']), 200)->header('Content-Type','application/json');
            return $response;
        
        } catch (\Exception $e) {
            return response()->json(['errors' => array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }
}
