<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Domicilio extends Model
{
    //fillable
    protected $fillable = ['reference', 'channel', 'observation', 'price', 'delivery', 'total', 'status', 'is_pay', 'domiciliario_id', 'name', 'dni', 'phone', 'address', 'email', 'is_warranty', 'is_liquidado', 'qr_code', 'icg_client', 'icg_document_id', 'type', 'type_payment', 'method_payment', 'user_id', 'discount', 'utilities', 'discount_porcent', 'seller_id'];

    //relations
    public function domiciliario() {
        return $this->belongsTo('App\Domiciliario');
    }

    public function User() {
        return $this->belongsTo('App\User');
    }

    public function products() {
        return $this->hasMany('App\DomicilioProduct');
    }

    public function statusDomicilio() {
        return $this->hasMany('App\Estados');
    }

    public function Seller() {
        return $this->belongsTo('App\Seller');
    }
}
