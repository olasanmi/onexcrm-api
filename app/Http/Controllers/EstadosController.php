<?php

namespace App\Http\Controllers;

use App\Estados;
use App\Domicilio;
use App\Domiciliario;
use Illuminate\Http\Request;

use App\Helpers\Helper;

use App\Http\Controllers\AuditoriaController;

use App\Http\Resources\Domicilio\DomicilioResource;
use App\Http\Resources\Domicilio\DomicilioCollection;

use Response;

use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;

class EstadosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //  SAVE STATUS IN BBDD
        $status = Estados::create([
            'status' => $request->status,
            'observation' => $request->observation,
            'domiciliario' => $request->user,
            'domicilio_id' => $request->domicilio,
        ]);

        // return data
        $domicilio = Domicilio::findOrFail($request->domicilio);
        $domicilio->status = $request->status;
        if ($request->status === 3) {
            $domicilio->is_pay = 'Si';
        }
        
        $domicilio->domiciliario_id = $request->domiciliario;
        $domicilio->save();

        if ($request->status == 1) {
            $estado = 'asignado';
        }

        if ($request->status == 2) {
            $estado = 'recogido';
        }

        if ($request->status == 3) {
            $estado = 'entregado';
        }

        if ($request->status == 4) {
            $estado = 'novedad';
        }

        if ($request->status == 5) {
            $estado = 'cancelado';
        }

        $mainId = $request->user()->getIdMain($request->user());
        //save auditoria
        AuditoriaController::store($request->user()->name, 'Ha modificado el estado del domicilio con la referencia: ' .$domicilio->reference , 'Estados', $mainId);
        //send ws msg
        // $sendWs = Helper::sendWhatsAppMsg($request->domicilio, $request->status, $request->observation);

        //send email msg
        $sendEmail = Helper::sendEmailMsg($request->domicilio, $request->status, $request->observation);

        $domicilios = Domicilio::orderBy('id', 'DESC')
        ->where('created_at', '>=', date('Y-m-d').' 00:00:00')
        ->get();

        $response = Response::make(json_encode(['domicilio' => new DomicilioResource($domicilio), 'allDomicilios' => new DomicilioCollection($domicilios)]), 201)->header('Content-Type','application/json');
        
        return $response;

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Estados  $estados
     * @return \Illuminate\Http\Response
     */
    public function show(Estados $estados)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Estados  $estados
     * @return \Illuminate\Http\Response
     */
    public function edit(Estados $estados)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Estados  $estados
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Estados $estados)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Estados  $estados
     * @return \Illuminate\Http\Response
     */
    public function destroy(Estados $estados)
    {
        //
    }
}
