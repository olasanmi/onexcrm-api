<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Domicilio;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Domicilio::class, function (Faker $faker) {
	$price = rand(1, 30000);
    $rand = rand(0, 1);
    if ($rand == 1) {
        $pago = 'Si';
    }
    if ($rand == 0) {
        $pago = 'No';
    }
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'channel' => 'WhatsApp',
        'price' => $price,
        'delivery' => 9000,
        'total' => $price + 9000,
        'reference' => Str::random(10),
        'observation' => $faker->text(),
        'status' => rand(1, 5),
        'is_pay' => $pago,
        'domiciliario_id' => 2,
        'phone' => '123456789',
        'dni' => Str::random(10),
        'address' => $faker->text(),

    ];
});
