<?php

namespace App\Http\Resources\Domicilio;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Http\Resources\Domiciliario\DomiciliarioResource;
use App\Http\Resources\DomicilioProduct\DomicilioProductsCollection;
use App\Http\Resources\Status\StatusCollection;
use App\Http\Resources\User\UserResource;
use App\Http\Resources\Seller\SellerResource;

class DomicilioResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'reference' => $this->reference,
            'channel' => $this->channel,
            'observation'  => $this->observation,
            'price'  => $this->price,
            'delivery'  => $this->delivery,
            'total'  => $this->total,
            'status'  => $this->status,
            'is_pay'  => $this->is_pay,
            'name'  => $this->name,
            'discountTotal'  => $this->discount,
            'discount'  => $this->discount_porcent,
            'utilities'  => $this->utilities,
            'dni'  => $this->dni,
            'phone'  => $this->phone,
            'email'  => $this->email,
            'address'  => $this->address,
            'liquidad'  => $this->is_liquidado,
            'garantia'  => $this->is_warranty,
            'type'  => $this->type,
            'seller_id'  => $this->seller_id,
            'seller'  => new SellerResource($this->Seller),
            'statusDomicilio' => new StatusCollection($this->statusDomicilio),
            'domiciliario'  => new DomiciliarioResource($this->domiciliario),
            'products'  => new DomicilioProductsCollection($this->products),
            'user'  => new UserResource($this->User)
        ];
    }
}
