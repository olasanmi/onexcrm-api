<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypePayment extends Model
{
    //fillable
    protected $fillable = ['name', 'user_id', 'photo'];
}
