<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PodOrder extends Model
{
    //fillable
    protected $fillable = ['total', 'total_payment', 'utilities', 'type_payment', 'observation', 'seller_id', 'client_id', 'user_id', 'effecty', 'debit', 'credit', 'transfer', 'reference'];

    //relations
    public function Products() {
        return $this->hasMany('App\PodOrderProduct');
    }

    public function Client() {
        return $this->belongsTo('App\Client');
    }

    public function User() {
        return $this->belongsTo('App\User');
    }

    public function Seller() {
        return $this->belongsTo('App\Seller');
    }

}
