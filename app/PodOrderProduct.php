<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PodOrderProduct extends Model
{
    //fillable
    protected $fillable = ['reference', 'product_id', 'name', 'price', 'quantity', 'total', 'utilities', 'pod_order_id'];

    //relations
    public function Order() {
        return $this->belongsTo('PodOrder');
    }

    public function product() {
        return $this->belongsTo('App\Product');
    }
}
