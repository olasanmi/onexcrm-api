<?php

namespace App\Jobs\Product;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use App\Attribute;
use App\ProductVariants;
use App\Helpers\Helper;

class ProductVariantCreate implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $data;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $variantsWoo = [
            'create' => []
        ];
        foreach ($this->data['variants'] as $var => $variants) {
            $arrayAtribute = [];
            foreach ($variants['attributes'] as $varAttr => $varAttribute) {
                $attribute = Attribute::find($varAttribute['id']);
                $attrData['id'] = $attribute->woocomerce_id;
                $attrData['option'] = $varAttribute['option'];
                array_push($arrayAtribute, $attrData);
            }
            $dataVariant = [
                'status' => 'publish',
                'quantity' => $variants['quantity'],
                'description' => $variants['description'], 
                'sku' => $variants['reference'],
                'price' => $variants['price'],
                'regular_price' => $variants['price'],
                'manage_stock' => true,
                'stock_status' => 'instock',
                'attributes' => $arrayAtribute
            ];
            array_push($variantsWoo['create'], $dataVariant);
        }
        $productWooVariants = Helper::createInWoocomerce($variantsWoo, 'products/'. $this->data['prod']['woocomerce_id'] . '/variations/batch', $this->data['user'], 1);
        // save variant in our bbdd
        foreach ($variantsWoo['create'] as $varWoo => $VariWoo) {
            $vari = collect($productWooVariants['create'])->where('sku', $VariWoo['sku'])->first();
            $label = '';
            foreach ($VariWoo['attributes'] as $termAtr => $termAttr) {
                if (count($VariWoo['attributes']) > 1) {
                    $label = $label . ' - ' . $termAttr['option'];
                } else {
                    $label = $label . $termAttr['option'];
                }
            }
            $var = ProductVariants::create([
                'status' => $VariWoo['status'],
                'description' => $this->data['prod']['description'],
                'sku' => $VariWoo['sku'],
                'attribute' => $label,
                'price' => $VariWoo['price'],
                'regular_price' => $VariWoo['price'],
                'stock_quantity' => $VariWoo['quantity'],
                'stock_status' => $this->data['prod']['stock_status'],
                'product_id' => $this->data['prod']['id'],
                'woocomerce_id' => $vari['id']
            ]);
        }
    }
}
