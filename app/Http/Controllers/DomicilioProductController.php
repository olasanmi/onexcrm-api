<?php

namespace App\Http\Controllers;

use App\DomicilioProduct;
use Illuminate\Http\Request;

class DomicilioProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\DomicilioProduct  $domicilioProduct
     * @return \Illuminate\Http\Response
     */
    public function show(DomicilioProduct $domicilioProduct)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\DomicilioProduct  $domicilioProduct
     * @return \Illuminate\Http\Response
     */
    public function edit(DomicilioProduct $domicilioProduct)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\DomicilioProduct  $domicilioProduct
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DomicilioProduct $domicilioProduct)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\DomicilioProduct  $domicilioProduct
     * @return \Illuminate\Http\Response
     */
    public function destroy(DomicilioProduct $domicilioProduct)
    {
        //
    }
}
