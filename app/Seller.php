<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seller extends Model
{
    //fillable
    protected $fillable = ['name', 'phone', 'email', 'nit', 'address', 'user_id'];

    //relations
    public function Domicilios() {
        return $this->hasMany('App\Domicilio');
    }

    public function Pos() {
        return $this->hasMany('App\PodOrder');
    }
}
