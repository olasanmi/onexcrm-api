<?php

namespace App\Http\Controllers;

use App\Attribute;
use Illuminate\Http\Request;

use Response;

use App\Http\Resources\Attribute\AttributeCollection;
use App\Http\Resources\Attribute\AttributeResource;

use App\Helpers\Helper;

use App\Http\Controllers\AuditoriaController;
use App\Http\Controllers\TermsController;

class AttributeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //get global attributes
        try {
            // get main id of user
            $mainId = $request->user()->getIdMain($request->user());
            // get all atributes
            $attributes = Attribute::orderBy('id', 'DESC')
            ->where('user_id', '=', $mainId)
            ->get();
            // return response
            $response = Response::make(json_encode(['success' => new AttributeCollection($attributes)]), 200)->header('Content-Type','application/json');
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors' => array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try {
            // get main id of user
            $mainId = $request->user()->getIdMain($request->user());
            // set data of attribute
            $data = [
                'name' => $request->name,
                'type' => 'select',
                'order_by' => 'menu_order',
                'has_archives' => true
            ];
            // post data to woocomerce
            $woocomerceTerm = Helper::createInWoocomerce($data, 'products/attributes', $request->user(), 1);
            // if isset woocomerce attribute id
            if (isset($woocomerceTerm['id'])) {
                // create atribute in our bbdd
                $attribute = Attribute::create([
                    'name' => $request->name,
                    'slug' => $woocomerceTerm['slug'],
                    'user_id' => $mainId,
                    'woocomerce_id' => $woocomerceTerm['id']
                ]);
                //save auditoria
                // get id of user main
                AuditoriaController::store($request->user()->name, 'Ha creado el atributo: ' .$attribute->name, 'Atributos', $mainId);

                $response = Response::make(json_encode(['success' => 'Se ha creado el atributo correctamente.']), 200)->header('Content-Type','application/json');
            } else {
                $response = Response::make(json_encode(['errors' => array(['code' => 422, 'message' => 'Error procesando el atributo.'])]), 400)->header('Content-Type','application/json');
            }
            return $response;
        
        } catch (\Exception $e) {
            return response()->json(['errors' => array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Attribute  $attribute
     * @return \Illuminate\Http\Response
     */
    public function show(Attribute $attribute)
    {
        //
        try {
            $response = ['success' => new AttributeResource($attribute)];    
            return response($response, 200);

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Attribute  $attribute
     * @return \Illuminate\Http\Response
     */
    public function edit(Attribute $attribute)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Attribute  $attribute
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Attribute $attribute)
    {
        //
        //
        try {
            // set data to update
            $data = [
                'name' => $request->name
            ];
            // post data to woocomerce
            $woocomerceTerm = Helper::createInWoocomerce($data, 'products/attributes/' . $attribute->woocomerce_id, $request->user(), 2);
            // if isset woocomerce attribute id
            if (isset($woocomerceTerm['id'])) {
                // create atribute in our bbdd
                $attribute->update([
                    'name' => $request->name,
                    'slug' => $woocomerceTerm['slug']
                ]);
                //save auditoria
                // get id of user main
                $mainId = $request->user()->getIdMain($request->user());
                AuditoriaController::store($request->user()->name, 'Ha modificado el atributo: ' .$attribute->name, 'Atributos', $mainId);

                // response if update success
                $response = Response::make(json_encode(['success' => 'Se ha modificado el atributo correctamente']), 200)->header('Content-Type','application/json');
            } else {
                // response if update error
                $response = Response::make(json_encode(['errors' => array(['code' => 422, 'message' => 'Error procesando el atributo.'])]), 400)->header('Content-Type','application/json');
            }
            // return response
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Attribute  $attribute
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Attribute $attribute)
    {
        //delete atribute
        try {
            // validate if have woocomerce id
            if ($attribute->woocomerce_id) {
                // delete data from woocomerce
                $woocomerceTerm = Helper::deleteInWoocomerce('products/attributes/' . $attribute->woocomerce_id, $request->user());
            }
            // delete attribute in our bbdd
            $attribute->delete();

            //save auditoria
            // get id of user main
            $mainId = $request->user()->getIdMain($request->user());
            AuditoriaController::store($request->user()->name, 'Ha eliminado el atributo: ' .$attribute->name, 'Atributos', $mainId);

            // response return
            $response = Response::make(json_encode(['success' => 'Se ha eliminado el atributo correctamente']), 200)->header('Content-Type','application/json');
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors' => array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    // import attributes
    public function importAttributes(Request $request) {
        //
        try {

            // get main id of user
            $mainId = $request->user()->getIdMain($request->user());

            //array for insert
            $attributes = array();
            // set new attribute data
            foreach ($request['attributes'] as $key => $value) {
                // validar si existe el atributo en nuestra bbdd
                $attribute = Attribute::where('woocomerce_id', '=', $value['id'])
                ->where('user_id', '=', $mainId)
                ->first();
                // si no existe hago push de la data del nuevo atributo a crear.
                if (!$attribute) {
                    $newAttribute = [
                        'name' => $value['name'],
                        'slug' => $value['slug'],
                        'woocomerce_id' => $value['id'],
                        'user_id' => $mainId
                    ];
                    //push atributo al arreglo de neuvos atributos
                    array_push($attributes,  $newAttribute);
                } else {
                    // modifico si existe el atributo en la base de datos
                    $attribute->name = $value['name'];
                    $attribute->slug = $value['slug'];
                    $attribute->save();
                }
                
            }

            // si poseemos nuevos atributos los procedemos a crear en la base de datos.
            if (count($attributes) > 0) {
                if (Attribute::insert($attributes)) {
                    // procesar terminos de cada atributo
                    $attrs = Attribute::all();
                    foreach ($attrs as $key => $value) {
                        $terms = TermsController::importTerms($value['woocomerce_id'], $request->user());
                    }
                    // retornar respuesta
                    $response = Response::make(json_encode(['success' => 'Se han importando los atributos correctamente.']), 200)->header('Content-Type','application/json');
                    return $response;

                } else {
                    return response()->json(['errors' => array(['code' => 400, 'message' => 'Ha ocurrido un error importando los atributos.'])], 400);
                }

            } else {
                // procesar terminos de cada atributo
                $attrs = Attribute::all();
                foreach ($attrs as $key => $value) {
                    $terms = TermsController::importTerms($value->woocomerce_id, $request->user());
                }
            }
        
        } catch (\Exception $e) {
            return response()->json(['errors' => array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

}
