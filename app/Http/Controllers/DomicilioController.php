<?php

namespace App\Http\Controllers;

use App\Domicilio;
use App\DomicilioProduct;
use App\PodOrder;
use App\User;
use Illuminate\Http\Request;

use App\Helpers\Helper;
use Response;

use App\Http\Controllers\AuditoriaController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ClientController;

use PDF;

use App\Http\Resources\Domicilio\DomicilioCollection;
use App\Http\Resources\Domicilio\DomicilioResource;

use Carbon\Carbon;

class DomicilioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        //listen
        try {
            if ($request->user()->type == 1)
                $domicilios = Domicilio::orderBy('id', 'DESC')
                ->where('created_at', '>=', date('Y-m-d').' 00:00:00')
                ->get();

            if ($request->user()->type == 2)
                $domicilios = Domicilio::orderBy('id', 'DESC')
                ->where('user_id', '=', $request->user()->id)
                ->where('created_at', '>=', date('Y-m-d').' 00:00:00')
                ->get();

            if ($request->user()->type == 4)
                $domicilios = Domicilio::orderBy('id', 'DESC')
                ->where('user_id', '=', $request->user()->parent_id)
                ->where('created_at', '>=', date('Y-m-d').' 00:00:00')
                ->get();

            if ($request->user()->type == 3)
                $domicilios = Domicilio::orderBy('id', 'DESC')
                ->where('user_id', '=', $request->user()->parent_id)
                ->where('domiciliario_id', '=', $request->user()->domiciliario_id)
                ->where('created_at', '>=', date('Y-m-d').' 00:00:00')
                ->get();

            $response = Response::make(json_encode(['success' => new DomicilioCollection($domicilios)]), 200)->header('Content-Type','application/json');
            
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }


    //filter range date
    public function filterDateRange(Request $request) {
        try {
            if ($request->user()->type == 1)
                $domicilios = Domicilio::orderBy('id', 'DESC')
                ->whereBetween('created_at', [$request->min.' 00:00:00', $request->max.' 23:59:59'])
                ->get();

            if ($request->user()->type == 2)
                $domicilios = Domicilio::orderBy('id', 'DESC')
                ->where('user_id', '=', $request->user()->id)
                ->whereBetween('created_at', [$request->min.' 00:00:00', $request->max.' 23:59:59'])
                ->get();

            if ($request->user()->type == 4)
                $domicilios = Domicilio::orderBy('id', 'DESC')
                ->where('user_id', '=', $request->user()->parent_id)
                ->whereBetween('created_at', [$request->min.' 00:00:00', $request->max.' 23:59:59'])
                ->get();
            
            $response = Response::make(json_encode(['allDomicilios' => new DomicilioCollection($domicilios)]), 200)->header('Content-Type','application/json');
            
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    //filter histori
    public function getHistory(Request $request)
    {
        try {
            if ($request->user()->type == 1)
                $domicilios = Domicilio::orderBy('id', 'DESC')
                ->get();

            if ($request->user()->type == 2)
                $domicilios = Domicilio::orderBy('id', 'DESC')
                ->where('user_id', '=', $request->user()->id)
                ->get();

            if ($request->user()->type == 4)
                $domicilios = Domicilio::orderBy('id', 'DESC')
                ->where('user_id', '=', $request->user()->parent_id)
                ->get();
            
            $response = Response::make(json_encode(['success' => new DomicilioCollection($domicilios)]), 200)->header('Content-Type','application/json');
            
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    //filter cancelled
    public function getHistoryCancel(Request $request)
    {
        try {
            if ($request->user()->type == 1)
                $domicilios = Domicilio::orderBy('id', 'DESC')
                ->where('status', '=', 5)
                ->get();

            if ($request->user()->type == 2)
                $domicilios = Domicilio::orderBy('id', 'DESC')
                ->where('status', '=', 5)
                ->where('user_id', '=', $request->user()->id)
                ->get();

            if ($request->user()->type == 4)
                $domicilios = Domicilio::orderBy('id', 'DESC')
                ->where('status', '=', 5)
                ->where('user_id', '=', $request->user()->parent_id)
                ->get();
            
            $response = Response::make(json_encode(['success' => new DomicilioCollection($domicilios)]), 200)->header('Content-Type','application/json');
            
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    //filter warranty
    public function getHistoryWarranty(Request $request)
    {
        try {
            if ($request->user()->type == 1)
                $domicilios = Domicilio::orderBy('id', 'DESC')
                ->where('is_warranty', '=', 1)
                ->get();

            if ($request->user()->type == 2)
                $domicilios = Domicilio::orderBy('id', 'DESC')
                ->where('is_warranty', '=', 1)
                ->where('user_id', '=', $request->user()->id)
                ->get();

            if ($request->user()->type == 4)
                $domicilios = Domicilio::orderBy('id', 'DESC')
                ->where('is_warranty', '=', 1)
                ->where('user_id', '=', $request->user()->parent_id)
                ->get();
            
            $response = Response::make(json_encode(['success' => new DomicilioCollection($domicilios)]), 200)->header('Content-Type','application/json');
            
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    // get month order
    public function orderMonth(Request $request)
    {
        //get date
        $mainId = $request->user()->getIdMain($request->user());
        $month = date('m');
        $date = Carbon::create(date('Y'), $month);
        //listen
        if ($request->user()->type !== 1) {
            $domicilios = Domicilio::orderBy('id', 'DESC')
            ->where('created_at', '>=', $date)
            ->where('user_id', '=', $mainId)
            ->get();
            $pos = PodOrder::orderBy('id', 'DESC')
            ->where('created_at', '>=', $date)
            ->where('user_id', '=', $mainId)
            ->get();
        } else{
            $domicilios = Domicilio::orderBy('id', 'DESC')
            ->where('created_at', '>=', $date)
            ->get();
            $pos = PodOrder::orderBy('id', 'DESC')
            ->where('created_at', '>=', $date)
            ->get();
        }  
        
        $response = Response::make(json_encode(['success' => ['orders' => new DomicilioCollection($domicilios), 'pos' => $pos]]), 200)->header('Content-Type','application/json');
        
        return $response;
    }

    // get year order
    public function orderYear(Request $request)
    {
        $mainId = $request->user()->getIdMain($request->user());

        if ($request->user()->type !== 1) {
            $domicilios = Domicilio::whereBetween('created_at', [
                Carbon::now()->startOfYear(),
                Carbon::now()->endOfYear()
            ])
            ->where('user_id', '=', $mainId)
            ->get();
            $pos = PodOrder::whereBetween('created_at', [
                Carbon::now()->startOfYear(),
                Carbon::now()->endOfYear()
            ])
            ->where('user_id', '=', $mainId)
            ->get();

        } else {
            $domicilios = Domicilio::whereBetween('created_at', [
                Carbon::now()->startOfYear(),
                Carbon::now()->endOfYear()
            ])
            ->get();
            $pos = PodOrder::whereBetween('created_at', [
                Carbon::now()->startOfYear(),
                Carbon::now()->endOfYear()
            ])
            ->get();
        }
        $response = Response::make(json_encode(['success' => ['orders' => new DomicilioCollection($domicilios), 'pos' => $pos]]), 200)->header('Content-Type','application/json');
        return $response;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return response()->json($request->all());
        try {
            //validate if isset domicilio
            $issetDomicilio = Domicilio::where('reference', '=', $request->reference)
            ->get();
            if (count($issetDomicilio) > 0) {
                return response()->json(['errors'=>array(['code' => 422, 'message'=>'Existe un comicilio con esta referencia.'])], 422);
            }
            // get id of user main
            $mainId = $request->user()->getIdMain($request->user());
            //save client
            $clientNew = ClientController::stores($request->name, $request->dni, $request->phone, $request->email, $request->address, $mainId);
            //create new domicilio
            $domicilio = Domicilio::create([
                'reference' => $request->reference,
                'channel' => $request->channel,
                'observation'  => $request->observation,
                'price'  => $request->price,
                'delivery'  => $request->delivery,
                'type_payment'  => $request->type_payment,
                'total'  => $request->total,
                'dni'  => $request->dni,
                'name'  => $request->name,
                'email'  => $request->email,
                'address'  => $request->address,
                'phone'  => $request->phone,
                'domiciliario_id'  => $request->domiciliario,
                'is_pay' => $request->is_pay,
                'seller_id' => $request->seller_id['id'],
                'status' => $request->status,
                'is_warranty' => $request->is_warranty,
                'type' => $request->type,
                'user_id' => $mainId,
                'discount' => $request->discount,
                'discount_porcent' => $request->discount_porcent
            ]);
            
            foreach ($request->products as $key => $value) {
                $product = new DomicilioProduct();   
                $product->reference = $value['sku'];
                $product->name = $value['name'];
                $product->utilities = $value['utilities'];
                $product->product_id = $value['product_id'];
                $product->price = $value['price'];
                $product->quantity = $value['quantity'];
                $product->total = $value['total'];
                $product->domicilio_id = $domicilio->id;
                if (isset($value['selectedStorage'])) {
                    $product->selected_store = $value['selectedStorage'];
                }
                $product->save();

                // update domicilio utilities
                $domicilio->utilities = $domicilio->utilities + $value['utilities'];
                $domicilio->save();
            }

            //manage stock          
            $ProductController = new ProductController();
            $updateStock = $ProductController->manageStock($request->products, $request->user());
            
            //save auditoria
            AuditoriaController::store($request->user()->name, 'Ha creado el domicilio con la referencia: ' .$domicilio->reference, 'Domicilios', $mainId);

            //return response
            $response = Response::make(json_encode(['data' => new DomicilioResource($domicilio)]), 201)->header('Content-Type','application/json');
            
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors' => array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Domicilio  $domicilio
     * @return \Illuminate\Http\Response
     */
    public function show(Domicilio $domicilio)
    {
        //
        try {
            $response = ['success' => new DomicilioResource($domicilio)];    
            return response($response, 200);

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Domicilio  $domicilio
     * @return \Illuminate\Http\Response
     */
    public function edit(Domicilio $domicilio)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Domicilio  $domicilio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {
            //return response()->json($request->all());
            // get id of user main
            $mainId = $request->user()->getIdMain($request->user());
            
            $domicilio = Domicilio::findOrFail($request->id);
            $domicilio->update([
                'reference' => $request->reference,
                'channel' => $request->channel,
                'observation'  => $request->observation,
                'price'  => $request->price,
                'delivery'  => $request->delivery,
                'total'  => $request->total,
                'dni'  => $request->dni,
                'name'  => $request->name,
                'email'  => $request->email,
                'seller_id' => $request->seller_id['id'],
                'address'  => $request->address,
                'phone'  => $request->phone,
                'domiciliario_id'  => $request->domiciliarios,
                'is_pay' => $request->is_pay,
                'status' => $request->status,
                'is_warranty' => $request->is_warranty,
                'type_payment'  => $request->type_payment,
                'type' => $request->type,
                'discount' => $request->discount,
                'discount_porcent' => $request->discount_porcent
            ]);

            //modify product in new quantity
            $domicilio->utilities = 0;
            $domicilio->save();

            $updatedsProds = [];
            foreach ($request->products['data'] as $key => $value) {
               if (isset($value['id'])) {
                    $product = DomicilioProduct::findOrFail($value['id']);
                    if ($value['quantity'] > $product->quantity) {
                        $newQuantity = $value['quantity'] - $product->quantity;
                        $operation = 1;
                    }

                    if ($value['quantity'] < $product->quantity){
                        $newQuantity = $product->quantity - $value['quantity'];
                        $operation = 2;
                    }

                    if ($value['quantity'] == $product->quantity){
                        $newQuantity = $product->quantity - $value['quantity'];
                        $operation = 3;
                    }
                    $product->reference = $value['reference'];
                    $product->name = $value['name'];
                    $product->price = $value['price'];
                    $product->quantity = $value['quantity'];
                    $product->total = $value['total'];
                    $product->utilities = $value['utilities'];
                    if (isset($value['selected_store'])) {
                        $product->selected_store = $value['selected_store'];
                    }

                    $product->save();

                    $domicilio->utilities = $domicilio->utilities + $value['utilities'];
                    $domicilio->save();
                    
                    $product->operation = $operation;
                    $product->newQuantity = $newQuantity;
                    if ($operation != 3) {
                        array_push($updatedsProds, $product);
                    }
               }
            }

            //update product in update item quantity
            $ProductController = new ProductController();
            $updateStock = $ProductController->manageStockFromUpdateItem($updatedsProds, $request->user());
            
            //
            if ($request->has('productsNew')) {
                foreach ($request->productsNew as $key => $value) {
                    $product = new DomicilioProduct();   
                    $product->reference = $value['reference'];
                    $product->product_id = $value['product_id'];
                    $product->name = $value['name'];
                    $product->price = $value['price'];
                    $product->quantity = $value['quantity'];
                    $product->total = $value['total'];
                    $product->utilities = $value['utilities'];
                    $product->domicilio_id = $domicilio->id;
                    if (isset($value['selectedStorage'])) {
                        $product->selected_store = $value['selectedStorage'];
                    }
                    $product->save();
                    
                    $domicilio->utilities = $domicilio->utilities + $value['utilities'];
                    $domicilio->save();
                }
                //manage stock
                $ProductController = new ProductController();
                $updateStock = $ProductController->manageStock($request->productsNew, $request->user());
            }

            //save auditoria
            AuditoriaController::store($request->user()->name, 'Ha modificado el domicilio con la referencia: ' .$domicilio->reference, 'Domicilios', $mainId);

            $response = ['domicilio' => new DomicilioResource($domicilio)];
            
            return response($response, 200);
 
        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Domicilio  $domicilio
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
        try {
            $domicilio = Domicilio::findOrFail($request->id);
        
            $mainId = $request->user()->getIdMain($request->user());
            //save auditoria
            AuditoriaController::store($request->user()->name, 'Ha eliminado el domicilio con la referencia: ' .$domicilio->reference, 'Domicilios', $mainId);

            $domicilio->delete();

            if ($request->user()->type == 1)
                $domicilios = Domicilio::orderBy('id', 'DESC')
                ->where('created_at', '>=', date('Y-m-d').' 00:00:00')
                ->get();

            if ($request->user()->type == 2)
                $domicilios = Domicilio::orderBy('id', 'DESC')
                ->where('user_id', '=', $request->user()->id)
                ->where('created_at', '>=', date('Y-m-d').' 00:00:00')
                ->get();

            if ($request->user()->type == 4)
                $domicilios = Domicilio::orderBy('id', 'DESC')
                ->where('user_id', '=', $request->user()->parent_id)
                ->where('created_at', '>=', date('Y-m-d').' 00:00:00')
                ->get();

            $response = Response::make(json_encode(['success' => new DomicilioCollection($domicilios)]), 200)->header('Content-Type','application/json');
            
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    //generate pdf file to load
    public function generatePdf(Request $request) {
        try {
            //main id user
            $mainId = $request->user()->getIdMain($request->user());

            //get user data
            $userData = User::findOrFail($mainId);

            //get domicilio
            $domicilio = Domicilio::findOrFail($request->id);
            $domicilio->brand = $userData->Ecommerce;


            $pdf = PDF::loadView('pdf/factura', array('domicilio' => new DomicilioResource($domicilio)));
            
            $path = public_path('pdf/');
            $fileName =  'factua_'. $request->id . '.' . 'pdf' ;
            $pdf->save($path . '/' . $fileName);
            
            $file = public_path() . "/pdf/" . $fileName;
            
            $headers = [
                'Content-Type' => 'application/pdf',
            ];
            
            return response()->download($file, 'factura.pdf', $headers);

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    //factura para el usuario
    public function userFactura($id) {
        try {
            $domicilio = Domicilio::findOrFail($id);
            $pdf = PDF::loadView('pdf/factura', array('domicilio' => new DomicilioResource($domicilio)));
            
            $path = public_path('pdf/');
            $fileName =  'factua_'. $domicilio->reference . '.' . 'pdf' ;
            $pdf->save($path . '/' . $fileName);
            
            $file = public_path() . "/pdf/" . $fileName;
            
            $headers = [
                'Content-Type' => 'application/pdf',
            ];
            
            return response()->download($file, 'factura.pdf', $headers);

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    //GENERATE PDF2
    public function liquidarPdf(Request $request) {
        try {
            $pdf = PDF::loadView('pdf/liquidar', array('data' => $request->all()));
            $path = public_path('liquidar/');
            $fileName =  'liquidar_'. date('Y-m-d') . '.' . 'pdf' ;
            $pdf->save($path . '/' . $fileName);

            $file = public_path() . "/liquidar/" . $fileName;

            $headers = [
                'Content-Type' => 'application/pdf',
            ];
            return response()->download($file, 'factura.pdf', $headers);

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    //track domicilio
    public function tracker(Request $request) {
        $domicilio = Domicilio::where('reference', '=', $request->reference)
        ->get();
        
        $response = Response::make(json_encode(['success' => new DomicilioResource($domicilio[0])]), 200)->header('Content-Type','application/json');
        
        return $response;
    }

    // from woocomerce
    public function fromWoocomerce(Request $request) {
        
        $order = json_decode($request->order, true);
        $products = json_decode($request->products, true);
        $domicilio = Domicilio::create([
            'reference' => $order['id'],
            'channel' => 'Página web',
            'observation'  => $request->observation,
            'price'  => $request->subtotal,
            'type_payment'  => $request->type_payment,
            'delivery'  => $request->delivery,
            'total'  => $request->total,
            'dni'  => $request->dni,
            'type'  => 1,
            'name'  => $order['billing']['first_name'] .' ' . $order['billing']['last_name'],
            'email'  => $order['billing']['email'],
            'address'  => $order['billing']['address_1'] . ' ' . $order['billing']['address_2'] . ' - ' . $order['billing']['city'] . '  ' . $order['billing']['state'],
            'phone'  => $order['billing']['phone'],
            'domiciliario_id'  => 2,
            'is_pay' => 'Si',
            'status' => 0,
            'is_warranty' => 0,
        ]);

        foreach ($products as $key => $value) {
            $product = new DomicilioProduct();   
            $product->reference = $value['sku'];
            $product->name = $value['name'];
            $product->product_id = $value['product_id'];
            $product->price = $value['price'];
            $product->quantity = $value['quantity'];
            $product->total = $value['total'];
            $product->domicilio_id = $domicilio->id;
            $product->save();
            $ProductController = new ProductController();
            if (isset($value['variation_id'])) {
                $variation_id = $value['variation_id'];
            } else {
                $variation_id = 0;
            }
            if (isset($value['product_id'])) {
                $product_id = $value['product_id'];
            } else {
                $product_id = 0;
            }
            if (isset($value['sku'])) {
                $sku = $value['sku'];
            } else {
                $sku = 0;
            }
            $updateStock = $ProductController->manageStock($product_id, $sku, $value['quantity']);
        }
    }

    //filter domicilio by client
    public function filterByClient(Request $request) {
        try {
            $domicilios = Domicilio::orderBy('id', 'DESC')
            ->where('user_id', '=', $request->user()->id)
            ->where('dn', '=', $request->dni)
            ->get();

            $response = Response::make(json_encode(['success' => new DomicilioCollection($domicilios)]), 200)->header('Content-Type','application/json');
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors' => array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

}
