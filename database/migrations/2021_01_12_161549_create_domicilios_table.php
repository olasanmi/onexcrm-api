<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDomiciliosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('domicilios', function (Blueprint $table) {
            $table->id();
            $table->string('reference', 250)->nullable()->unique();
            $table->string('channel', 250)->nullable();
            $table->string('observation', 250)->nullable();
            $table->float('price', 100, 2)->nullable();
            $table->float('delivery', 100, 2)->nullable();
            $table->float('total', 100, 2)->nullable();
            $table->integer('status')->default(1);
            $table->string('is_pay')->nullable();
            $table->unsignedBigInteger('domiciliario_id')->nullable()->unsigned();
            $table->foreign('domiciliario_id')->references('id')->on('domiciliarios')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('domicilios');
    }
}
