<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Ecommerce\EcommerceResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'photo' => $this->photo,
            'type' => $this->type,
            'domiciliario_id' => $this->domiciliario_id,
            'woocomerce_url'  => $this->woocomerce_url,
            'woocomerce_ck'  => $this->woocomerce_ck,
            'woocomerce_cs'  => $this->woocomerce_cs,
            'ownUser' =>  $this->getOwnUser($this->parent_id),
            'brand'  => new EcommerceResource($this->Ecommerce)
        ];
    }
}
