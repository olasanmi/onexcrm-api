<?php

namespace App\Http\Resources\Product;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Http\Resources\Category\CategoryCollection;
use App\Http\Resources\ProductVariants\ProductVariantsCollection;

class ProductsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'type' => $this->type,
            'status' => $this->status,
            'description' => $this->description,
            'short_description' => $this->short_description,
            'featured' => $this->featured,
            'sku' => $this->sku,
            'price' => $this->price,
            'value' => $this->utilities,
            'regular_price' => $this->regular_price,
            'sale_price' => $this->sale_price,
            'stock_quantity' => $this->stock_quantity,
            'stock_status' => $this->stock_status,
            'terms' => $this->Terms,
            'attributes' => $this->Attributes,
            'categories' => new CategoryCollection($this->Categories),
            'variants' => new ProductVariantsCollection($this->ProductVariants),
            'storages' => $this::getStorages($this)
        ];
    }
}
