<?php

namespace App\Http\Resources\User;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Http\Resources\Services\ServicesCollection;

class UserCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->transform(function ($models){
                return [
                    'id' => $models->id,
                    'username' => $models->name,
                    'email' => $models->email,
                    'type'  => $models->type,
                    'woocomerce_url'  => $models->woocomerce_url,
                    'woocomerce_ck'  => $models->woocomerce_ck,
                    'woocomerce_cs'  => $models->woocomerce_cs,
                    'type_person'  => $models->type_person
                ];
            })
        ];
    }
}
