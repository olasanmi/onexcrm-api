<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estados extends Model
{
    //fillable
    protected $fillable = ['status', 'observation', 'domicilio_id', 'domiciliario'];

    //relations
    public function domicilio() {
        return $this->belongsTo('App\Domicilio');
    }
}
