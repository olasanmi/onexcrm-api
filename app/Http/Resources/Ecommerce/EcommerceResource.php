<?php

namespace App\Http\Resources\Ecommerce;

use Illuminate\Http\Resources\Json\JsonResource;

class EcommerceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'nit'  => $this->nit,
            'brand_name'  => $this->brand_name,
            'phone'  => $this->phone,
            'type_doc'  => $this->type_doc,
            'commercial_brand'  => $this->commercial_brand,
            'economic_activity'  => $this->economic_activity,
            'address'  => $this->address,
            'state'  => $this->state,
            'city'  => $this->city,
            'user_id'  => $this->user_id,
            'legal_form'  => $this->legal_form,
            'iva'  => $this->iva,
            'email_invoice'  => $this->email_invoice,
            'email_notification'  => $this->email_notification,
            'web_url'  => $this->web_url,
            'rut_path'  => $this->rut_path
        ];
    }
}
