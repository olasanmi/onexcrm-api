<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Channel extends Model
{
    //fillable
    protected $fillable = ['name', 'path', 'user_id'];

    //relations
    public function User() {
        return $this->belongsTo('App\User');
    }
}
