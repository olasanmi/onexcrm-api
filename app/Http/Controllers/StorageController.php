<?php

namespace App\Http\Controllers;

use App\Storage;
use App\ProductStorage;
use App\Product;

use App\Http\Resources\Product\ProductsResource;

use Illuminate\Http\Request;

use Response;
use App\Http\Resources\Storage\StoragesCollection;

use App\Http\Controllers\AuditoriaController;

class StorageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //get storages by users
        try {
            // get id of user main
            $mainId = $request->user()->getIdMain($request->user());

            if ($request->user()->type == 1)
                $storages = Storage::orderBy('id', 'DESC')
                ->get();
            else
                $storages = Storage::orderBy('id', 'DESC')
                ->where('user_id', '=', $mainId)
                ->get();

            //return response
            $response = Response::make(json_encode(['success' => new StoragesCollection($storages)]), 200)->header('Content-Type','application/json');
            return $response;
            

        } catch (\Exception $e) {
            return response()->json(['errors' => array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //store  new storage
        try {
            // get id of user main
            $mainId = $request->user()->getIdMain($request->user());
            $storage = Storage::create([
                'description' => $request->description,
                'city' => $request->city,
                'type' => 0,
                'user_id' => $mainId 
            ]);

            if ($request->has('is_default') and $request->is_default == true) {
                $storage->is_default = 1;
                $oldStorgaeDefault = Storage::where('user_id', '=', $mainId)
                ->where('is_default', '=', 1)
                ->first();
                if (isset($oldStorgaeDefault->id)) {
                    $oldStorgaeDefault->is_default = 0;
                    $oldStorgaeDefault->save();
                }
                
            } else {
                $storage->is_default = 0;
            }
            $storage->save();

            $mainId = $request->user()->getIdMain($request->user());
            AuditoriaController::store($request->user()->name, 'Ha creado la bodega: ' .$storage->description, 'Bodegas', $mainId);

        } catch (\Exception $e) {
            return response()->json(['errors' => array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Storage  $storage
     * @return \Illuminate\Http\Response
     */
    public function show(Storage $storage)
    {
        //
         try {
            // get id of user main
            $mainId = $request->user()->getIdMain($request->user());
            $storage = Storage::create([
                'description' => $request->description,
                'city' => $request->city,
                'type' => 0,
                'user_id' => $mainId 
            ]);
            $response = Response::make(json_encode(['success' => 'Se ha agregado la bodega correctamente.']), 200)->header('Content-Type','application/json');
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors' => array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Storage  $storage
     * @return \Illuminate\Http\Response
     */
    public function edit(Storage $storage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Storage  $storage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Storage $storage)
    {
        //update storage
        try {
            // get id of user main
            $mainId = $request->user()->getIdMain($request->user());
            $storage->update([
                'description' => $request->description,
                'city' => $request->city
            ]);

            if ($request->has('is_default') and $request->is_default == true) {
                $storage->is_default = 1;
                $oldStorgaeDefault = Storage::where('user_id', '=', $mainId)
                ->where('is_default', '=', 1)
                ->first();
                if (isset($oldStorgaeDefault->id)) {
                    $oldStorgaeDefault->is_default = 0;
                    $oldStorgaeDefault->save();
                }
                
            } else {
                $storage->is_default = 0;
            }
            $storage->save();

            $mainId = $request->user()->getIdMain($request->user());
            AuditoriaController::store($request->user()->name, 'Ha modificado la bodega: ' .$storage->description, 'Bodegas', $mainId);

            $response = Response::make(json_encode(['success' => 'Se ha modificado la bodega correctamente.']), 200)->header('Content-Type','application/json');
            return $response;
            

        } catch (\Exception $e) {
            return response()->json(['errors' => array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Storage  $storage
     * @return \Illuminate\Http\Response
     */
    public function destroy(Storage $storage, Request $request)
    {
        //delete storage
        try {
            // get id of user main
            $storage->delete();

            $mainId = $request->user()->getIdMain($request->user());
            //return list of storages
            if ($request->user()->type == 1)
                $storages = Storage::orderBy('id', 'DESC')
                ->get();
            else
                $storages = Storage::orderBy('id', 'DESC')
                ->where('user_id', '=', $mainId)
                ->get();

            $mainId = $request->user()->getIdMain($request->user());
            AuditoriaController::store($request->user()->name, 'Ha eliminado la bodega: ' .$storage->description, 'Bodegas', $mainId);

            $response = Response::make(json_encode(['success' => new StoragesCollection($storages)]), 200)->header('Content-Type','application/json');
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors' => array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    //set storage to product
    public function setProductStorage(Request $request) {
        try {
            // get id of user main
            $productStorage = ProductStorage::create([
                'product_id' => $request->product,
                'storage_id' => $request->storage,
                'quantity' => $request->quantity
            ]);

            $product = Product::findOrFail($request->product);
            $response = Response::make(json_encode(['success' => new ProductsResource($product)]), 200)->header('Content-Type','application/json');
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors' => array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    //update product storage
     public function updateProductStorage(Request $request)
    {
        try {
            // get id of user main
            $storage = ProductStorage::findOrFail($request->idT);
            $storage->quantity = $request->quantity;
            $storage->save();

            //return data
            $product = Product::findOrFail($storage->product_id);
            $response = Response::make(json_encode(['success' => new ProductsResource($product)]), 200)->header('Content-Type','application/json');
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors' => array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    //delete product storage
    public function delProductStorage($id, Request $request)
    {
        //delete storage
        try {
            // get id of user main
            $storage = ProductStorage::findOrFail($id);
            $storage->delete();

            //return data
            $product = Product::findOrFail($storage->product_id);
            $response = Response::make(json_encode(['success' => new ProductsResource($product)]), 200)->header('Content-Type','application/json');
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors' => array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }
}
