<?php

namespace App\Http\Controllers;

use App\PodOrder;
use App\PodOrderProduct;
use Illuminate\Http\Request;

use Response;

use App\Http\Controllers\AuditoriaController;
use App\Http\Resources\Pod\PodResource;
use App\Http\Resources\Pod\PodCollection;

use App\Http\Controllers\ProductController;

use Carbon\Carbon;

class PodOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //get all pod orders
        try {
            // get main id of user
            $mainId = $request->user()->getIdMain($request->user());

            if ($request->user()->type === 1)
                $pos = PodOrder::orderBy('id', 'DESC')
                ->where('created_at', '>=', Carbon::now()->format('Y-m-d'))
                ->get();
            else
                $pos = PodOrder::orderBy('id', 'DESC')
                ->where('user_id', '=', $mainId)
                ->where('created_at', '>=', Carbon::now()->format('Y-m-d'))
                ->get();

            $response = Response::make(json_encode(['success' => new PodCollection($pos)]), 200)->header('Content-Type','application/json');
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors' => array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try {
            $mainId = $request->user()->getIdMain($request->user());

            //save new order
            $order = PodOrder::create([
                'total' => $request->total,
                'total_payment' => $request->total,
                'type_payment'  => $request->typePayment,
                'observation'  => $request->observation,
                'seller_id'  => $request->seller_id['id'],
                'client_id'  => $request->client,
                'reference'  => $request->reference,
                'effecty'  => $request->effecty,
                'debit'  => $request->debit,
                'credit'  => $request->credit,
                'transfer'  => $request->transfer,
                'user_id'  => $mainId
            ]);

            foreach ($request->products as $key => $value) {
                $product = new PodOrderProduct();   
                $product->reference = $value['sku'];
                //calculate utilities
                if ($value['value'] > 0) {
                    $valueUtility = ($value['price'] - $value['value']);
                    if ($valueUtility > 0) {
                        $product->utilities = ($value['quantity'] * $valueUtility);
                    } else {
                        $product->utilities = 0;
                    }
                }
                //end calculate utilities
                $product->product_id = $value['product_id'];
                $product->price = $value['price'];
                $product->name = $value['name'];
                $product->quantity = $value['quantity'];
                $product->total = ($value['quantity'] * $value['price']);
                $product->pod_order_id = $order->id;
                $product->save();

                //set order utilities
                $order->utilities = $order->utilities + $product->utilities;
                $order->save();
            }
            
            $ProductController = new ProductController();
            $updateStock = $ProductController->manageStock($request->products, $request->user());

            AuditoriaController::store($request->user()->name, 'Ha creado el pod con la referencia: ' . $order->id, 'Pod', $mainId);

            $response = Response::make(json_encode(['success' => new PodResource($order)]), 200)->header('Content-Type','application/json');
            
            return $response;
            

        } catch (\Exception $e) {
            return response()->json(['errors' => array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PodOrder  $podOrder
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        try {
            $pos = PodOrder::findOrFail($id);

            $response = Response::make(json_encode(['success' => new PodResource($pos)]), 200)->header('Content-Type','application/json');
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors' => array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PodOrder  $podOrder
     * @return \Illuminate\Http\Response
     */
    public function edit(PodOrder $podOrder)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PodOrder  $podOrder
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PodOrder $podOrder)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PodOrder  $podOrder
     * @return \Illuminate\Http\Response
     */
    public function destroy(PodOrder $podOrder)
    {
        //
    }
}
