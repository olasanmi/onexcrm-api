<?php

namespace App\Http\Resources\Domicilio;

use Illuminate\Http\Resources\Json\ResourceCollection;

class DomicilioReportCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->map(function ($models) {
            return [
                'id' => $models->id,
                'reference' => $models->reference,
                'channel' => $models->channel,
                'observation'  => $models->observation,
                'price'  => $models->price,
                'discountTotal'  => $models->discount,
                'discount'  => $models->discount_porcent,
                'utilities'  => $models->utilities,
                'delivery'  => $models->delivery,
                'total'  => $models->total,
                'status'  => $models->status,
                'is_pay'  => $models->is_pay,
                'name'  => $models->name,
                'dni'  => $models->dni,
                'phone'  => $models->phone,
                'email'  => $models->email,
                'address'  => $models->address,
                'liquidad'  => $models->is_liquidado,
                'garantia'  => $models->is_warranty,
                'type_payment'  => $models->type_payment,
                'hora'  => $models->created_at->format('d-m-Y'),
                'qr_code'  => $models->qr_code,
                'icg_client'  => $models->icg_client,
                'icg_document_id'  => $models->icg_document_id,
                'type'  => $models->type,
                'type_payment'  => $models->type_payment,
                'method_payment'  => $models->method_payment
            ];
        });
    }
}
