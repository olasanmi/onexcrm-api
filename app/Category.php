<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //fillable
    protected $fillable = ['name' , 'slug', 'description', 'user_id', 'woocomerce_id'];

    // relations
    public function Products() {
    	return $this->belongsToMany('App\Product', 'category_product');
    }

    public function user() {
        return $this->belongsTo('App\User');
    }
}
