<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\DomicilioProduct::class, function (Faker $faker) {
	$price = rand(1, 30000);
	$quantity = rand(1, 10);
    return [
        'name' => $faker->name,
        'price' => $price,
        'reference' => Str::random(10),
        'quantity' => $quantity,
        'total' => $quantity * $price,
    ];
});
