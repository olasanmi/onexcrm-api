<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name', 250)->nullable();
            $table->string('slug', 250)->nullable();
            $table->string('type', 250)->nullable();
            $table->string('status', 250)->nullable();
            $table->longText('description')->nullable();
            $table->longText('short_description')->nullable();
            $table->boolean('featured')->default(false);
            $table->string('sku', 250)->nullable();
            $table->float('price', 250, 2)->default(0);
            $table->float('regular_price', 250, 2)->default(0);
            $table->float('sale_price', 250, 2)->default(0);
            $table->integer('stock_quantity')->default(0);
            $table->string('stock_status', 250)->nullable();
            $table->unsignedBigInteger('category_id')->nullable()->unsigned();
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            $table->bigInteger('woocomerce_id')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
