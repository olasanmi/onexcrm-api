<?php

use Illuminate\Database\Seeder;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
            'name' => 'onex-admin',
            'email' => 'admin@crm.co',
            'password' => bcrypt('admin2021-'),
            'type' => 1
        ]);
    }
}
