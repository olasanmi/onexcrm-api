<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPaymetsToPodOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pod_orders', function (Blueprint $table) {
            //
            $table->float('effecty', 100, 2)->nullable();
            $table->float('debit', 100, 2)->nullable();
            $table->float('credit', 100, 2)->nullable();
            $table->float('transfer', 100, 2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pod_orders', function (Blueprint $table) {
            //
        });
    }
}
