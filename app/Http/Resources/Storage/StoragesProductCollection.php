<?php

namespace App\Http\Resources\Storage;

use Illuminate\Http\Resources\Json\ResourceCollection;

class StoragesProductCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->map(function ($models) {
            return [
                'id' => $models->id,
                'description' => $models->description,
                'city' => $models->city,
                'type' => $models->type,
                'quantity' => $models->quantity,
                'idT' => $models->idT,
                'is_default' => $models->is_default
            ];
        });
    }
}
