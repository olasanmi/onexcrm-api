<?php

namespace App\Http\Resources\Attribute;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Http\Resources\Terms\TermsCollection;

class AttributeCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->map(function ($models) {
            return [
                'id' => $models->id,
                'name' => $models->name,
                'slug' => $models->slug,
                'terms' => new TermsCollection($models->terms),
                'woocomerce_id' => $models->woocomerce_id
            ];
        });
    }
}
