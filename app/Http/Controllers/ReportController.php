<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Domicilio;
use App\PodOrder;
use App\DomicilioProduct;
use App\PodOrderProduct;

use DB;

use Carbon\Carbon;

use Response;

use App\Http\Resources\Pod\PodReportCollection;
use App\Http\Resources\Domicilio\DomicilioReportCollection;
use App\Http\Resources\DomicilioProduct\ReportCollection;
use App\Http\Resources\PodProduct\ReportCollections;

class ReportController extends Controller
{   
    // ventas diarias
    public function diary(Request $request) {
        try {
            // get main id
            $mainId = $request->user()->getIdMain($request->user());
            
            // init query
            $domicilios = Domicilio::where('user_id', '=', $mainId);
            $pos = PodOrder::where('user_id', '=', $mainId);

            // validate query
            if ($request->date)
                $domicilios = $domicilios->where('created_at', '>=', $request->date.' 00:00:00')
                ->where('created_at', '<=', $request->date.' 23:58:00')
                ->get();
                $pos = $pos->where('created_at', '>=', $request->date.' 00:00:00')
                ->where('created_at', '<=', $request->date.' 23:58:00')
                ->get();

            $data['orders'] = new DomicilioReportCollection($domicilios);
            $data['pos'] = new PodReportCollection($pos);

            $response = Response::make(json_encode(['success' =>  $data]), 200)->header('Content-Type','application/json');
            
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    // ventar por clientes
    public function client(Request $request) {
        try {
            // get main id
            $mainId = $request->user()->getIdMain($request->user());
            
            // init query
            $domicilios = Domicilio::where('user_id', '=', $mainId);
            $pos = PodOrder::where('user_id', '=', $mainId);

            // validate query
            if ($request->date)
                $domicilios = $domicilios->where('created_at', '>=', $request->date.' 00:00:00')
                ->where('created_at', '<=', $request->date2.' 23:58:00')
                ->where('dni', '=', $request->dni)
                ->get();
                $pos = $pos->where('created_at', '>=', $request->date.' 00:00:00')
                ->where('created_at', '<=', $request->date2.' 23:58:00')
                ->where('client_id', '=', $request->clientId)
                ->get();

            $data['orders'] = new DomicilioReportCollection($domicilios);
            $data['pos'] = new PodReportCollection($pos);

            $response = Response::make(json_encode(['success' =>  $data]), 200)->header('Content-Type','application/json');
            
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    // ventar por vendedores
    public function sellers(Request $request) {
        try {
            // get main id
            $mainId = $request->user()->getIdMain($request->user());
            
            // init query
            $domicilios = Domicilio::where('user_id', '=', $mainId);
            $pos = PodOrder::where('user_id', '=', $mainId);

            // validate query
            if ($request->date)
                $domicilios = $domicilios->where('created_at', '>=', $request->date.' 00:00:00')
                ->where('created_at', '<=', $request->date2.' 23:58:00')
                ->where('seller_id', '=', $request->seller)
                ->get();
                $pos = $pos->where('created_at', '>=', $request->date.' 00:00:00')
                ->where('created_at', '<=', $request->date2.' 23:58:00')
                ->where('seller_id', '=', $request->seller)
                ->get();

            $data['orders'] = new DomicilioReportCollection($domicilios);
            $data['pos'] = new PodReportCollection($pos);

            $response = Response::make(json_encode(['success' =>  $data]), 200)->header('Content-Type','application/json');
            
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    // ventar por vendedores
    public function products(Request $request) {
        // get main id
        $mainId = $request->user()->getIdMain($request->user());
        
        // get domicilio products
        $orderProducts = DomicilioProduct::where('product_id', '=', $request->product_id)
        ->where('created_at', '>=', $request->date.' 00:00:00')
        ->where('created_at', '<=', $request->date2.' 23:58:00')
        ->get();

        // get pos products
        $posProducts = PodOrderProduct::where('product_id', '=', $request->product_id)
        ->where('created_at', '>=', $request->date.' 00:00:00')
        ->where('created_at', '<=', $request->date2.' 23:58:00')
        ->get();

        $data['orders'] = new ReportCollection($orderProducts);
        $data['pos'] = $posProducts;

        $response = Response::make(json_encode(['success' =>  $data]), 200)->header('Content-Type','application/json');
        return $response;
    }

}
