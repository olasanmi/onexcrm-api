<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Http\Resources\Product\ProductsCollection;

class Storage extends Model
{
    //fillable
    protected $fillable = ['description', 'city', 'type', 'user_id', 'is_default'];

    //relations
    public function Products()
    {
        return $this->belongsToMany('App\Product');
    }

    //hook
    public static function getPorducts($model) {
        if (count($model->Products) > 0) {
            return new ProductsCollection($model->Products);
        } else {
            return [];
        }
    } 
}
