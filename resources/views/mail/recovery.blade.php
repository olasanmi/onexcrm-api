<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Recuperar contraseña</title>
</head>
<style>
        @import url('https://fonts.googleapis.com/css2?family=Work+Sans:wght@300;400&display=swap'); 

        .b-color {
            background-color: #080652;
        }
    </style>
<body style="font-family: 'Work Sans', sans-serif;">
    <section style="width: 600px; margin: 0px auto;">

        <div class="logo" style="background-color: #080652;
            padding: 1rem 0;
            width: 100%;">
            <center>
                <img src="https://onexerp.co/assets/icon.png" width="60px">
            </center>
        </div>

        <div class="vector">
            <center>
            	<img style="width: 130px;
            margin: 4rem 0 2rem 0;" src="https://onexerp.co/assets/recuperar.png" alt="recuperar">
            </center>
        </div>

        <div class="pad" style="padding: 0 3rem;">
            <h2 style="font-size: 18pt;
            font-weight: 400;
            text-align: center;">Recupera tu contraseña</h2>

            <p style="font-size: 13pt;
            font-weight: 300;
            text-align: center;">¡Hola!</p>

            <p style="font-size: 13pt;
            font-weight: 300;
            text-align: center;">Vamos a recuperar tu contraseña, te adjuntamos el código de verificación para hacer el cambio.</p>
        </div>

        <center>
        	<div class="cod" style="background-color: #CCCCCC;
            border-radius: 10px;
            width: auto;
            padding: .5rem 2rem; width: 300px; ">
            <p style="
            color: #080652;
            font-weight: 400;
            font-size: 16pt;
            text-align: center;">{{$data['token']}}</p>
        </div>
        </center>

        <div class="pad" style="padding: 0 3rem;">
            <p style="font-size: 13pt;
            font-weight: 300;
            text-align: center;">Recuerda, tienes 30 minutos para hacer el cambio de tu contraseña. Pero no te preoucupes si pasa este lapso de tiempo, puedes pedir un nuevo código de verificación a través de Onex ERP.</p>
        </div>

        <div class="social" style=" width: 100%;
            margin: 2rem 0;">
            <center>
                <a target="_blank" href="#">
                    <img src="https://onexerp.co/assets/Facebook.png" width="30px">
                </a>
                <a target="_blank" style="margin-left: 30px;" href="#">
                    <img src="https://onexerp.co/assets/Twitter.png" width="30px">
                </a>
                <a target="_blank" style="margin-left: 30px;" href="#">
                    <img src="https://onexerp.co/assets/Instagram.png" width="30px">
                </a>
                <a target="_blank" style="margin-left: 30px;" href="#">
                    <img src="https://onexerp.co/assets/Whatsapp.png" width="30px">
                </a>
            </center>
        </div>
        <div style="
            color: #fff;
            background-color: #080652;
            padding: .2rem 0;
            margin-top: 1rem; font-size: 13pt;">
        	<center>
        	<p style="
            font-size: 13pt; text-align: center;">
            	<center>
            		Generado automáticamente por Onex ERP, por favor no responder.
            	</center>
            </p>
        </center>
    </div>
    </section>
</body>
</html>