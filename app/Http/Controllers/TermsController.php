<?php

namespace App\Http\Controllers;

use App\Term;
use App\Attribute;

use Illuminate\Http\Request;

use Response;

use App\Helpers\Helper;

use App\Http\Resources\Attribute\AttributeResource;

use App\Http\Controllers\AuditoriaController;

use App\Jobs\TermsAttribute\TermsAttributeImport;

class TermsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //store new terms
        try {
            // get id of user main
            $mainId = $request->user()->getIdMain($request->user());
            // set batch data to woocomerce
            $data = [
                'create' => $request->terms
            ];
            // envio batch
            $attrTerms = Helper::createInWoocomerce($data, 'products/attributes/' . $request->woocomerce_id . '/terms/batch', $request->user(), 1);
            // si enserta los terminos en el woocomerce procedo a crearlos en el sistema.
            if ($attrTerms['create']) {
                foreach ($attrTerms['create'] as $key => $value) {
                    if (isset($value['error']) and $value['error']['code'] === 'term_exists') {
                        return response()->json(['errors' => array(['code' => 400, 'message' => 'Ya existe en esta categoria un termino con uno de los nombres suministrados, por favor verifica tu ecommerce.' ])], 400);
                    }
                    Term::create([
                        'name' => $value['name'],
                        'woocomerce_id' => $value['id'],
                        'slugs' => $value['slug'],
                        'attribute_id' => $request->id
                    ]);
                    //save auditoria
                    AuditoriaController::store($request->user()->name, 'Ha creado el término: ' . $value['name'], 'Atributos', $mainId);
                }
            }

            // devolvemos el resource attributo como respuesta.
            $response = Response::make(json_encode(['success' => new AttributeResource(Attribute::find($request->id))]), 200)->header('Content-Type','application/json');
            return $response;
        
        } catch (\Exception $e) {
            return response()->json(['errors' => array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Terms  $terms
     * @return \Illuminate\Http\Response
     */
    public function show(Term $term)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Terms  $terms
     * @return \Illuminate\Http\Response
     */
    public function edit(Term $term)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Terms  $terms
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Term $term)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Terms  $terms
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Term $term)
    {
        //
        //delete atribute
        try {
            // validate if have woocomerce id
            if ($term->woocomerce_id) {
                // delete data from woocomerce
                $termInWoocomerceDelete = Helper::deleteInWoocomerce('products/attributes/' . $term->attribute->woocomerce_id . '/terms/' . $term->woocomerce_id, $request->user());
            }
            // delete attribute in our bbdd
            $term->delete();
            // get id of user main
            $mainId = $request->user()->getIdMain($request->user());
            //save auditoria
            AuditoriaController::store($request->user()->name, 'Ha eliminado el término: ' . $term->name, 'Atributos', $mainId);

            // response return
            $response = Response::make(json_encode(['success' => 'Se ha eliminado el término del atributo correctamente.']), 200)->header('Content-Type','application/json');
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors' => array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    // importar terminos
    public static function importTerms ($woocomerce_id, $user) {
        try {
            // saco el atributo principal
            // $attribute = Attribute::where('woocomerce_id', '=', $woocomerce_id)->first();

            // saco los terminos del atributo desde el woocomercer
            // $terms = Helper::getWoocomerce('products/attributes/' . $woocomerce_id . '/terms?per_page=100&page=1', $user);

            // foreach ($terms as $key => $value) {
                // valido si existe el termino por su woocomerce id
                // $issetTerms = Term::where('woocomerce_id', '=', $value->id)->first();

                // si no existe el termino lo creamos
                // if (!isset($issetTerms)) {
                    // Term::create([
                        // 'name' => $value->name,
                        // 'woocomerce_id' => $value->id,
                        // 'slugs' => $value->slug,
                        // 'attribute_id' => $attribute->id
                    // ]);
                // } else {
                    // si existe lo modifico
                    // $issetTerms->update([
                        // 'name' => $value->name,
                        // 'woocomerce_id' => $value->id,
                        // 'slugs' => $value->slug
                    // ]);
                // }
            // }

            // import with job
            $data['woocomerce_id'] = $woocomerce_id;
            $data['user'] = $user;
            TermsAttributeImport::dispatch($data)->delay(now()->addMinutes(1));
        
        } catch (\Exception $e) {
            return response()->json(['errors' => array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

}
