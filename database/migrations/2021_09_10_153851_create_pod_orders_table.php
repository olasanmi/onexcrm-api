<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePodOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pod_orders', function (Blueprint $table) {
            $table->id();
            $table->float('total', 100, 2)->nullable();
            $table->float('total_payment', 100, 1)->nullable();
            $table->float('utilities', 100, 2)->nullable();
            $table->string('type_payment', 1)->nullable();
            $table->string('observation', 1)->nullable();

            $table->unsignedBigInteger('seller_id')->nullable()->unsigned();
            $table->foreign('seller_id')->references('id')->on('sellers')->onDelete('cascade');
            $table->unsignedBigInteger('client_id')->nullable()->unsigned();
            $table->foreign('client_id')->references('id')->on('clients')->onDelete('cascade');
            $table->unsignedBigInteger('user_id')->nullable()->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pod_orders');
    }
}
