<?php

namespace App\Http\Controllers;

use App\TermProduct;
use Illuminate\Http\Request;

use App\Product;
use App\Attribute;

use App\Helpers\Helper;

use App\Http\Controllers\AuditoriaController;

use App\Http\Resources\Product\ProductsResource;

use Response;

class TermProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\TermProduct  $termProduct
     * @return \Illuminate\Http\Response
     */
    public function show(TermProduct $termProduct)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\TermProduct  $termProduct
     * @return \Illuminate\Http\Response
     */
    public function edit(TermProduct $termProduct)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TermProduct  $termProduct
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TermProduct $termProduct)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TermProduct  $termProduct
     * @return \Illuminate\Http\Response
     */
    public function destroy(TermProduct $termProduct)
    {
        //
    }

    // del term in product
    public function deleteTermInProduct(Request $request) {
        try {
            // get main id of user
            $mainId = $request->user()->getIdMain($request->user());
            // get product
            $products = Product::findOrFail($request->product_id);
            // set the product attribute
            $product['attributes'] = [];
            foreach ($request->newTerms as $key => $value) {
                $attribute['id'] = null;
                $attribute['variation'] = true;
                $attribute['visible'] = true;
                // set attriute terms options
                $attribute['options'] = [];
                foreach ($value['options'] as $key2 => $value2) {
                    //get attribute
                    $attr = Attribute::find($value2['attribute_id']);
                    $attribute['id'] = $attr->woocomerce_id;
                    array_push($attribute['options'], $value2['name']);
                }
                array_push($product['attributes'],  $attribute);
            }
            // update product attributes
            $woocommerceProduct = Helper::createInWoocomerce($product, 'products/'.$products->woocomerce_id, $request->user(), 2);
            // auditorias
            AuditoriaController::store($request->user()->name, 'Ha eliminado un termino del producto: ' . $products->name, 'Terminos', $mainId);
            // get terms of product
            $termProduct = TermProduct::where('product_id',  '=', $request->product_id)
            ->where('term_id', '=', $request->term_id)
            ->first();
            // delete
            $termProduct->delete();

            //response
            $response = Response::make(json_encode(['success' => new ProductsResource(Product::findOrFail($products->id))]), 200)->header('Content-Type','application/json');
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors'=>array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }
}
