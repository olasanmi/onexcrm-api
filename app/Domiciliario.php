<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Domiciliario extends Model
{
    //fillable
    protected $fillable = ['name', 'dni', 'phone', 'placa', 'photo', 'username', 'password', 'user_id'];

    //relations
    
    public function domicilios() {
        return $this->hasMany('App\Domicilio');
    }

    public function User() {
        return $this->belongsTo('App\User');
    }
}
