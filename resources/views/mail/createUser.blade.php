<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Nuevo usuario generado</title>
</head>
<body style="font-family: 'Work Sans', sans-serif;">
    
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Work+Sans:wght@300;400&display=swap');

        .b-color {
            background-color: #080652;
        }

        .social {
            width: 100%;
            display: flex;
            justify-content: center;
            align-items: center;
            margin: 2rem 0;
        }

        .social > div {
            display: flex;
            justify-content: space-evenly;
            align-items: center;
            width: 100%;
            max-width: 300px;
        }

        .social svg {
            width: 25px;
        }

        footer {
            display: flex;
            justify-content: center;
            align-items: center;
            color: #fff;
            background-color: #080652;
            padding: .2rem 0;
            margin-top: 1rem;
        }
        
        footer > p{
            font-size: 8pt;
        }
    </style>

    <section style="width: 600px; margin: 0px auto;">

        <div class="logo" style="padding: 1rem 0;
            width: 100%;">
            <center><img src="https://onexerp.co/assets/icon.png" alt="Onex ERP logo" style="width: 60px;"></center>
        </div>

        <div class="pad" style="padding: 0 3rem;">
            <h2 style="font-size: 18pt;
            font-weight: 400;
            text-align: center;">Hola</h2>

            <p style="font-size: 13pt;
            font-weight: 300;
            text-align: center;">Se ha generado un nuevo usuario en Onex ERP para ti, ha continuación se procede a mostrar tus nuevos credenciales:</p>
        </div>

        <div class="cod" style=" background-color: #CCCCCC;
            border-radius: 10px;
            width: auto;
            padding: .5rem 2rem;">
            <center>
            	<p style="color: #080652;
            font-weight: 400;
            font-size: 16pt; text-align: center;">
            	USUARIO: {{$data['username']}}<br>
				CONTRASEÑA: {{$data['password']}}
            </p>
            </center>
        </div>

        <div class="social" style=" width: 100%;
            margin: 2rem 0;">
            <center>
                <a target="_blank" href="#">
                    <img src="https://onexerp.co/assets/Facebook.png" width="30px">
                </a>
                <a target="_blank" style="margin-left: 30px;" href="#">
                    <img src="https://onexerp.co/assets/Twitter.png" width="30px">
                </a>
                <a target="_blank" style="margin-left: 30px;" href="#">
                    <img src="https://onexerp.co/assets/Instagram.png" width="30px">
                </a>
                <a target="_blank" style="margin-left: 30px;" href="#">
                    <img src="https://onexerp.co/assets/Whatsapp.png" width="30px">
                </a>
            </center>
        </div>
        <div style="
            color: #fff;
            background-color: #080652;
            padding: .2rem 0;
            margin-top: 1rem; font-size: 13pt;">
        	<center>
        	<p style="
            font-size: 13pt; text-align: center;">
            	<center>
            		Generado automáticamente por Onex ERP, por favor no responder.
            	</center>
            </p>
        </center>
    </div>
    </section>
</body>
</html>