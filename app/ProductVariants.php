<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductVariants extends Model
{
    //fillble
    protected $fillable = ['status', 'description', 'attribute', 'sku', 'price', 'regular_price', 'sale_price', 'stock_quantity', 'stock_status', 'product_id', 'woocomerce_id'];

    // relations
    public function Product() {
    	return $this->belongsTo('App\Product');
    }

}
