<?php

namespace App\Http\Resources\Auditoria;

use Illuminate\Http\Resources\Json\ResourceCollection;

class AuditoriaCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->transform(function ($models){
                return [
                    'id' => $models->id,
                    'user' => $models->user,
                    'observation' => $models->observation,
                    'entity'  => $models->entity,
                    'date'  => $models->created_at,
                    'hour'  => $models->hour,
                ];
            })
        ];
    }
}
