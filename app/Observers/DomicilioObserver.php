<?php

namespace App\Observers;

use App\Domicilio;

use App\Mail\Domicilio\DomicilioAddEmail;
use Illuminate\Support\Facades\Mail;

use Automattic\WooCommerce\Client;

class DomicilioObserver
{
    /**
     * Handle the domicilio "created" event.
     *
     * @param  \App\Domicilio  $domicilio
     * @return void
     */
    public function created(Domicilio $domicilio)
    {
        //
        $data = Domicilio::findOrFail($domicilio->id);
        Mail::to($data->email)->send(new DomicilioAddEmail($data));
    }

    /**
     * Handle the domicilio "updated" event.
     *
     * @param  \App\Domicilio  $domicilio
     * @return void
     */
    public function updated(Domicilio $domicilio)
    {
        //
    }

    /**
     * Handle the domicilio "deleted" event.
     *
     * @param  \App\Domicilio  $domicilio
     * @return void
     */
    public function deleted(Domicilio $domicilio)
    {
        //
    }

    /**
     * Handle the domicilio "restored" event.
     *
     * @param  \App\Domicilio  $domicilio
     * @return void
     */
    public function restored(Domicilio $domicilio)
    {
        //
    }

    /**
     * Handle the domicilio "force deleted" event.
     *
     * @param  \App\Domicilio  $domicilio
     * @return void
     */
    public function forceDeleted(Domicilio $domicilio)
    {
        //
    }

    //manage stock
    public function manageStock($products, $user) {

    }
}
