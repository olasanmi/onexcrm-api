<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePodOrderProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pod_order_products', function (Blueprint $table) {
            $table->id();
            $table->string('reference')->nullable();
            $table->bigInteger('product_id')->nullable();
            $table->string('name')->nullable();
            $table->float('price', 100, 2)->nullable();
            $table->integer('quantity')->nullable();
            $table->float('total', 100, 2)->nullable();
            $table->float('utilities', 100, 2)->default(0);

            $table->unsignedBigInteger('pod_order_id')->nullable()->unsigned();
            $table->foreign('pod_order_id')->references('id')->on('pod_orders')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pod_order_products');
    }
}
