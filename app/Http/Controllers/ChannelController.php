<?php

namespace App\Http\Controllers;

use App\Channel;
use Illuminate\Http\Request;

use App\Helpers\Helper;
use Response;

use App\Http\Resources\Channel\ChannelCollection;


class ChannelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //list all
        try {
            // get id of user main
            $mainId = $request->user()->getIdMain($request->user());

            if ($request->user()->type == 1)
                $channels = Channel::orderBy('id', 'DESC')
                ->get();
            else
                $channels = Channel::orderBy('id', 'DESC')
                ->where('user_id', '=', $mainId)
                ->get();

            $response = Response::make(json_encode(['success' => new ChannelCollection($channels)]), 200)->header('Content-Type','application/json');
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors' => array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //store new channel
        try {
            // get id of user main
            $mainId = $request->user()->getIdMain($request->user());
            
            $channel = Channel::create([
                'name' => $request->name,
                'user_id' => $mainId
            ]);

            if (isset($request->file)) {
                $path = Helper::uploadImage($request->file, 'channels');
                $channel->path = $path;
            }
            $channel->save();
            $response = Response::make(json_encode(['success' => 'Se ha guardado el canal correctamente']), 201)->header('Content-Type','application/json');
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors' => array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Channel  $channel
     * @return \Illuminate\Http\Response
     */
    public function show(Channel $channel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Channel  $channel
     * @return \Illuminate\Http\Response
     */
    public function edit(Channel $channel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Channel  $channel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        try {
            $channel = Channel::findOrFail($request->id);
            $channel->name = $request->name;
            if (isset($request->file)) {
                Helper::deleteFile($channel->path, 'channels');
                $path = Helper::uploadImage($request->file, 'channels');
                $channel->path = $path;
            }
            $channel->save();
            $response = Response::make(json_encode(['success' => 'Se ha guardado el canal correctamente']), 200)->header('Content-Type','application/json');
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors' => array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Channel  $channel
     * @return \Illuminate\Http\Response
     */
    public function destroy(Channel $channel)
    {
        //delete channel
        try {
            if ($channel->path) {
                Helper::deleteFile($channel->path, 'channels');
            }
            $channel->delete();

            $response = Response::make(json_encode(['success' => 'Se ha eliminado el canal correctamente']), 200)->header('Content-Type','application/json');
            return $response;

        } catch (\Exception $e) {
            return response()->json(['errors' => array(['code' => 422, 'message' => $e->getMessage()])], 422);
        }
    }
}
